#ifndef __TASK_QUEUE_H__
#define __TASK_QUEUE_H__

#include <list>
#include <mutex>
#include <condition_variable>
#include "Task.h"

namespace Tiny2D
{

	class TaskQueue
	{
	public:
		TaskQueue() {};

		void Push(Task* task);
		void PushFront(Task* task);
		Task* Pop();
		Task* TryPop();
		bool EraseById(unsigned int id);
		void EraseByOwner(void* owner);

	private:
		std::list<Task*> _list;
		std::mutex _mutex;
		std::condition_variable _condition;
	};

}

#endif // __TASK_QUEUE_H__