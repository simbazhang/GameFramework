#ifndef __SERVICE_H__
#define __SERVICE_H__


#include "TaskQueue.h"
#include <thread>
#include <vector>

namespace Tiny2D
{

	class Service
	{
	public:
		Service() {};
		~Service() { Shutdown(); }

	public:
		void Init();
		int GetWokerCount() const { return _wokers.size(); }

	public:
		void AddTask(Task* task);
		void AddTaskFront(Task* task);
		void AddCompleteTask(Task* task);
		bool CancelById(unsigned int id);
		void CancelByOwner(void* owner);

		void Tick(float dt);
		void Shutdown();

		static int GetCurWorkerIndex() { return _curWorkerIndex; }

		void PerformInMainThread(const std::function<void()>& func);
		void PerformInNextLoop(const std::function<void()>& func);

		void AddTickFunction(const std::function<void(float)>& func);

	private:
		void Worker(int index);
		Task* GetNextTask();

	private:
		TaskQueue _queue;
		TaskQueue _completeQueue;
		std::vector<std::thread> _wokers;
		std::vector<std::function<void(float)>> _tickFuncs;
		std::vector<std::function<void()>> _nextFuncs;

		static thread_local int _curWorkerIndex;
	};

}

#endif // __SERVICE_H__
