#include "TaskQueue.h"
#include <limits>
#include <algorithm>


namespace Tiny2D
{

	void TaskQueue::Push(Task* task)
	{
		if (task)
		{
			int priority = task->GetPriority();
			std::unique_lock<std::mutex> lk(_mutex);
			if (_list.size() > 0 && _list.back()->GetPriority() < priority)
			{
				for (auto iter = _list.begin(); iter != _list.end(); iter++)
				{
					if (priority >= (*iter)->GetPriority())
					{
						_list.insert(iter, std::move(task));
						break;
					}
				}
			}
			else
			{
				_list.push_back(std::move(task));
			}
			lk.unlock();
		}
		else
		{
			std::unique_lock<std::mutex> lk(_mutex);
			_list.push_back(std::move(task));
			lk.unlock();
		}
		_condition.notify_one();
	}

	void TaskQueue::PushFront(Task* task)
	{
		task->SetPriority(std::numeric_limits<int>::max());
		std::unique_lock<std::mutex> lk(_mutex);
		_list.push_front(std::move(task));
		lk.unlock();
		_condition.notify_one();
	}

	Task* TaskQueue::Pop()
	{
		std::unique_lock<std::mutex> lk(_mutex);
		_condition.wait(lk, [this] {return !_list.empty(); });
		Task* task = std::move(_list.front());
		_list.pop_front();
		lk.unlock();
		return task;
	}

	Task* TaskQueue::TryPop()
	{
		if (_mutex.try_lock())
		{
			if (!_list.empty())
			{
				Task* task = std::move(_list.front());
				_list.pop_front();
				_mutex.unlock();
				return task;
			}
			_mutex.unlock();
		}
		return nullptr;
	}

	bool TaskQueue::EraseById(unsigned int id)
	{
		std::lock_guard<std::mutex> lk(_mutex);
		if (!_list.empty())
		{
			for (auto iter = _list.begin(); iter != _list.end(); iter++)
			{
				if ((*iter) && (*iter)->GetId() == id)
				{
					_list.erase(iter);
					return true;
				}
			}
		}

		return false;
	}

	void TaskQueue::EraseByOwner(void* owner)
	{
		std::lock_guard<std::mutex> lk(_mutex);
		if (!_list.empty())
		{
			_list.erase(std::remove_if(_list.begin(), _list.end(), [owner](const Task* task) {
				return task && task->GetOwner() == owner;
			}), _list.end());
		}
	}

}

