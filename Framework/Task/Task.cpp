#include "Task.h"
#include <climits>

namespace Tiny2D
{

	static unsigned int GenId()
	{
		static unsigned int sId = 0;
		sId = (sId + 1) % UINT_MAX;
		return sId;
	}


	Task::Task() :
		_id(GenId()),
		_owner(nullptr),
		_priority(0)
	{
	}

	Task::~Task()
	{
		//
	}

	bool Task::OnSchedule()
	{
		return false;
	}

	void Task::OnComplete()
	{
		//
	}

	void SimpleTask::SetCompleteFunction(const std::function<void()>& func)
	{
		_completeFunc = func;
	}

	void SimpleTask::OnComplete()
	{
		if (_completeFunc)
		{
			_completeFunc();
		}
	}

}
