#ifndef __TASK_H__
#define __TASK_H__
#include <memory>
#include <functional>

namespace Tiny2D
{

	class Task
	{
	public:
		Task();
		virtual ~Task();

		unsigned int GetId() const { return _id; }

		void* GetOwner() const { return _owner; }
		void SetOwner(void* v) { _owner = v; }

		int GetPriority() const { return _priority; }
		void SetPriority(int v) { _priority = v; }

	protected:
		virtual bool OnSchedule();
		virtual void OnComplete();

	protected:
		unsigned int _id;
		void* _owner;
		int _priority;

		friend class Service;

	};

	class SimpleTask : public Task
	{
	public:

		void SetCompleteFunction(const std::function<void()>& func);

	protected:
		virtual void OnComplete();

		std::function<void()> _completeFunc;
	};

}

#endif // __TASK_H__