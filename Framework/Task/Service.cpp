#include "Service.h"
#include <cassert>

namespace Tiny2D
{
	thread_local int Service::_curWorkerIndex = -1;
	void Service::Init()
	{
		if (_wokers.empty())
		{
			int hc = std::thread::hardware_concurrency() - 1;
			hc = hc < 3 ? 3 : hc;
			_wokers.reserve(hc);
			for (int i = 0; i < hc; i++)
			{
				std::thread thread(&Service::Worker, this, i);
				_wokers.push_back(std::move(thread));
			}
		}
	}

	void Service::AddTask(Task* task)
	{
		if (task)
		{
			_queue.Push(std::move(task));
		}
	}

	void Service::AddTaskFront(Task* task)
	{
		if (task)
		{
			_queue.PushFront(std::move(task));
		}
	}

	void Service::AddCompleteTask(Task* task)
	{
		if (task)
		{
			_completeQueue.Push(std::move(task));
		}
	}

	bool Service::CancelById(unsigned int id)
	{
		return _queue.EraseById(id);
	}

	void Service::CancelByOwner(void* owner)
	{
		_queue.EraseByOwner(owner);
	}

	void Service::Tick(float dt)
	{
		if (!_nextFuncs.empty())
		{
			for (auto& func : _nextFuncs)
			{
				func();
			}
			_nextFuncs.clear();
		}

		for (auto& func : _tickFuncs)
		{
			func(dt);
		}

		Task* task = _completeQueue.TryPop();
		while (task)
		{
			task->OnComplete();
			task = _completeQueue.TryPop();
		}
	}

	void Service::Shutdown()
	{
		int size = _wokers.size();
		for (int i = 0; i < size; i++)
		{
			_queue.Push(nullptr);
		}
		for (int i = 0; i < size; i++)
		{
			_wokers[i].join();
		}
	}

	void Service::Worker(int index)
	{
		_curWorkerIndex = index;

		while (true)
		{
			Task* task = _queue.Pop();
			if (task)
			{
				if (task->OnSchedule())
				{
					AddCompleteTask(std::move(task));
				}
			}
			else
			{
				break;
			}
		}
	}

	Task* Service::GetNextTask()
	{
		return _queue.Pop();
	}

	void Service::PerformInMainThread(const std::function<void()>& func)
	{
		auto task = new SimpleTask();
		task->SetCompleteFunction(func);
		AddCompleteTask(std::move(task));
	}

	void Service::PerformInNextLoop(const std::function<void()>& func)
	{
		auto task = new SimpleTask();
		task->SetCompleteFunction([this, func]() {
			_nextFuncs.push_back(func);
		});
		AddCompleteTask(std::move(task));
	}

	void Service::AddTickFunction(const std::function<void(float)>& func)
	{
		_tickFuncs.push_back(func);
	}

}

