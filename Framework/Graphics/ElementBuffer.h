

#pragma once

#include "Object.h"
#include "GraphicsDefs.h"

namespace Tiny2D
{

class ElementBuffer : public Object
{
public:
    /// Construct. Optionally force headless (no GPU-side buffer) operation.
    explicit ElementBuffer(Context* context);
    /// Destruct.
    ~ElementBuffer() override;

    void SetVertexSize(unsigned size);

    void Release();

	void DebugPrint();

    bool SetSize(unsigned vertexCount, bool dynamic = false);
    
    /// Set all data in the buffer.
    bool SetData(const void* data);
    /// Set a data range in the buffer. Optionally discard data outside the range.
    bool SetDataRange(const void* data, unsigned start, unsigned count, bool discard = false);

    bool IsDynamic() const { return dynamic_; }

    unsigned GetVertexCount() const { return vertexCount_; }

    unsigned GetVertexSize() const { return vertexSize_; }
    unsigned GetGpuObject() const { return ebo_; }
	void SetDataType(unsigned type) { dataType_ = type; }
	unsigned GetDataType() { return dataType_; }
private:
   
    bool Create();
  
    bool UpdateToGPU();

    unsigned vertexCount_{};
    unsigned vertexSize_{};
 
    bool dynamic_{};
    unsigned ebo_{ 0 };
    unsigned char* data_{};
    unsigned dataType_{};
};

}
