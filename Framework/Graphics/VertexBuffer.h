

#pragma once

#include "Object.h"
#include "GraphicsDefs.h"

namespace Tiny2D
{

class VertexBuffer : public Object
{
public:
    /// Construct. Optionally force headless (no GPU-side buffer) operation.
    explicit VertexBuffer(Context* context);
    /// Destruct.
    ~VertexBuffer() override;

    void SetVertexSize(unsigned size);

    void Release();

    bool SetSize(unsigned vertexCount, bool dynamic = false);
    
    /// Set all data in the buffer.
    bool SetData(const void* data);
    /// Set a data range in the buffer. Optionally discard data outside the range.
    bool SetDataRange(const void* data, unsigned start, unsigned count, bool discard = false);

    bool IsDynamic() const { return dynamic_; }

    unsigned GetVertexCount() const { return vertexCount_; }

    unsigned GetVertexSize() const { return vertexSize_; }
    unsigned GetGpuObject() const { return vbo_; }
	void DebugPrint();
    void DebugPrintChar();
	unsigned char* GetData() const { return data_; }
private:
   
    bool Create();
  
    bool UpdateToGPU();

    unsigned vertexCount_{};
    unsigned vertexSize_{};
 
    bool dynamic_{};
    unsigned vbo_{ 0 };
    unsigned char* data_{};
};

}
