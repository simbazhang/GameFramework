
#pragma once

#include "GraphicsDefs.h"
#include "Color.h"
#include "Resource.h"

namespace Tiny2D
{


/// Base class for texture resources.
class Texture : public Resource
{
public:
    explicit Texture(Context* context);

    virtual ~Texture() {};

    int GetWidth() const { return width_; }
    int GetHeight() const { return height_; }

    TextureFilterMode GetFilterMode() const { return filterMode_; }
    TextureAddressMode GetAddressMode(TextureCoordinate coord) const { return addressModes_[coord]; }

    unsigned GetExternalFormat(unsigned format);
    unsigned GetDataType(unsigned format);
    // ��������
    unsigned GetTarget() { return target_; };
    unsigned GetTextureID() { return textureID_; };

    void UpdateParameters();
protected:
    virtual bool Create() { return true; }
    unsigned target_{};
    unsigned format_{};
    TextureUsage usage_{TEXTURE_STATIC};

    int width_{};
    int height_{};
    unsigned textureID_{};

    TextureFilterMode filterMode_{FILTER_DEFAULT};
    TextureAddressMode addressModes_[MAX_COORDS]{ADDRESS_WRAP, ADDRESS_WRAP, ADDRESS_WRAP};
};

}
