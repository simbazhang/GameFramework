#include "Texture.h"
#include <glad/glad.h>
#include "GraphicsDefs.h"

static GLenum gl3WrapModes[] =
{
    GL_REPEAT,
    GL_MIRRORED_REPEAT,
    GL_CLAMP_TO_EDGE,
    GL_CLAMP_TO_BORDER
};

static GLenum GetWrapMode(Tiny2D::TextureAddressMode mode)
{
    return gl3WrapModes[mode];
}

Tiny2D::Texture::Texture(Context* context) :Resource(context)
{

}


unsigned Tiny2D::Texture::GetExternalFormat(unsigned format)
{

    if (format == GL_DEPTH_COMPONENT16 || format == GL_DEPTH_COMPONENT24 || format == GL_DEPTH_COMPONENT32 || format == GL_DEPTH_COMPONENT)
        return GL_DEPTH_COMPONENT;
    else if (format == GL_R8 || format == GL_R16F || format == GL_R32F)
        return GL_RED;
    else if (format == GL_RG8 || format == GL_RG16 || format == GL_RG16F || format == GL_RG32F)
        return GL_RG;
    else if (format == GL_RGBA16)
        return GL_RGBA;
    else if (format == GL_RGB32F || format == GL_RGB16F)
        return GL_RGB;
    else
        return format;
}

unsigned Tiny2D::Texture::GetDataType(unsigned format)
{
    if (format == GL_RG16 || format == GL_RGBA16)
        return GL_UNSIGNED_SHORT;
    else if (format == GL_RG32F || format == GL_R32F)
        return GL_FLOAT;
    else if (format == GL_RGB32F || format == GL_RGB16F)
        return GL_FLOAT;
    else if (format == GL_DEPTH_COMPONENT)
        return GL_FLOAT;
    else
        return GL_UNSIGNED_BYTE;
}

void Tiny2D::Texture::UpdateParameters()
{
    if (textureID_ > 0)
    {
        glTexParameteri(target_, GL_TEXTURE_WRAP_S, GetWrapMode(addressModes_[COORD_U]));
        glTexParameteri(target_, GL_TEXTURE_WRAP_T, GetWrapMode(addressModes_[COORD_V]));
      
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        GLfloat borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    }

}
