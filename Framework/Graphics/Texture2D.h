#pragma once
#include "Texture.h"

namespace Tiny2D
{

class Image;

/// 2D texture resource.
class  Texture2D : public Texture
{
    OBJECT_DEFINE(Texture2D)
public:
    /// Construct.
    explicit Texture2D(Context* context);
    ~Texture2D() override;

    /// Load resource from stream. May be called from a worker thread. Return true if successful.
    bool BeginLoad(File& source) override;
    /// Finish resource loading. Always called from the main thread. Return true if successful.
    bool EndLoad() override;
    /// Release the texture.
    void Release() override;

    bool SetSize(int width, int height, unsigned format, TextureUsage usage = TEXTURE_STATIC);
    bool SetData(Image* image);
    bool SetData(int x, int y, int width, int height, const void* data);
    bool ReSize(int width, int height, unsigned format);
protected:
    /// Create the GPU texture.
    bool Create() override;
    Image* loadImage_{};
};

/// 2D texture resource.
class  TextureCube : public Texture
{
    OBJECT_DEFINE(TextureCube)
public:
    /// Construct.
    explicit TextureCube(Context* context);
    ~TextureCube() override;

    /// Load resource from stream. May be called from a worker thread. Return true if successful.
    bool BeginLoad(File& source) override;
    /// Finish resource loading. Always called from the main thread. Return true if successful.
    bool EndLoad() override;
    /// Release the texture.
    void Release() override;
    bool SetData();
    bool SetSize(int width, int height, unsigned format, TextureUsage usage = TEXTURE_STATIC);
protected:
    /// Create the GPU texture.
    bool Create() override;
    Image* loadImage_{};
};

}
