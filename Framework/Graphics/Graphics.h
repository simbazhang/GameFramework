#pragma once
#include "Object.h"
#include "GraphicsDefs.h"
#include "Rect.h"
#include <glad/glad.h>
#include "Batch3D.h"

namespace Tiny2D
{
    class Texture;
    class Texture2D;
    class VertexBuffer;
    class ProgramState;
   
	union Char2Int
	{
		char a[4];
		int i;
	};

	union Char2Float
	{
		char a[4];
		float f;
        int i;
	};

    struct RenderSurface : public Object
    {
        explicit RenderSurface(Context* context);
        ~RenderSurface();
        Texture2D* colorAtt = {};
        Texture2D* depthAtt = {};
        unsigned framebuffer = 0;
        int width=0;
        int height=0;
        unsigned int rbo=0;
        void Release();
    };

    class  Graphics : public Object
    {
    public:
        /// Construct.
        explicit Graphics(Context* context);
        /// Destruct. Release the Direct3D11 device and close the window.
        ~Graphics() override;

        void ClearColor(float r, float g, float b);
        void ClearColor();
        void ClearDepth();
        void SetTextureForUpdate(Texture* texture);

        void SetTexture(unsigned index, Texture* texture);

        void SetVBO(unsigned object);
        void SetEBO(unsigned object);
        void SetVAO();

        void SetDepthTest(bool test) {
            if(test)
                glEnable(GL_DEPTH_TEST);
            else
                glDisable(GL_DEPTH_TEST);
        };

        void SetViewport(const IntRect& rect);
        void ApplyViewport();

        void SetCullMode(CullMode mode);

        void SetVertexBuffer(VertexBuffer* buffer);

        void SetShaders(ProgramState* shader);

        void SetUniform(bool isArray, GLuint location, unsigned int size, GLenum uniformType, void* data);

        IntRect GetViewport() const { return viewport_; }

        void Draw(PrimitiveType type, unsigned vertexStart, unsigned vertexCount);

		void DrawBatch3D(const Batch3D& batch, int idx = 0);
        void DrawSkyBox();
        void PrepareDraw();

        void OnFrameStart(int width, int heigh);
        void OnFrameEnd();

        RenderSurface* CreateRenderSurface(int width, int height, bool shadowmap=false);
        void ResizeRenderSurface(RenderSurface* rs, int width, int height);
        void PushRenderSurface(RenderSurface* surface);
        void PopRenderSurface();
        RenderSurface* GetCurRenderSurface();
        void ResetRenderSurface();
        void ApplyTopRenderSurface();

        void SetRenderSurface(RenderSurface* surface);
        
        RenderSurface* GetScreenQuad() { return renderQued_; };

        const char* GetTextureUnitName(int idx);

        void BindDepthTexture(Texture* tex);
    private:
        Texture* textures_[MAX_TEXTURE_UNITS]{};
        unsigned textureTypes_[MAX_TEXTURE_UNITS]{};
        // 当前激活的的纹理单元
        unsigned activeTexture_;

        unsigned  boundVBO_{0};
        unsigned  boundEBO_{ 0 };

        IntRect viewport_{};

		VertexBuffer* vertexBuffer_{};
		ProgramState* programState_{};

        unsigned numPrimitives_{};
        unsigned numBatches_{};
        unsigned vertexArrayObject_{};
        std::vector<RenderSurface*> renderSurfaceStack_;
        RenderSurface* renderQued_{};
        RenderSurface* curSurface{};
        unsigned renderQuedVbo_{ 0 };
        float color_[3]{};
    };

}
