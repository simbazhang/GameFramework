#include "Graphics.h"
#include "Texture.h"
#include <glad/glad.h>
#include "MathDefs.h"
#include "Shader.h"
#include "VertexBuffer.h"
#include "Texture2D.h"
#include "ElementBuffer.h"
#include <iostream>
#include "Material.h"
namespace Tiny2D {


	float quadVertices[] = { // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
		// positions   // texCoords
		-1.0f,  1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
		 1.0f, -1.0f,  1.0f, 0.0f,

		-1.0f,  1.0f,  0.0f, 1.0f,
		 1.0f, -1.0f,  1.0f, 0.0f,
		 1.0f,  1.0f,  1.0f, 1.0f
	};

	const char* TextureUnitName[MAX_TEXTURE_UNITS] = {
		{"sDiffMap"},
		{"sNormalMap"},
		{"sSkybox"},
		{"sIrradianceMap"},
		{"sMatRougMap"},
		{"sOccMap"},
		{"sShadowMap"},
		{"sShadowMap_1"},
		{"sShadowMap_2"},
	};

	Graphics::Graphics(Context* context) :Object(context),activeTexture_(0)
	{
		glGenVertexArrays(1, &vertexArrayObject_);
		glBindVertexArray(vertexArrayObject_);

		glGenBuffers(1, &renderQuedVbo_);
		glBindBuffer(GL_ARRAY_BUFFER, renderQuedVbo_);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	Graphics::~Graphics()
	{
		if (renderQuedVbo_)
		{
			glDeleteBuffers(1, &renderQuedVbo_);
		}
	}

	void Graphics::ClearColor(float r, float g, float b)
	{
		color_[0] = r;
		color_[1] = g;
		color_[2] = b;
		ClearColor();
	}

	void Graphics::ClearColor()
	{
		glCheckError();
		glClearColor(color_[0], color_[1], color_[2], 1.0);
		glCheckError();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glCheckError();
	}

    void Graphics::ClearDepth()
    {
        glClear(GL_DEPTH_BUFFER_BIT);
    }

	void Graphics::SetTextureForUpdate(Texture* texture)
	{
		if (activeTexture_ != 0)
		{
			glActiveTexture(GL_TEXTURE0);
			activeTexture_ = 0;
		}

		unsigned glType = texture->GetTarget();
		// Unbind old texture type if necessary
		if (textureTypes_[0] && textureTypes_[0] != glType)
			glBindTexture(textureTypes_[0], 0);
		glBindTexture(glType, texture->GetTextureID());
		textureTypes_[0] = glType;
		textures_[0] = texture;
	}

	void Graphics::SetTexture(unsigned index, Texture* texture)
	{
        if(!texture)
            return;
		if (index >= MAX_TEXTURE_UNITS)
			return;


		if (textures_[index] != texture)
		{
			if (activeTexture_ != index)
			{
				glActiveTexture(GL_TEXTURE0 + index);
				activeTexture_ = index;
			}

			if (texture)
			{
				unsigned glType = texture->GetTarget();
				// Unbind old texture type if necessary
				if (textureTypes_[index] && textureTypes_[index] != glType)
					glBindTexture(textureTypes_[index], 0);
				glBindTexture(glType, texture->GetTextureID());
				textureTypes_[index] = glType;
			}
			else if (textureTypes_[index])
			{
				glBindTexture(textureTypes_[index], 0);
				textureTypes_[index] = 0;
			}

			textures_[index] = texture;
		}
		else
		{
			if (texture)
			{
				if (activeTexture_ != index)
				{
					glActiveTexture(GL_TEXTURE0 + index);
					activeTexture_ = index;
				}

				glBindTexture(texture->GetTarget(), texture->GetTextureID());

			}
		}
	}

	void Graphics::SetVBO(unsigned object)
	{
		if (boundVBO_ != object)
		{
			if (object)
				glBindBuffer(GL_ARRAY_BUFFER, object);
			boundVBO_ = object;
		}
	}

	void Graphics::SetEBO(unsigned object)
	{
		if (boundEBO_ != object)
		{
			if (object)
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, object);
			boundEBO_ = object;
		}
	}

	void Graphics::SetVAO()
	{
		glBindVertexArray(vertexArrayObject_);
	}

	void Graphics::SetViewport(const IntRect& rect)
	{
		glCheckError();
		IntRect rectCopy = rect;
		glViewport(rectCopy.left_, rectCopy.top_, rectCopy.Width(), rectCopy.Height());
		viewport_ = rectCopy;
		glCheckError();
	}

	void Graphics::ApplyViewport()
	{
		glViewport(viewport_.left_, viewport_.top_, viewport_.Width(), viewport_.Height());
	}

	void Graphics::SetCullMode(CullMode mode)
	{

	}

	void Graphics::SetVertexBuffer(VertexBuffer* buffer)
	{
		vertexBuffer_ = buffer;
	}

	void Graphics::SetShaders(ProgramState* programState)
	{
		if (programState_ != programState)
		{
			programState_ = programState;
			glUseProgram(programState_->GetShader()->GetProgramObject());
			programState_->Apply();

		}
		else
		{
			programState_->Apply(false);
		}
	}


#define DEF_TO_INT(pointer, index)     (*((GLint*)(pointer) + index))
#define DEF_TO_FLOAT(pointer, index)   (*((GLfloat*)(pointer) + index))

	void Graphics::SetUniform(bool isArray, GLuint location, unsigned int size, GLenum uniformType, void* data)
	{
		GLsizei count = size;
		switch (uniformType)
		{
		case GL_INT:
		case GL_BOOL:
		case GL_SAMPLER_2D:
		case GL_SAMPLER_CUBE:
			if (isArray)
				glUniform1iv(location, count, (GLint*)data);
			else
				glUniform1i(location, DEF_TO_INT(data, 0));
			break;
		case GL_INT_VEC2:
		case GL_BOOL_VEC2:
			if (isArray)
				glUniform2iv(location, count, (GLint*)data);
			else
				glUniform2i(location, DEF_TO_INT(data, 0), DEF_TO_INT(data, 1));
			break;
		case GL_INT_VEC3:
		case GL_BOOL_VEC3:
			if (isArray)
				glUniform3iv(location, count, (GLint*)data);
			else
				glUniform3i(location,
					DEF_TO_INT(data, 0),
					DEF_TO_INT(data, 1),
					DEF_TO_INT(data, 2));
			break;
		case GL_INT_VEC4:
		case GL_BOOL_VEC4:
			if (isArray)
				glUniform4iv(location, count, (GLint*)data);
			else
				glUniform4i(location,
					DEF_TO_INT(data, 0),
					DEF_TO_INT(data, 1),
					DEF_TO_INT(data, 2),
					DEF_TO_INT(data, 4));
			break;
		case GL_FLOAT:
			if (isArray)
				glUniform1fv(location, count, (GLfloat*)data);
			else
				glUniform1f(location, DEF_TO_FLOAT(data, 0));
			break;
		case GL_FLOAT_VEC2:
			if (isArray)
				glUniform2fv(location, count, (GLfloat*)data);
			else
				glUniform2f(location, DEF_TO_FLOAT(data, 0), DEF_TO_FLOAT(data, 1));
			break;
		case GL_FLOAT_VEC3:
			if (isArray)
				glUniform3fv(location, count, (GLfloat*)data);
			else
				glUniform3f(location,
					DEF_TO_FLOAT(data, 0),
					DEF_TO_FLOAT(data, 1),
					DEF_TO_FLOAT(data, 2));
			break;
		case GL_FLOAT_VEC4:
			if (isArray)
				glUniform4fv(location, count, (GLfloat*)data);
			else
				glUniform4f(location,
					DEF_TO_FLOAT(data, 0),
					DEF_TO_FLOAT(data, 1),
					DEF_TO_FLOAT(data, 2),
					DEF_TO_FLOAT(data, 3));
			break;
		case GL_FLOAT_MAT2:
			glUniformMatrix2fv(location, count, GL_FALSE, (GLfloat*)data);
			break;
		case GL_FLOAT_MAT3:
			glUniformMatrix3fv(location, count, GL_FALSE, (GLfloat*)data);
			break;
		case GL_FLOAT_MAT4:
			glUniformMatrix4fv(location, count, GL_FALSE, (GLfloat*)data);
			break;
			break;

		default:
			assert(false);
			break;
		}
	}

	static void GetGLPrimitiveType(unsigned elementCount, PrimitiveType type, unsigned& primitiveCount, GLenum& glPrimitiveType)
	{
		switch (type)
		{
		case TRIANGLE_LIST:
			primitiveCount = elementCount / 3;
			glPrimitiveType = GL_TRIANGLES;
			break;

		case LINE_LIST:
			primitiveCount = elementCount / 2;
			glPrimitiveType = GL_LINES;
			break;

		case POINT_LIST:
			primitiveCount = elementCount;
			glPrimitiveType = GL_POINTS;
			break;

		case TRIANGLE_STRIP:
			primitiveCount = elementCount - 2;
			glPrimitiveType = GL_TRIANGLE_STRIP;
			break;

		case LINE_STRIP:
			primitiveCount = elementCount - 1;
			glPrimitiveType = GL_LINE_STRIP;
			break;

		case TRIANGLE_FAN:
			primitiveCount = elementCount - 2;
			glPrimitiveType = GL_TRIANGLE_FAN;
			break;
		}
	}

	void Graphics::Draw(PrimitiveType type, unsigned vertexStart, unsigned vertexCount)
	{
		if (!vertexCount)
			return;

		PrepareDraw();

		unsigned primitiveCount;
		GLenum glPrimitiveType;

		GetGLPrimitiveType(vertexCount, type, primitiveCount, glPrimitiveType);
		
		glDrawArrays(glPrimitiveType, vertexStart, vertexCount);

		numPrimitives_ += primitiveCount;
		++numBatches_;
	}

	void Graphics::DrawBatch3D(const Batch3D & batch, int idx)
	{
        auto& pss = batch.mat->GetAllPass()[idx];
        const auto& programAttrs = pss.programState->GetAttributeInfo();
        for (size_t i = 0; i < batch.vertexAttributList->size(); i++)
        {

            const auto & attr = (*batch.vertexAttributList)[i];
            SetVBO(attr.buffer->GetGpuObject());
            const auto& itr = programAttrs.find(attr.name);
            if (itr != programAttrs.end())
            {
                const auto& info = itr->second;
                glVertexAttribPointer(info.location, attr.compCount, attr.compType, GL_FALSE, attr.byteStride, (void*)attr.byteOffset);
				glCheckError();
                glEnableVertexAttribArray(info.location);
            }
        }

        if (batch.elementBuffer)
        {
            SetEBO(batch.elementBuffer->GetGpuObject());
			assert(batch.elementBuffer->GetDataType() != 0);
            glDrawElements(batch.mode, batch.count, batch.elementBuffer->GetDataType(), (void*)batch.start);
			glCheckError();
            SetEBO(0);
            
        }
        else
        {
             glDrawArrays(batch.mode, batch.start, batch.count);
        }
        SetVBO(0);
			
	}

    void Graphics::DrawSkyBox()
    {
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
        glDepthFunc(GL_LEQUAL);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glDepthFunc(GL_LESS);
    }

	void Graphics::PrepareDraw()
	{
        
		SetVBO(vertexBuffer_->GetGpuObject());

		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
		// color attribute
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(2 * sizeof(float)));
		glEnableVertexAttribArray(1);
		// texture coord attribute
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glEnableVertexAttribArray(2);

        glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	void Graphics::OnFrameStart(int width, int heigh)
	{
		numBatches_ = 0;
		numPrimitives_ = 0;
		if (!renderQued_)
		{
			renderQued_ = CreateRenderSurface(width, heigh);
		}

		if (renderQued_ && (renderQued_->width != width || renderQued_->height != heigh))
		{
			ResizeRenderSurface(renderQued_, width, heigh);
		}
		SetRenderSurface(renderQued_);
		ClearColor();
	}

	void Graphics::OnFrameEnd()
	{
		ApplyTopRenderSurface();
		glDisable(GL_DEPTH_TEST);
		
		if (!renderQued_)
			return;

		std::vector<std::string> defines;
		auto uiProgramState = context_->GetShaderCache()->GetOrCreate("Shaders/ScreenQuad.glsl", defines);
		unsigned textureID = renderQued_->colorAtt->GetTextureID();
		int textureIdx = 0;
		SetTexture(textureIdx, renderQued_->colorAtt);
		uiProgramState->SetUniform("screenTexture", &textureIdx);
		float frameBufSize[2] = { (float)renderQued_->width, (float)renderQued_->height };
		uiProgramState->SetUniform("frameBufSize", frameBufSize);
		SetShaders(uiProgramState);
		SetVBO(renderQuedVbo_);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
		glDrawArrays(GL_TRIANGLES, 0, 6);
		SetVBO(0);
	}

	RenderSurface* Graphics::CreateRenderSurface(int width, int height, bool shadowmap)
	{
		GLint drawFboId;
		glGetIntegerv(GL_FRAMEBUFFER_BINDING, &drawFboId);

		auto surface = new RenderSurface(context_);

		unsigned int framebuffer;
		glGenFramebuffers(1, &framebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
		surface->framebuffer = framebuffer;
		surface->width = width;
		surface->height = height;

		if (shadowmap)
		{
			auto tex = new Texture2D(context_);
			tex->SetName("cascde_shadw_map");
			tex->AddRef();
			surface->depthAtt = tex;

			tex->SetSize(1024, 1024, GL_DEPTH_COMPONENT);
			tex->SetData(0, 0, 1024, 1024, nullptr);

			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex->GetTextureID(), 0);
			glDrawBuffer(GL_NONE);
			glReadBuffer(GL_NONE);
			glBindFramebuffer(GL_FRAMEBUFFER, drawFboId);
            return surface;
		}

		unsigned int rbo;
		glGenRenderbuffers(1, &rbo);
		surface->rbo = rbo;

		auto tex = new Texture2D(context_);
		tex->SetName("RenderToTexture");
		tex->AddRef();
		surface->colorAtt = tex;
		
		if(width > 0 && height > 0)
		{
			tex->SetSize(width,height, GL_RGBA);
			tex->SetData(0, 0, width, height, nullptr);	
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex->GetTextureID(), 0);	
		}

		glBindRenderbuffer(GL_RENDERBUFFER, rbo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo); 
		glBindRenderbuffer(GL_RENDERBUFFER, 0);

		glBindFramebuffer(GL_FRAMEBUFFER, drawFboId);
		return surface;
	}

	void Graphics::ResizeRenderSurface(RenderSurface* surface, int width, int height)
	{
		assert(surface->colorAtt != nullptr);

		surface->Release();
		surface->width = width;
		surface->height = height;

		unsigned int framebuffer;
		glGenFramebuffers(1, &framebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
		surface->framebuffer = framebuffer;

		unsigned int rbo;
		glGenRenderbuffers(1, &rbo);
		surface->rbo = rbo;

		glCheckError();
		if (width > 0 && height > 0 && surface->colorAtt)
		{
			surface->colorAtt->ReSize(width, height, GL_RGBA);
			glCheckError();
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, surface->colorAtt->GetTextureID(), 0);
			glCheckError();
		}
		glCheckError();
		glBindRenderbuffer(GL_RENDERBUFFER, rbo);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glCheckError();
	}

	void Graphics::PushRenderSurface(RenderSurface* surface)
	{
		surface->AddRef();
		renderSurfaceStack_.push_back(surface);
		//glBindFramebuffer(GL_FRAMEBUFFER, surface->framebuffer);
	}

	void Graphics::PopRenderSurface()
	{
		RenderSurface* last = nullptr;
		if (renderSurfaceStack_.size() > 0)
		{
			last = renderSurfaceStack_.back();
			renderSurfaceStack_.pop_back();
		}
		
		if (renderSurfaceStack_.size() > 0)
		{
			const auto& surface = renderSurfaceStack_.back();
			glBindFramebuffer(GL_FRAMEBUFFER, surface->framebuffer);
		}
		else
		{
			glBindFramebuffer(GL_FRAMEBUFFER,0);
		}

		last->ReleaseRef();
	}

	RenderSurface* Graphics::GetCurRenderSurface()
	{
		if (renderSurfaceStack_.size() > 0)
		{
			 return renderSurfaceStack_.back();
		}

		return nullptr;
	}
	void Graphics::ResetRenderSurface()
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	void Graphics::ApplyTopRenderSurface()
	{
		if (renderSurfaceStack_.size() > 0)
		{
			const auto& surface = renderSurfaceStack_.back();
			SetRenderSurface(surface);
			ClearColor();
		}
		else
		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		}
	}
	
	void Graphics::SetRenderSurface(RenderSurface* surface)
	{
		if (!surface)
			return;

		if (!curSurface || curSurface->framebuffer != surface->framebuffer)
		{
			curSurface = surface;
			glBindFramebuffer(GL_FRAMEBUFFER, curSurface->framebuffer);
		}
	}

	const char* Graphics::GetTextureUnitName(int idx)
	{
		if (idx >= MAX_TEXTURE_UNITS)
		{
			return "";
		}

		return TextureUnitName[idx];
	}

	void Graphics::BindDepthTexture(Texture* tex)
	{
		if (!tex)
		{
			return;
		}

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tex->GetTarget(), tex->GetTextureID(), 0);
	}
	
	RenderSurface::RenderSurface(Context* context):Object(context)
	{
		
	}

	RenderSurface::~RenderSurface()
	{
		if (colorAtt)
		{
			colorAtt->ReleaseRef();
			colorAtt = nullptr;
		}
		if (depthAtt)
		{
			depthAtt->ReleaseRef();
			depthAtt = nullptr;
		}
		Release();
	}
	void RenderSurface::Release()
	{
		/*if (colorAtt)
		{
			colorAtt->ReleaseRef();
			colorAtt = nullptr;
		}*/

		if (framebuffer > 0)
		{
			glDeleteFramebuffers(1, &framebuffer);
			framebuffer = 0;
		}

		if (rbo > 0)
		{
			glDeleteRenderbuffers (1, &rbo);
			rbo = 0;
		}

	}
}
