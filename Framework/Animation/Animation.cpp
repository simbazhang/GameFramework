#include "Animation.h"
#include "glm/gtx/compatibility.hpp"
#include "GameObject.h"
#include <iostream>
using namespace Tiny2D;

Animation::Animation(Context* context) :Component(context)
{

}

Animation::~Animation()
{
	for (size_t i = 0; i < nodes_.size(); i++)
	{
		nodes_[i]->ReleaseRef();
	}

	for (auto& itr : animationStates_)
	{
		itr->ReleaseRef();
	}

	for (auto& itr : skins_)
	{
		delete itr;
	}
}

void Animation::GetBatch(std::vector<Batch3D>& batches)
{
}

void Animation::Update(float dt)
{
	for (size_t i = 0; i < animationStates_.size(); i++)
	{
		auto& states = animationStates_[i];
		states->curTime_ += dt;
		if (states->curTime_ > states->maxTime_)
		{
			if (states->loop_)
				states->curTime_ = 0.f;
			else
				continue;
		}

		for (size_t c = 0; c < states->channels_.size(); c++)
		{
			auto& chan = states->channels_[c];

			for (size_t n = 0; n < chan->animationTrack_.size(); n++)
			{
				auto& track = chan->animationTrack_[n];
				GameObject* target = nodes_[chan->nodeIdx_[n]];
				glm::vec4 data = track->GetData(states->curTime_);
				if (track->path_ == "rotation")
				{
					glm::quat q;
					q.x = data[0];
					q.y = data[1];
					q.z = data[2];
					q.w = data[3];
					target->SetRotation(q);
				}
				else if (track->path_ == "translation")
				{
					target->SetPosition(glm::vec3(data));
				}
				else if (track->path_ == "scale")
				{
					target->SetScale(glm::vec3(data));
				}
			}
		}
	}
}

void Animation::AddGameObject(GameObject* obj)
{
	if (obj)
	{
		obj->AddRef();
		nodes_.push_back(obj);
	}
}

void Animation::AddAnimationState(AnimationState* state)
{
	state->AddRef();
	animationStates_.push_back(state);
}

void Animation::AddSkin(Skin* skin)
{
	skins_.push_back(skin);
}

GameObject* Tiny2D::Animation::GetGameObjectByIdx(int idx)
{
	if (idx < 0 || idx >= nodes_.size())
	{
		return nullptr;
	}
	return nodes_[idx];
}

Skin* Tiny2D::Animation::GetCurSkin()
{
	if (skins_.size() > 0)
	{
		return skins_.back();
	}
	return nullptr;
}

Component* Animation::Clone()
{
	Animation* temp = new Animation(context_);
	for (size_t i = 0; i < skins_.size(); i++)
	{
		temp->AddSkin(skins_[i]->Clone());
	}

	for (size_t i = 0; i < animationStates_.size(); i++)
	{
		temp->AddAnimationState(animationStates_[i]->Clone());
	}
	return temp;
}

glm::vec4 AnimationTrack::GetData(float time)
{
	if (time_.size() <= 0)
	{
		assert(0);
	}

	if (time_.size() != data_.size())
	{
		assert(0);
	}

	if (time <= time_[0])
	{
		return data_[0];
	}

	unsigned index = 1;
	for (; index < time_.size(); ++index)
	{
		if (time < time_[index])
			break;
	}

	if (index >= time_.size() || interpolation_ == Interpolation::STEP)
		return data_[index - 1];
	else
	{
		float framedy = time_[index] - time_[index - 1];
		float pass = time - time_[index - 1];
		float t = pass / framedy;
		if (interpolation_ == Interpolation::LINEAR)
			return LinearInterpolation(index - 1, index, t);
		else
			// TODO
			return LinearInterpolation(index - 1, index, t);
	}


}

glm::vec4 Tiny2D::AnimationTrack::LinearInterpolation(unsigned index1, unsigned index2, float scaledTime) const
{
	if (path_ == "rotation")
	{
		// quat order is w.x,y,z data order x,y,z,w
		glm::quat rot1(data_[index1].w, data_[index1].x, data_[index1].y, data_[index1].z);
		glm::quat rot2(data_[index2].w, data_[index2].x, data_[index2].y, data_[index2].z);
		glm::quat out(glm::lerp(rot1, rot2, scaledTime));
		return glm::vec4(rot2.x, rot2.y, rot2.z, rot2.w);
	}
	else
	{
		return glm::lerp(data_[index1], data_[index2], scaledTime);
	}	
}

AnimationState::~AnimationState()
{
	for (auto& itr : channels_)
	{
		delete itr;
	}
}
