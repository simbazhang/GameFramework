#pragma once
#include "Component.h"
#include "Batch3D.h"
#include "glm/glm.hpp"

namespace Tiny2D
{
	enum class Interpolation
	{
		LINEAR,
		STEP,
	};
	class GameObject;
	class AnimationTrack
	{
	public:
		AnimationTrack() {};
		glm::vec4 GetData(float time);
		AnimationTrack(AnimationTrack&& r) noexcept :data_(std::move(r.data_)), time_(std::move(r.time_)), path_(std::move(r.path_)) {};
		glm::vec4 LinearInterpolation(unsigned index1, unsigned index2, float scaledTime) const;

		AnimationTrack* Clone()
		{
			AnimationTrack* temp = new AnimationTrack();
			temp->interpolation_ = interpolation_;
			temp->time_ = time_;
			temp->data_ = data_;
			temp->path_ = path_;
			return temp;
		}
	public:
		Interpolation interpolation_{ Interpolation::LINEAR };
		std::vector<float> time_{};
		std::vector<glm::vec4> data_{};
		std::string path_{};
	};

	class Channel
	{
	public:
		Channel() {};
		std::vector<int> nodeIdx_{};
		std::vector<AnimationTrack*> animationTrack_{};
		Channel(Channel&& r) noexcept :animationTrack_(std::move(r.animationTrack_)), nodeIdx_(std::move(r.nodeIdx_)) {};

		Channel* Clone()
		{
			Channel* temp = new Channel();
			temp->nodeIdx_ = nodeIdx_;
			for (size_t i = 0; i < animationTrack_.size(); i++)
			{
				temp->animationTrack_.push_back(animationTrack_[i]->Clone());
			}
			return temp;
		}
	};

	class Skin
	{
	public:
		Skin() {};
		std::vector<int> nodeIdx_{};
		std::vector<glm::mat4> inverseBindMatrices{};
		Skin* Clone() 
		{
			Skin* temp = new Skin();
			temp->nodeIdx_ = nodeIdx_;
			temp->inverseBindMatrices = inverseBindMatrices;
			return temp;
		}
	};

	class AnimationState : public RefCounted
	{
	public:
		~AnimationState();
		float maxTime_{};
		float curTime_{};
		std::string name_{};
		std::vector<Channel*> channels_{};
		bool loop_{ true };

		AnimationState* Clone()
		{
			AnimationState* temp = new AnimationState();
			temp->maxTime_ = maxTime_;
			temp->curTime_ = curTime_;
			temp->name_ = name_;
			for (size_t i = 0; i < channels_.size(); i++)
			{
				temp->channels_.push_back(channels_[i]->Clone());
			}
			
			return temp;
		}
	};

	class  Animation : public Component
	{
		OBJECT_DEFINE(Animation)
	public:
		explicit Animation(Context* context);
		~Animation() override;

		virtual void GetBatch(std::vector<Batch3D>& batches);
		virtual std::string GetComponentName() { return "Animation"; };
		
		virtual void Update(float dt);

		void AddGameObject(GameObject* obj);
		void AddAnimationState(AnimationState* channel);
		void AddSkin(Skin* skin);
		GameObject* GetGameObjectByIdx(int idx);

		Skin* GetCurSkin();

		virtual Component* Clone();
	private:
		std::vector<GameObject*> nodes_{};
		std::vector<AnimationState*> animationStates_{};
		std::vector<Skin*> skins_{};
	};
}