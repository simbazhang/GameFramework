#pragma once
#include "Object.h"
#include "UIElement.h"
#include "UIBatch.h"
#include <glm/glm.hpp>
#include "Component.h"
#include "Batch3D.h"
#include <map>

namespace Tiny2D {
    class VertexBuffer;
    class GameObject;
	class ProgramState;
	class Mesh;
    class Camera;
    class Light;
    class Texture2D;
    class DirLight;
    class PointLight;
    enum class RenderPass
    {
        ShadowPass,
        PreZPass,
        BasePass,
        RefractPass,
        MaxPassType
    };

    class Render : public Object
    {
    public:
        explicit Render(Context* context);
        ~Render();
        void Clear();
        void Update(float timeStep);
        void RenderUpdate();
        void RenderBatches();
        void RenderBatch(Batch3D& batch, Camera* camera, RenderPass passtype);
        void RenderDirShadow(Batch3D& batch, DirLight* light);
        void RenderPointShadow(Batch3D& batch, PointLight* light);
        void RenderSkyBox(Camera* camera);
        void RenderReflection(Camera* camera);
        void DebugDraw(UIElement* element);

        GameObject* GetRootObject() { return rootObject_; }

        void UpdateWinSize(glm::vec2 size);
        glm::vec2 GetWinSize() {
            return winSize_;
        }
        
        void ApplyLightUniform(GameObject* object, ProgramState* program);
        void RenderShadowMap();
        void RenderShadowCubeMap();
        void GetComponents(GameObject* root);
        void SetEnvIBLMap(Texture2D* tex);
        void SetEnvMap(Texture2D* tex);
        
        const std::vector<Light*>& GetLights(){return lights_;}
    private:
		std::vector<Batch3D> batches_{};
		std::vector<Component*> components_{};
        std::vector<Camera*> cameras_{};
        std::vector<Light*> lights_{};
		VertexBuffer* vertexBuffer_{};
		GameObject* rootObject_{};
		glm::vec2 winSize_{};
		glm::mat4x4 pProj_{};
		glm::mat4x4 pView_{};
        Texture2D* envIBLMap_{};
        Texture2D* envMap_{};
    };
}
