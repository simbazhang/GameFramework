#include "Material.h"
#include "File.h"
#include "Shader.h"
#include "Texture2D.h"
#include "Animation.h"
#include "Render.h"

using namespace Tiny2D;

Material::Material(Context* context) :Resource(context)
{
	Pass pass;
	pass.passIdx = (unsigned)RenderPass::PreZPass;
	pass.programState = context_->GetShaderCache()->Create("Shaders/PreZ.glsl", { });
	this->AddPass(pass);
}

Material::~Material()
{
	for (auto& itr : pass_)
	{
		itr.Release();
	}
}

bool Material::BeginLoad(File & source)
{
	SetName(source.GetName());
	source.Open();
	source.Seek(0);
	unsigned dataSize = source.GetSize();

	std::string context;
	context.resize(dataSize + 1);
//	source.Read(&context.at(0), dataSize);
//	_config = YAML::Load(context);
	return true;
}

bool Material::EndLoad()
{
	return true;
}

void Material::AddPass(const Pass & pass)
{
	if (pass.programState)
	{
		pass.programState->AddRef();
	}

	for (size_t i = 0; i < MAX_TEXTURE_UNITS; i++)
	{
		if (pass.textures[i])
		{
			pass.textures[i]->AddRef();
		}
	}
	pass_.push_back(pass);
}

void Material::SetAnimation(Animation* ani)
{
	if (ani == animation_)
	{
		return;
	}

	if (animation_)
	{
		animation_->ReleaseRef();
	}

	if (ani)
	{
		ani->AddRef();
	}


	animation_ = ani;
}

Material* Material::Clone()
{
	Material* temp = new Material(context_);
	temp->pass_ = pass_;
	if(animation_)
		temp->SetAnimation((Animation*)animation_->Clone());
	return temp;
}

void Pass::Release()
{
	if (programState)
	{
		programState->ReleaseRef();
	}

	for (size_t i = 0; i < MAX_TEXTURE_UNITS; i++)
	{
		if (textures[i])
		{
			textures[i]->ReleaseRef();
		}
	}
}
