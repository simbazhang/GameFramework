#pragma once
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include "Component.h"
#include "Graphics.h"

namespace Tiny2D
{
    class Camera : public Component
    {
    public:
        explicit Camera(Context* context);

        virtual void OnStart();
        virtual void OnDestory();

        virtual void OnEnable() {};
        virtual void OnDisable() {};

        virtual void GetBatch(std::vector<Batch3D>& batches);
        virtual std::string GetComponentName() { return "Camera"; };

        void SetOrtho(bool ortho);
        void SetOrthoParams(float param[4]);

        const glm::mat4& GetViewMatrix();
        const glm::mat4& GetProjMatrix();

        void SetViewMatrix(const glm::mat4& mat);
        void SetProjMatrix(const glm::mat4& mat);

        unsigned char GetMask() { return cameraMask_; }
        void SetMask(unsigned char mask) { cameraMask_ = mask; }

        void SetUseReflection(bool enable);
        bool UseReflection() { return enableReflect_; }
        void SetAspect(float aspect);
        float GetAspect() { return aspect_; }

        void SetNearFar(glm::vec2 nearfar);
        const glm::vec2& GetNearFar() { return nearFar_; };

        void OnWinSize(glm::vec2 winsize);

        void StartRender(glm::vec2 winsize);
        void EndRender();

        float GetFov() { return fovy_; }

        RenderSurface* GetReflectSurface() {return reflectSurface_;}
    private:
        glm::mat4 view_{};
        glm::mat4 reflectionView_{};
        glm::mat4 proj_{};
        unsigned char cameraMask_{ 0x1 };
        unsigned int order_{ 0 };
        bool enableReflect_{ false };
        
        // reflect plane
        glm::vec3 normal_{0.f, 1.f, 0.f};
        float d_{};

        float aspect_{};
        glm::vec2 nearFar_{};
        float fovy_{ 60.f };
        bool projDirty_{ true };
        bool isOrtho_{ false };

        //��������
        float orthoParams_[4]{};

        RenderSurface* reflectSurface_{};
    };
}