#pragma once
#include "Object.h"
#include "Resource.h"
#include "GraphicsDefs.h"

namespace Tiny2D
{
	class ProgramState;
	class Texture2D;
	struct Pass
	{
	public:
		Pass() {
			memset(textures, 0, sizeof(void*) * MAX_TEXTURE_UNITS);
		}
		unsigned passIdx;
		ProgramState* programState;
		unsigned blend;
		bool depthwrite;
		unsigned depthtest;
		unsigned cullMode;
		unsigned fillMode;
		Texture2D* textures[MAX_TEXTURE_UNITS]{};
		void Release();
	};

	class Context;
	class Animation;
	class  Material : public Resource
	{
		OBJECT_DEFINE(Material)
	public:
		Material(Context* context);
		virtual ~Material();

		virtual bool BeginLoad(File& file);
		virtual bool EndLoad();

		void AddPass(const Pass& pass);
		std::vector<Pass>& GetAllPass() { return pass_; }

		void SetAnimation(Animation* ani);
		Animation* GetAnimation() { return animation_; }
		Material* Clone();
	private:
		Animation* animation_{};
		std::vector<Pass> pass_{};
	};
}
