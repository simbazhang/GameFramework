#include "Light.h"
#include "GameObject.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Camera.h"
#include "Texture2D.h"
#include <iostream>
using namespace Tiny2D;

Light::Light(Context* context):Component(context)
{
	AttributeBind* posBind = new AttributeBind();
	posBind->name = "Color";
	posBind->value = GetLightColor();
	posBind->get = [=]() {
		posBind->value = GetLightColor();
	};
	posBind->set = [=]() {
		SetColor(posBind->value.v.color);
	};
	attributeBind_.push_back(posBind);

	AttributeBind* factorBind = new AttributeBind();
	factorBind->name = "Factor";
	factorBind->value = GetLightFactor();
	factorBind->get = [=]() {
		factorBind->value = GetLightFactor();
	};
	factorBind->set = [=]() {
		SetLightFactor(factorBind->value.v.f);
	};
	attributeBind_.push_back(factorBind);
}

void Light::OnStart()
{

}

void Light::OnDestory()
{
	
}

void Light::GetBatch(std::vector<Batch3D>& batches)
{
}

void Tiny2D::Light::EnableShadowMap(bool enable)
{
	shadowMapEnable_ = enable;
}


Tiny2D::DirLight::DirLight(Context* context):Light(context)
{
	virtualCamera = new Camera(context);
}

Tiny2D::DirLight::~DirLight()
{
	virtualCamera->ReleaseRef();
}

const glm::mat4& Tiny2D::DirLight::GetLightProjection()
{
	const auto& param = shadowOrthoProjInfo_;
	glm::mat4 lightProjection = glm::ortho(param.l, param.r, param.b, param.t, param.n, param.f);
	glm::mat4 lightView = GetLightViewMat();
	glm::mat4 lightSpaceMatrix = lightProjection * lightView;
	lightProjection_ = lightSpaceMatrix;
	return lightProjection_;
}

Camera* DirLight::GetVirtualCamera()
{
	const auto& param = shadowOrthoProjInfo_;
	glm::mat4 lightProjection = glm::ortho(param.l, param.r, param.b, param.t, param.n, param.f);
	glm::mat4 lightView = GetLightViewMat();
	glm::mat4 lightSpaceMatrix = lightProjection * lightView;
	lightProjection_ = lightSpaceMatrix;
	virtualCamera->SetProjMatrix(lightProjection);
	virtualCamera->SetViewMatrix(lightView);
	return virtualCamera;
}

void DirLight::CalcOrthoProjs(Camera* mainCam)
{
	glm::mat4 Cam = mainCam->GetViewMatrix();
	glm::mat4 CamInv = glm::inverse(Cam);
	glm::mat4 LightM = GetLightViewMat();
	float ar = mainCam->GetAspect();
	float fov = mainCam->GetFov();
	float tanHalfHFOV = tanf(glm::radians(fov / 2.0f));
	glm::vec2 nf = mainCam->GetNearFar();
}

glm::mat4 Tiny2D::DirLight::GetLightViewMat()
{
	auto& mat = gameObject_->GetTransform();
	auto inv = glm::inverse(mat);
	return inv;
}

glm::vec3 Tiny2D::DirLight::GetLightDIr()
{
	auto& mat = gameObject_->GetTransform();
	glm::vec4 front = mat * glm::vec4(0, 0, -1, 1);
	glm::vec4 o = mat * glm::vec4(0, 0, 0, 1);
	glm::vec3 dir = glm::vec3(front - o);
	return dir;
}

Tiny2D::PointLight::PointLight(Context* context):Light(context)
{
	cubeTex_ = new TextureCube(context);
	cubeTex_->SetName("cascde_shadw_map");
	cubeTex_->AddRef();

	cubeTex_->SetSize(1024, 1024, GL_DEPTH_COMPONENT);
	cubeTex_->SetData();

	GLfloat aspect = (GLfloat)1024 / (GLfloat)1024;
	GLfloat near = 1.0f;
	GLfloat far = 25.0f;
	glm::mat4 shadowProj = glm::perspective(90.0f, aspect, near, far);
	
	auto lightPos = GetOwner()->GetPosition();
	shadowTransforms_.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms_.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(-1.0, 0.0, 0.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms_.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 1.0, 0.0), glm::vec3(0.0, 0.0, 1.0)));
	shadowTransforms_.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0, -1.0, 0.0), glm::vec3(0.0, 0.0, -1.0)));
	shadowTransforms_.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 0.0, 1.0), glm::vec3(0.0, -1.0, 0.0)));
	shadowTransforms_.push_back(shadowProj * glm::lookAt(lightPos, lightPos + glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, -1.0, 0.0)));
}
