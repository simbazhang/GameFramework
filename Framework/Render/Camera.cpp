#include "Camera.h"
#include "GameObject.h"
#include "Render.h"

using namespace Tiny2D;

Camera::Camera(Context* context):Component(context)
{
	reflectSurface_ = context_->GetGraphics()->CreateRenderSurface(0, 0);
}

void Camera::OnStart()
{
}

void Camera::OnDestory()
{
}

void Camera::GetBatch(std::vector<Batch3D>& batches)
{
}

void Camera::SetOrtho(bool ortho)
{
	if (isOrtho_ != ortho)
	{
		isOrtho_ = ortho;
		MarkDirty();
	}
}

void Camera::SetOrthoParams(float param[4])
{
	if (!param)
	{
		return;
	}

	memcpy(orthoParams_, param, sizeof(float) * 4);
}

const glm::mat4& Camera::GetViewMatrix()
{
	if (gameObjectDirty_&& gameObject_)
	{
		auto& mat = gameObject_->GetTransform();
		view_ = glm::inverse(mat);
		reflectionView_ = view_;
		gameObjectDirty_ = false;
	}

	return enableReflect_ ? reflectionView_ : view_;
}

const glm::mat4& Camera::GetProjMatrix()
{
	if (projDirty_)
	{
		//FIX ME 未初始化特殊设置下
		if (aspect_ == 0.f)
		{
			auto size = context_->GetRender()->GetWinSize();
			aspect_ = size.x / size.y;
		}
		if (!isOrtho_)
		{
			proj_ = glm::perspective(glm::radians(fovy_), aspect_, nearFar_.x, nearFar_.y);
		}
		else
		{
			proj_ = glm::ortho(orthoParams_[0], orthoParams_[1], orthoParams_[2], orthoParams_[3], nearFar_.x, nearFar_.y);
		}
		projDirty_ = false;
	}
	
	return proj_;
}

void Camera::SetViewMatrix(const glm::mat4& mat)
{
	view_ = mat;
	projDirty_ = false;
}

void Camera::SetProjMatrix(const glm::mat4& mat)
{
	proj_ = mat;
	projDirty_ = false;
}

void Camera::SetUseReflection(bool enable)
{
	enableReflect_ = true;
}

void Tiny2D::Camera::SetAspect(float aspect)
{
	aspect_ = aspect;
	projDirty_ = true;

}

void Tiny2D::Camera::SetNearFar(glm::vec2 nearfar)
{
	nearFar_ = nearfar;
	projDirty_ = true;
}

void Tiny2D::Camera::OnWinSize(glm::vec2 winsize)
{
	SetAspect(winsize.x / winsize.y);
}

void Camera::StartRender(glm::vec2 winsize)
{
	if (enableReflect_)
	{
		if (reflectSurface_->width != winsize.x || reflectSurface_->height != winsize.y)
		{
			context_->GetGraphics()->ResizeRenderSurface(reflectSurface_, winsize.x, winsize.y);
		}
		context_->GetGraphics()->SetRenderSurface(reflectSurface_);
		context_->GetGraphics()->ClearColor();
	}
}

void Camera::EndRender()
{
	context_->GetGraphics()->SetRenderSurface(context_->GetGraphics()->GetScreenQuad());
}
