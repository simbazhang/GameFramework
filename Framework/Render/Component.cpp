#include "Component.h"
#include "Texture.h"
#include <glm/gtc/matrix_transform.hpp>
#include "UI.h"
#include "GameObject.h"
namespace Tiny2D
{
    Component::Component(Context* context) :Object(context)
    {

    }

    Component::~Component()
    {

    }

    void Component::OnInit(GameObject* owner)
    {
        if (owner != gameObject_ && owner)
        {
            gameObject_ = owner;
            OnStart();
        }

    }

    void Component::SetEnabled(bool enable)
    {
        if (enable_ != enable)
        {
            enable_ = enable;
            if (enable_)
            {
                OnEnable();
            }
            else
            {
                OnDisable();
            }
        }

    }


}
