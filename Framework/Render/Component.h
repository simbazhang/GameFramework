#pragma once
#include "Object.h"
#include "Batch3D.h"

namespace Tiny2D
{
	class GameObject;
	class Mesh;
	class  Component : public Object
	{
		OBJECT_DEFINE(Component)
	public:
		explicit Component(Context* context);
		~Component() override;
		/// Return UI rendering batches.
		void SetEnabled(bool enable);
		bool GetEnabled() { return enable_; }

		void OnInit(GameObject* owner);

		virtual void OnStart() {};
		virtual void OnDestory() {};

		virtual void OnEnable() {};
		virtual void OnDisable() {};

		virtual Component* Clone() { return nullptr; };

		virtual void Update(float dt) {};
		virtual void GetBatch(std::vector<Batch3D>& batches) {};
		virtual std::string GetComponentName() { return "Component"; };

		GameObject* GetOwner() { return gameObject_; }

		void MarkDirty() { gameObjectDirty_ = true; }
	protected:
		int order_{};
		unsigned flag_{};
		bool enable_{ true };
		GameObject* gameObject_{};
		bool gameObjectDirty_{true};
	};
}
