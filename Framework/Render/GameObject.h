#pragma once

#include "Object.h"
#include "UIBatch.h"
#include "Rect.h"
#include<memory>
#include "Color.h"
#include "AttributeBind.hpp"
#include "Component.h"
#include <glm/gtc/quaternion.hpp>

namespace Tiny2D {

    class  GameObject : public Object
    {
        OBJECT_DEFINE(GameObject);
    public:
        /// Construct.
        explicit GameObject(Context* context);
        /// Destruct.
        ~GameObject() override;
       
    
        void SetPosition(const glm::vec3& position);
        const glm::vec3& GetPosition() const { return position_; }
        const glm::vec3& GetWorldPosition();

        const glm::vec3& GetScale() const { return scale_; }
        const glm::vec3& GetRotation() const { return rotation_; }
        void SetScale(const glm::vec3& scale);
        void SetScale(float scale);
        void SetRotation(const glm::vec3& angle);
        void SetRotation(const glm::quat& angle);

        virtual const glm::mat4x4& GetTransform() const;
        void MarkDirty();

        const std::vector<GameObject* >& GetChildren() const { return children_; }
        void AddChild(GameObject* element);
        void RemoveChild(GameObject* element);

        GameObject* GetParent() { return parent_; }
        std::string GetName() { return name_; }
        void SetName(std::string name);
        
        const std::vector<Component*>& GetComponents() { return components_; }
        void AddComponent(Component* comp);
        void RemoveComponent(Component* target);

        const unsigned char& GetMask() { return cameraMask_ ;}

        GameObject* Clone();
    protected:
     
        std::vector<Component*> components_{};
        glm::vec3 position_{0.f}; 
        glm::vec3 scale_{1.0f};
        glm::vec3 rotation_{0.f};

        mutable glm::vec3 worldPosition_{ 0.f };
           
        mutable bool positionDirty_{true};

        GameObject* parent_{};
        std::vector<GameObject*> children_;
    
        mutable glm::mat4x4 transform_;
        std::string name_;
        unsigned char cameraMask_{ 0xff };
    };
}
