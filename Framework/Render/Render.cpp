#include "Render.h"
#include "VertexBuffer.h"
#include "Graphics.h"
#include <glm/gtc/matrix_transform.hpp>
#include "Shader.h"
#include <glm/gtc/type_ptr.hpp>
#include "GameObject.h"
#include "Mesh.h"
#include "Camera.h"
#include "Material.h"
#include "Light.h"
#include "Texture2D.h"
#include "GraphicsDefs.h"
#include "Animation.h"
#include <fmt/chrono.h>
#include <fmt/core.h>

namespace Tiny2D {

    const float skyboxVertices[] = {
        // positions
        -1.0f,  1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

        -1.0f,  1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f,  1.0f
    };

	Render::Render(Context* context):Object(context),pProj_(1.f), pView_(1.f)
	{
        vertexBuffer_ = new VertexBuffer(context_);
		rootObject_ = new GameObject(context_);
        rootObject_->SetName("SCENEROOT");
        vertexBuffer_->SetVertexSize(12);
        vertexBuffer_->SetSize(36);
        vertexBuffer_->SetData(skyboxVertices);
	}
    Render::~Render()
    {
        if(vertexBuffer_)
            delete vertexBuffer_;
        if (rootObject_)
            delete rootObject_;

        if (envIBLMap_)
        {
            envIBLMap_->ReleaseRef();
        }
        if (envMap_)
        {
            envMap_->ReleaseRef();
        }
    }
	void Render::Clear()
	{
	}

	void Render::Update(float timeStep)
	{
        components_.clear();
		cameras_.clear();
		lights_.clear();

        GetComponents(rootObject_);
        for (int i = 0; i < components_.size(); i++) {
            const std::string cname(std::move(components_[i]->GetComponentName()));
			if (cname == "Camera")
			{
				cameras_.push_back((Camera*)components_[i]);
			}
			else if (cname == "Light")
			{
				lights_.push_back((Light*)components_[i]);
			}
            components_[i]->Update(timeStep);
        }
	}

	void Render::RenderUpdate()
	{
		batches_.clear();
        for (int i = 0; i < components_.size(); i++) {
			components_[i]->GetBatch(batches_);
        }
	}

	void Render::RenderBatches()
	{
        auto graphics = context_->GetGraphics();
        graphics->SetDepthTest(true);
        for (size_t n = 0; n < cameras_.size(); n++)
        {
            Camera* curCam = cameras_[n];
            curCam->StartRender(winSize_);         
            for (size_t i = 0; i < batches_.size(); i++)
            {
                RenderBatch(batches_[i], curCam, RenderPass::PreZPass);
            }
            for (size_t i = 0; i < batches_.size(); i++)
            {
                RenderBatch(batches_[i], curCam, RenderPass::BasePass);
            }
            RenderSkyBox(curCam);
            if (curCam->UseReflection())
            {
                for (size_t i = 0; i < batches_.size(); i++)
                {
                    RenderBatch(batches_[i], curCam, RenderPass::RefractPass);
                }
            }
            curCam->EndRender();
        }
	}

    void Render::RenderBatch(Batch3D& batch, Camera* camera, RenderPass passtype)
    {
        auto& mat = batch.mat;
        if (!mat || !batch.gameObject)
        {
            return;
        }

        if ((camera->GetMask() & batch.gameObject->GetMask()) == 0)
        {
            return;
        }

        auto graphics = context_->GetGraphics();
        auto& passes = mat->GetAllPass();
        auto anim = mat->GetAnimation();
        for (size_t n = 0; n < passes.size(); n++)
        {
            Pass& curPass = passes[n];
            RenderPass curpasstype = RenderPass(curPass.passIdx);
            if (! curPass.programState ||  curpasstype != passtype)
            {
                continue;
            }
           
            if (anim)
            {
                Skin* skin = anim->GetCurSkin();
                if (!skin)
                {
                    continue;
                }

                static float skinmat[128 * 16] = {};
                for (size_t i = 0; i < skin->nodeIdx_.size(); i++)
                {
                    glm::mat4 im = skin->inverseBindMatrices[i];
                    glm::mat4 m = anim->GetGameObjectByIdx(skin->nodeIdx_[i])->GetTransform() * im;
                    float* matArray = glm::value_ptr(m);
                    for (size_t n = 0; n < 16; n++)
                    {
                        skinmat[i * 16 + n] = matArray[n];
                    }
                }
                curPass.programState->SetUniform("bones", skinmat);
            }
            else
            {
                curPass.programState->SetUniform("cModel", glm::value_ptr(batch.gameObject->GetTransform()));
            }
           
            auto& view = camera->GetViewMatrix();
            if(camera->GetOwner())
            {
                auto campos = camera->GetOwner()->GetWorldPosition();
                curPass.programState->SetUniform("cCamPos", glm::value_ptr(campos));
            }
            
            auto& proj = camera->GetProjMatrix();
            curPass.programState->SetUniform("cView", glm::value_ptr(view));
            curPass.programState->SetUniform("cProj", glm::value_ptr(proj));
            

            if (envIBLMap_)
            {
                context_->GetGraphics()->SetTexture(TU_ENVIRONMENT_IBL, envIBLMap_);
                int textureidx = TU_ENVIRONMENT_IBL;
                curPass.programState->SetUniform("sIrradianceMap", &textureidx);
            }

            if (envMap_)
            {
                context_->GetGraphics()->SetTexture(TU_ENVIRONMENT, envMap_);
                int textureidx = TU_ENVIRONMENT;
                curPass.programState->SetUniform("sSkybox", &textureidx);
            }

            for (size_t ii = 0; ii < MAX_TEXTURE_UNITS; ii++)
            {
                if (curPass.textures[ii])
                {
                    graphics->SetTexture(ii, curPass.textures[ii]);
                    const char* name = graphics->GetTextureUnitName(ii);
                    curPass.programState->SetUniform(name, &ii);
                }
            }
            
            glCheckError();
            ApplyLightUniform(batch.gameObject, curPass.programState);

            for (auto& light : lights_)
            {
                if (light->GetRenderSurface() && passtype != RenderPass::ShadowPass)
                {
                   
                }
            }
            
            graphics->SetShaders(curPass.programState);
            glCheckError();
            graphics->DrawBatch3D(batch, n);
        }
    }

    void Render::RenderDirShadow(Batch3D& batch, Tiny2D::DirLight* light)
    {

    }

    void Render::RenderPointShadow(Batch3D& batch, Tiny2D::PointLight* light)
    {
    }

	void Render::DebugDraw(UIElement* element)
	{
        
	}

    
    void Render::GetComponents(GameObject *element)
    {
        const auto& children = element->GetChildren();
        if (children.empty())
            return;

        auto i = children.begin();
        while (i != children.end())
        {
            auto& comps = (*i)->GetComponents();
            if(comps.size() > 0)
            {
                components_.insert(components_.end(), comps.begin(), comps.end());
            }
            GetComponents(*i);
            ++i;
        }
    }

	void Render::UpdateWinSize(glm::vec2 size)
	{
		winSize_ = size;
        pProj_ = glm::perspective(glm::radians(60.f), (float)winSize_.x/(float)winSize_.y, 1.f, 50.f);
        for (size_t i = 0; i < cameras_.size(); i++)
        {
            cameras_[i]->OnWinSize(winSize_);
        }
	}


	void Render::ApplyLightUniform(GameObject* object, ProgramState* program)
	{
		if (lights_.size() > 0) {
            if (lights_[0]->GetComponentName() == "DirLight")
            {
                DirLight* dlight = (DirLight*)lights_[0];
                program->SetUniform("cLightPositions", glm::value_ptr(dlight->GetOwner()->GetWorldPosition()));
                Color c = dlight->GetLightColor() * dlight->GetLightFactor();
                program->SetUniform("cLightColors", c.Data());
                program->SetUniform("cLightDir", glm::value_ptr(-(dlight->GetLightDIr())));
                float time = context_->GetTime()->GetElapsedTime();
                program->SetUniform("cElapsedTime", &time);
            }
			
		}
	}

    void Render::RenderShadowMap()
    {
        if (cameras_.size() == 0)
            return;
        Camera* cam = cameras_[0];
        for(int i = 0; i < lights_.size(); i++)
        {
            auto light = lights_[i];
            if(light->GetLightType() != LightType::DirectionalLight)
            {
                continue;
            }
            DirLight* dirlight = (DirLight*)light;
            if (!dirlight->GetRenderSurface())
            {
                auto shadow = context_->GetGraphics()->CreateRenderSurface(1024, 1024, true);
                dirlight->SetRenderSurface(shadow);
            }

            dirlight->CalcOrthoProjs(cam);

            auto graphics = context_->GetGraphics();
            graphics->SetDepthTest(true);
            glDisable(GL_CULL_FACE);
            glCullFace(GL_FRONT);
            
            graphics->SetRenderSurface(dirlight->GetRenderSurface());
            glDrawBuffer(GL_NONE);
            glReadBuffer(GL_NONE);
            glBindRenderbuffer(GL_RENDERBUFFER, 0);
            graphics->ClearDepth();
            glViewport(0, 0, 1024, 1024);
            for (size_t m = 0; m < batches_.size(); m++)
            {
                RenderBatch(batches_[m], dirlight->GetVirtualCamera(), RenderPass::ShadowPass);
            }
            

            glDisable(GL_CULL_FACE);
            glCullFace(GL_BACK);
        }
    }

    void Render::RenderShadowCubeMap()
    {
      
    }

    void Render::SetEnvIBLMap(Texture2D* tex)
    {
        if(envIBLMap_ == tex)
            return;
        
        if(envIBLMap_)
        {
            envIBLMap_->ReleaseRef();
        }
        
        envIBLMap_ = tex;
        if (envIBLMap_) {
            envIBLMap_->AddRef();
        }
        
    }

    void Render::SetEnvMap(Texture2D* tex)
    {
        if(envMap_ == tex)
            return;
        
        if(envMap_)
        {
            envMap_->ReleaseRef();
        }
        
        envMap_ = tex;
        if (envMap_) {
            envMap_->AddRef();
        }
        
    }

    void Render::RenderSkyBox(Camera* camera)
    {
        if(envMap_)
        {
            auto program = context_->GetShaderCache()->GetOrCreate("Shaders/SkyBox.glsl", {});
            context_->GetGraphics()->SetTexture(TU_ENVIRONMENT, envMap_);
            int textureidx = TU_ENVIRONMENT;
            program->SetUniform("sSkybox", &textureidx);
            
            auto& view = camera->GetViewMatrix();
            auto viewNoMove = glm::mat4(glm::mat3(view));
            program->SetUniform("cView", glm::value_ptr(viewNoMove));
            program->SetUniform("cProj", glm::value_ptr(pProj_));
            context_->GetGraphics()->SetShaders(program);
            context_->GetGraphics()->SetVBO(vertexBuffer_->GetGpuObject());
            context_->GetGraphics()->DrawSkyBox();
        }
    }

    void Render::RenderReflection(Camera* camera)
    {
       
    }
}
