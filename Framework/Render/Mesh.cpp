#include "Mesh.h"
#include "ElementBuffer.h"
#include "VertexBuffer.h"
#include "Context.h"
#include "Shader.h"
#include <glm/gtc/type_ptr.hpp>
#include "Material.h"
namespace Tiny2D
{
    Mesh::Mesh(Context* context) :Component(context)
    {

    }

    Mesh::~Mesh()
    {
        if (elementBuffer_)
        {
            elementBuffer_->ReleaseRef();
        }

        for (auto& attr : vertexAttributList_)
        {
            attr.buffer->ReleaseRef();
        }

		if (material_)
		{
			material_->ReleaseRef();
		}
    }
	void Mesh::GetBatch(std::vector<Batch3D>& batches)
	{
		Batch3D batch(gameObject_, material_, mode_, elementBuffer_, &vertexAttributList_);
		batch.start = start_;
		batch.count = count_;
		batches.emplace_back(batch);
	}
	void Mesh::SetElementBuffer(ElementBuffer* elem)
	{
        if (elementBuffer_ && elem != elementBuffer_)
        {
            elementBuffer_->ReleaseRef();
        }

        if (elem != elementBuffer_)
        {
            elementBuffer_ = elem;
            elementBuffer_->AddRef();
        }
	}
	void Mesh::AddVertextAttribut(const VertextAttribut & attr)
	{
        attr.buffer->AddRef();
        vertexAttributList_.push_back(attr);
	}
	
	void Mesh::SetMaterial(Material * mat)
	{
		if (mat == material_)
		{
			return;
		}

		if (mat)
		{
			if (material_)
			{
				material_->ReleaseRef();
			}
			material_ = mat;
			material_->AddRef();
		}
	}
	Component* Mesh::Clone()
	{
		Mesh* temp = new Mesh(context_);
		temp->SetElementBuffer(elementBuffer_);
		temp->SetMaterial(material_->Clone());
		temp->SetMode(mode_);
		temp->SetRang(start_, count_);

		for (size_t i = 0; i < vertexAttributList_.size(); i++)
		{
			temp->AddVertextAttribut(vertexAttributList_[i]);
		}

		return temp;
	}
}
