#pragma once

#include <glm/glm.hpp>
#include <vector>
#include "GraphicsDefs.h"
#include <string>
namespace Tiny2D {
	class GameObject;
	class ProgramState;
	class VertexBuffer;
	class ElementBuffer;
	class Material;
	struct VertextAttribut
	{
		VertexBuffer* buffer; //vbo
		std::string name; //属性名字
		size_t byteOffset;  // 在buffer中的起始偏移
		int compSIze;  // 数据的字节数 float类型就 = 4
		int compType; // 对应gl里的type
		size_t count; // vertext数量
		int compCount; //数据保护数量 vec3 就是= 3
		int byteStride;
	};
	class Batch3D
	{
	public:
		Batch3D(GameObject* gameobject, Material* mat, unsigned mode, ElementBuffer*, std::vector<VertextAttribut>*vertexAttribut);
		~Batch3D();
		Material* mat;
		GameObject* gameObject;
		std::vector<VertextAttribut>* vertexAttributList{};
		ElementBuffer* elementBuffer{};
		unsigned mode{ 4 };
		unsigned start{};
		unsigned count{};
	};
}
