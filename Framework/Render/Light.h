#pragma once
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include "Component.h"
#include "Color.h"
#include "Texture.h"

#define NUM_FRUSTUM_CORNERS 8

namespace Tiny2D
{
    class RenderSurface;
    class Camera;
    enum class LightType { DirectionalLight, PointLight, Spotlight};
    class Texture2D;
    class Texture;
    class TextureCube;
    struct OrthoProjInfo
    {
        float l;
        float r;
        float b;
        float t;
        float n;
        float f;
    };

    class Light : public Component
    {
    public:

        explicit Light(Context* context);

        virtual void OnStart();
        virtual void OnDestory();

        virtual void OnEnable() {};
        virtual void OnDisable() {};

        virtual void GetBatch(std::vector<Batch3D>& batches);
        virtual std::string GetComponentName() { return "Light"; }
        const Color& GetLightColor() { return lightColor_; }
        void SetColor(const Color& c) { lightColor_ = c; }
        float GetLightFactor() { return lightFactor_; }
        void SetLightFactor(float factor) { lightFactor_ = factor; }

        void EnableShadowMap(bool enable);

        LightType GetLightType() {
            return lightType_;
        }
        RenderSurface* GetRenderSurface() { return shadowSurface_; }
        void SetRenderSurface(RenderSurface* surface) { shadowSurface_ = surface; }

        virtual Texture* GetDepthTex(int n) { return nullptr; };
        virtual int GetPassNum() { return 1; };
    private:
        bool shadowMapEnable_{false};
        RenderSurface* shadowSurface_{nullptr};
        Color lightColor_{};
        float lightFactor_{ 1 };
        LightType lightType_{};

    };

    class DirLight : public Light
    {
    public:

        explicit DirLight(Context* context);
        ~DirLight();
        virtual std::string GetComponentName() { return "DirLight"; }
        Camera* GetVirtualCamera();

        void CalcOrthoProjs(Camera* mainCam);
        const glm::mat4& GetLightProjection();

        glm::mat4 GetLightViewMat();
        glm::vec3 GetLightDIr();

    private:
        OrthoProjInfo shadowOrthoProjInfo_;
        Camera* virtualCamera{};
        glm::mat4 lightProjection_{};
    };

    class PointLight : public Light
    {
    public:
        explicit PointLight(Context* context);
        virtual std::string GetComponentName() { return "PointLight"; }
    private:
        TextureCube* cubeTex_{ nullptr };
        std::vector<glm::mat4> shadowTransforms_{};
    };
}
