#pragma once
#include "Component.h"
#include "Batch3D.h"
namespace Tiny2D
{
	class VertexBuffer;
	class ElementBuffer;
	class Material;
	class  Mesh : public Component
	{
		OBJECT_DEFINE(Mesh)
	public:
		explicit Mesh(Context* context);
		~Mesh() override;

		virtual void GetBatch(std::vector<Batch3D>& batches);
		virtual std::string GetComponentName() { return "Mesh"; };

		void SetElementBuffer(ElementBuffer* elem);
		void AddVertextAttribut(const VertextAttribut& attr);
		void SetMode(unsigned mode) { mode_ = mode; }
		void SetRang(unsigned start, unsigned count) {
			start_ = start;  
			count_ = count;
		}
		void SetMaterial(Material* mat);
		Material* GetMaterial() { return material_; }

		virtual Component* Clone();
	private:
		std::vector<VertextAttribut> vertexAttributList_{};
		ElementBuffer* elementBuffer_{};
		unsigned mode_{4};
		unsigned count_{ 0 };
		unsigned start_{ 0 };
		Material* material_{};
	};
}