#include "Batch3D.h"
#include "Mesh.h"
#include "ElementBuffer.h"
#include "VertexBuffer.h"
#include "GameObject.h"
#include "Shader.h"
#include "Material.h"
namespace Tiny2D {
	Batch3D::Batch3D(GameObject * object, Material * p, unsigned mode, ElementBuffer * ebuff,  std::vector<VertextAttribut>* attribut):
		gameObject(object),
		mat(p),
		mode(mode),
		elementBuffer(ebuff),
		vertexAttributList(attribut)
	{

	}

	Batch3D::~Batch3D()
	{
	}
}
