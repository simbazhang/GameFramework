#include "UIElement.h"
#include <algorithm>
#include <iostream>
#include "UI.h"
#include <glm/gtc/matrix_transform.hpp>
#include "Resource.h"

namespace Tiny2D {
    static bool CompareUIElements(const UIElement* lhs, const UIElement* rhs)
    {
        return lhs->GetPriority() < rhs->GetPriority();
    }

    UIElement::UIElement(Context* context) :Object(context), transform_(1.0f)
    {
        name_ = "UIElement";

        AttributeBind* nameBind = new AttributeBind();
        nameBind->name = "Name";
        nameBind->value = name_;
        nameBind->get = [=]() {
            nameBind->value = name_;
        };
        nameBind->set = [=]() {
            name_ = nameBind->value.v.str;
        };
        attributeBind_.push_back(nameBind);

        ACCESSOR_ATTRIBUTE("Position", glm::vec2, GetPosition, SetPosition);
        ACCESSOR_ATTRIBUTE("EnableAnchor", bool, GetEnableAnchor, SetEnableAnchor);
        ACCESSOR_ATTRIBUTE("Size", glm::vec2, GetSize, SetSize);
        ACCESSOR_ATTRIBUTE("Pivot", glm::vec2, GetPivot, SetPivot);
        ACCESSOR_ATTRIBUTE("AnchorMin", glm::vec2, GetMinAnchor, SetMinAnchor);
        ACCESSOR_ATTRIBUTE("AnchorMax", glm::vec2, GetMaxAnchor, SetMaxAnchor);
        ACCESSOR_ATTRIBUTE("MinOffset", glm::vec2, GetMinOffset, SetMinOffset);
        ACCESSOR_ATTRIBUTE("MaxOffset", glm::vec2, GetMaxOffset, SetMaxOffset);
        ACCESSOR_ATTRIBUTE("Rotation", float, GetRotation, SetRotation);
        ACCESSOR_ATTRIBUTE("Scale", glm::vec2, GetScale, SetScale);
    }

    UIElement::~UIElement()
    {
        for (size_t i = 0; i < attributeBind_.size(); i++)
        {
            delete attributeBind_[i];
        }
        for (int i = 0; i < components_.size(); i++) {
            components_[i]->ReleaseRef();
        }

        for (int i = 0; i < children_.size(); i++) {
            children_[i]->ReleaseRef();
        }
    }

    void UIElement::GetBatches(std::vector<UIBatch>& batches, std::vector<float>& vertexData, const IntRect& currentScissor)
    {
    }

    void UIElement::SetPosition(const glm::vec2& position)
    {
        if (position != position_)
        {
            position_ = position;
            OnPositionSet(position);
            MarkDirty();
        }
    }

    void UIElement::SortChildren()
    {
        if (sortOrderDirty_)
        {
            std::sort(children_.begin(), children_.end(), CompareUIElements);
            sortOrderDirty_ = false;
        }
    }

    void UIElement::SetSize(const glm::vec2& size)
    {
        if (size != size_)
        {
            size_ = size;
            OnSizeSet(size);
            MarkDirty();
        }
    }

    void UIElement::SetEnableAnchor(bool enable)
    {
        enableAnchor_ = enable;
        if (enableAnchor_)
            UpdateAnchoring();
    }

    void UIElement::SetMinOffset(const glm::vec2& offset)
    {
        if (offset != minOffset_)
        {
            minOffset_ = offset;
            if (enableAnchor_)
                UpdateAnchoring();
        }
    }

    void UIElement::SetMaxOffset(const glm::vec2& offset)
    {
        if (offset != maxOffset_)
        {
            maxOffset_ = offset;
            if (enableAnchor_)
                UpdateAnchoring();
        }
    }

    void UIElement::SetMinAnchor(const glm::vec2& anchor)
    {
        if (anchor != anchorMin_)
        {
            anchorMin_ = anchor;
            if (enableAnchor_)
                UpdateAnchoring();
        }
    }


    void UIElement::SetMaxAnchor(const glm::vec2& anchor)
    {
        if (anchor != anchorMax_)
        {
            anchorMax_ = anchor;
            if (enableAnchor_)
                UpdateAnchoring();
        }
    }

    void UIElement::SetPivot(const glm::vec2& pivot)
    {
        if (pivot != pivot_)
        {
            pivot_ = pivot;
            MarkDirty();
        }
    }

    void UIElement::SetScale(const glm::vec2& scale)
    {
        if (scale != scale_)
        {
            scale_ = scale;
            MarkDirty();
        }
    }

    void UIElement::SetScale(float x, float y)
    {
        SetScale(glm::vec2(x, y));
    }

    void UIElement::SetScale(float scale)
    {
        SetScale(glm::vec2(scale, scale));
    }

    void UIElement::SetRotation(float angle)
    {
        if (angle != rotation_)
        {
            rotation_ = angle;
            MarkDirty();
        }
    }

    float UIElement::GetDerivedOpacity() const
    {
        if (!useDerivedOpacity_)
            return opacity_;

        if (opacityDirty_)
        {
            derivedOpacity_ = opacity_;
            const UIElement* parent = parent_;

            while (parent)
            {
                derivedOpacity_ *= parent->opacity_;
                parent = parent->parent_;
            }

            opacityDirty_ = false;
        }

        return derivedOpacity_;
    }

    const Color& UIElement::GetDerivedColor() const
    {
        if (derivedColorDirty_)
        {
            derivedColor_ = colors_[C_TOPLEFT];
            derivedColor_.a_ *= GetDerivedOpacity();
            derivedColorDirty_ = false;
        }

        return derivedColor_;
    }

    void UIElement::SetColor(const Color& color)
    {
        for (auto& cornerColor : colors_)
            cornerColor = color;
        colorGradient_ = false;
        derivedColorDirty_ = true;
    }

    const glm::vec2& UIElement::GetScreenPosition() const
    {
        GetTransform();
        return screenPosition_;
    }

    const glm::mat4x4& UIElement::GetTransform() const
    {
        if (positionDirty_)
        {
            glm::mat4x4 parentTransform(1.0f);

            if (parent_)
            {
                parentTransform = parent_->GetTransform();
            }
            else
            {
                parentTransform = context_->GetUI()->GetView();
            }

            glm::mat4x4 mainTransform(1.0);
            mainTransform = glm::translate(mainTransform, glm::vec3((float)position_.x, (float)position_.y, 0.0f));
            mainTransform = glm::rotate(mainTransform, glm::radians(rotation_), glm::vec3(0.f, 0.f, 1.f));
            mainTransform = glm::scale(mainTransform, glm::vec3(scale_.x, scale_.y, 1.f));
           
            transform_ = parentTransform * mainTransform;
            positionDirty_ = false;

            glm::vec4 topLeftCorner = transform_ * glm::vec4(0.f, 0.f, 0.f, 1.f);
            screenPosition_ = glm::vec2(topLeftCorner.x, topLeftCorner.y);
        }

        return transform_;
    }

    void UIElement::SetColor(Corner corner, const Color& color)
    {
        colors_[corner] = color;
        colorGradient_ = false;
        derivedColorDirty_ = true;

        for (unsigned i = 0; i < MAX_UIELEMENT_CORNERS; ++i)
        {
            if (i != corner && colors_[i] != colors_[corner])
                colorGradient_ = true;
        }
    }

    void UIElement::MarkDirty()
    {
        positionDirty_ = true;
        opacityDirty_ = true;
        derivedColorDirty_ = true;

        for (size_t i = 0; i < attributeBind_.size(); i++)
        {
            attributeBind_[i]->inited = false;
        }

        for (auto i = children_.begin(); i != children_.end(); ++i)
            (*i)->MarkDirty();
    }

    void UIElement::Remove()
    {
        if (parent_)
            parent_->RemoveChild(this);
    }

    void UIElement::AddChild(UIElement* element)
    {
        if (!element || element == this || element->parent_ == this || parent_ == element)
            return;
      
        element->AddRef();
        children_.push_back(element);

        sortOrderDirty_ = true;
        if(element->parent_)
        {
            element->parent_->RemoveChild(element);
        }
        element->parent_ = this;
        element->MarkDirty();
    }

    void UIElement::RemoveChild(UIElement* element)
    {
        for (size_t i = 0; i < children_.size(); i++)
        {
            if (children_[i] == element)
            {
                sortOrderDirty_ = true;
                children_.erase(children_.begin() + i);
                element->ReleaseRef();
                return;
            }
        }
    }

    void UIElement::SetName(std::string name)
    {
        if (name.length() < 24)
        {
            name_ = name;
        }
    }

    void UIElement::UpdateAnchoring()
    {
        if (parent_ && enableAnchor_)
        {
            glm::vec2 newSize;
            newSize.x =  (parent_->size_.x * Clamp(anchorMax_.x - anchorMin_.x, 0.0f, 1.0f)) + maxOffset_.x - minOffset_.x;
            newSize.y =  (parent_->size_.y * Clamp(anchorMax_.y - anchorMin_.y, 0.0f, 1.0f)) + maxOffset_.y - minOffset_.y;

            if (position_ != minOffset_)
                SetPosition(minOffset_);
            if (size_ != newSize)
                SetSize(newSize);
        }
    }

    glm::vec2 UIElement::ScreenToElement(const glm::vec2& screenPosition)
    {
        //return screenPosition - GetScreenPosition();
        glm::vec4 floatPos((float)screenPosition.x, (float)screenPosition.y, 0.0f, 1.0f);
        glm::vec4 transformedPos = glm::inverse(GetTransform()) * floatPos;
        return glm::vec2(transformedPos.x, transformedPos.y);
    }

    glm::vec2 UIElement::ElementToScreen(const glm::vec2& position)
    {
        //return position + GetScreenPosition();
        glm::vec4 floatPos((float)position.x, (float)position.y, 0.0f, 1.0f);
        glm::vec4 transformedPos = GetTransform() * floatPos;
        return glm::vec2(transformedPos.x, transformedPos.y);
    }

    bool UIElement::IsInside(glm::vec2 position, bool isScreen)
    {
        if (isScreen)
            position = ScreenToElement(position);
        auto lbx = -size_.x * pivot_.x;
        auto lby = -size_.y * pivot_.y;
        return position.x >= lbx && position.y >= lby && position.x < lbx + size_.x && position.y < lby + size_.y;
    }

    void UIElement::AddComponent(UIComponent* comp)
    {
        if (!comp) {
            return;
        }
        if(comp->GetOwner() == this)
        {
            return;
        }
        
        comp->AddRef();
        if (comp->GetOwner()) {
            comp->GetOwner()->RemoveComponent(comp);
        }
        
        components_.push_back(comp);
        comp->OnInit(this);
        comp->OnStart();
        comp->OnEnable();
        
    }

    void UIElement::RemoveComponent(UIComponent *target)
    {
        for (int i = 0; i < components_.size(); i++) {
            if (components_[i] == target) {
                components_.erase(components_.begin() + i);
                target->OnDisable();
                target->OnDestory();
                target->ReleaseRef();
                
            }
        }
    }

    glm::vec2 UIElement::GetCornerPosition(Corner corner)
    {
        const glm::vec2& size = GetSize();
        glm::vec2 out = {};
        if (corner == Corner::C_TOPLEFT)
        {
            out.x = -size.x * pivot_.x;
            out.y = -size.y * pivot_.y + size.y;
        }
        else  if (corner == Corner::C_TOPRIGHT)
        {
            out.x = -size.x * pivot_.x + size.x;
            out.y = -size.y * pivot_.y + size.y;
        }
        else  if (corner == Corner::C_BOTTOMLEFT)
        {
            out.x = -size.x * pivot_.x;
            out.y = -size.y * pivot_.y;
        }
        else  if (corner == Corner::C_BOTTOMRIGHT)
        {
            out.x = -size.x * pivot_.x + size.x;
            out.y = -size.y * pivot_.y;
        }

        return out;
    }
}

