#include "Sprite.h"
#include "Texture.h"
#include <glm/gtc/matrix_transform.hpp>
#include "UI.h"

namespace Tiny2D
{

extern const char* blendModeNames[];
extern const char* horizontalAlignments[];
extern const char* verticalAlignments[];
extern const char* UI_CATEGORY;

SpriteComponent::SpriteComponent(Context* context) :
    UIComponent(context),
    imageRect_(IntRect::ZERO),
    blendMode_(BLEND_ALPHA)
{
   
}

SpriteComponent::~SpriteComponent() = default;


void SpriteComponent::GetBatches(std::vector<UIBatch>& batches, std::vector<float>& vertexData, const IntRect& currentScissor)
{
    if (!texture_)
        return;
    if(element_ && !element_->IsVisible())
    {
        return;
    }
    
    const glm::vec2& size = element_->GetSize();
    const glm::vec2& pivot_ = element_->GetPivot();
    UIBatch
        batch(element_, blendMode_, currentScissor, texture_, &vertexData);

    batch.AddQuad(element_->GetTransform(), -size.x * pivot_.x, -size.y * pivot_.y, size.x, size.y, imageRect_.left_, imageRect_.top_, imageRect_.right_ - imageRect_.left_,
        imageRect_.bottom_ - imageRect_.top_);

    UIBatch::AddOrMerge(batch, batches);
}


void SpriteComponent::SetTexture(Texture* texture)
{
    texture_ = texture;
    if (imageRect_ == IntRect::ZERO)
        SetFullImageRect();
}

void SpriteComponent::SetImageRect(const IntRect& rect)
{
    if (rect != IntRect::ZERO)
        imageRect_ = rect;
}

void SpriteComponent::SetFullImageRect()
{
    if (texture_)
        SetImageRect(IntRect(0, 0, texture_->GetWidth(), texture_->GetHeight()));
}

void SpriteComponent::SetBlendMode(BlendMode mode)
{
    blendMode_ = mode;
}

}
