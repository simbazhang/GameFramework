//
// Copyright (c) 2008-2020 the Urho3D project.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#pragma once
#include <glm/glm.hpp>
#include <vector>
#include "GraphicsDefs.h"
#include "Rect.h"
#include "Color.h"

namespace Tiny2D
{
class UIElement;
class Texture;
static const unsigned UI_VERTEX_SIZE = 8;

/// %UI rendering draw call.
class UIBatch
{
public:
    /// Construct with defaults.
    UIBatch();
    /// Construct.
    UIBatch(UIElement* element, BlendMode blendMode, const IntRect& scissor, Texture* texture, std::vector<float>* vertexData);

    /// Set new color for the batch. Overrides gradient.
    void SetColor(const Color& color, bool overrideAlpha = false);
    /// Restore UI element's default color.
    void SetDefaultColor();
    
    void AddQuad(const glm::mat4x4& transform, float x, float y, float width, float height, float texOffsetX, float texOffsetY, float texWidth = 0.f,
        float texHeight = 0.f);

    /// Merge with another batch.
    bool Merge(const UIBatch& batch);
    /// Return an interpolated color for the UI element.
    Color GetColor(float x, float y);

    /// Add or merge a batch.
    static void AddOrMerge(const UIBatch& batch, std::vector<UIBatch>& batches);

    /// Element this batch represents.
    UIElement* element_{};
    /// Blending mode.
    BlendMode blendMode_{BLEND_ALPHA};
    /// Scissor rectangle.
    IntRect scissor_;
    /// Texture.
    Texture* texture_{};
    /// Inverse texture size.
    glm::vec2 invTextureSize_{glm::vec2(0,0)};
    /// Vertex data.
    std::vector<float>* vertexData_{};
    /// Vertex data start index.
    unsigned vertexStart_{};
    /// Vertex data end index.
    unsigned vertexEnd_{};
    /// Current color. By default calculated from the element.
    unsigned color_{};
    /// Gradient flag.
    bool useGradient_{};
    /// Custom material.
    ///Material* customMaterial_{};

    /// Position adjustment vector for pixel-perfect rendering. Initialized by UI.
    static glm::vec3 posAdjust;
};

}
