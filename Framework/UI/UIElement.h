#pragma once

#include "Object.h"
#include "UIBatch.h"
#include "Rect.h"
#include<memory>
#include "Color.h"
#include "AttributeBind.hpp"
#include "UIComponent.h"

namespace Tiny2D {
    enum Corner
    {
        C_TOPLEFT = 0,
        C_TOPRIGHT,
        C_BOTTOMLEFT,
        C_BOTTOMRIGHT,
        MAX_UIELEMENT_CORNERS
    };

    class  UIElement : public Object
    {
        OBJECT_DEFINE(UIElement);
    public:
        /// Construct.
        explicit UIElement(Context* context);
        /// Destruct.
        ~UIElement() override;
       
        virtual void GetBatches(std::vector<UIBatch>& batches, std::vector<float>& vertexData, const IntRect& currentScissor);

        void SetPosition(const glm::vec2& position);
        void SortChildren();
        const glm::vec2& GetPosition() const { return position_; }

        void SetSize(const glm::vec2& size);
        const glm::vec2& GetSize() const { return size_; }

        void SetEnableAnchor(bool enable);
        bool GetEnableAnchor() const { return enableAnchor_; }

        void SetMinOffset(const glm::vec2& offset);
        void SetMaxOffset(const glm::vec2& offset);
        void SetMinAnchor(const glm::vec2& anchor);
        void SetMaxAnchor(const glm::vec2& anchor);

       const glm::vec2& GetMinOffset() { return minOffset_; };
       const glm::vec2& GetMaxOffset() { return maxOffset_; };
       const glm::vec2& GetMinAnchor() { return anchorMin_; };
       const glm::vec2& GetMaxAnchor() { return anchorMax_; };

        void SetPivot(const glm::vec2& pivot);
        const glm::vec2& GetPivot() const { return pivot_; }

        const glm::vec2& GetScale() const { return scale_; }
        float GetRotation() const { return rotation_; }
        void SetScale(const glm::vec2& scale);
        void SetScale(float x, float y);
        void SetScale(float scale);
        void SetRotation(float angle);
        /// Return opacity.
        float GetOpacity() const { return opacity_; }
        bool IsVisible() const { return visible_; }
        int GetPriority() const { return priority_; }
        /// Return derived opacity (affected by parent elements). If UseDerivedOpacity is false, returns same as element's own opacity.
        float GetDerivedOpacity() const;
        const Color& GetDerivedColor() const;
        bool HasColorGradient() const { return colorGradient_; }
        void SetColor(Tiny2D::Corner corner, const Tiny2D::Color& color);
        void SetColor(const Tiny2D::Color& color);
        const Color& GetColor(Corner corner) const { return colors_[corner]; }
        virtual const glm::vec2& GetScreenPosition() const;
        virtual const glm::mat4x4& GetTransform() const;
        void MarkDirty();
        void Remove();
        /// events
        /// React to position change.
        virtual void OnPositionSet(const glm::vec2& newPosition) { }
        virtual void OnSizeSet(const glm::vec2& newSize) { }
        const std::vector<UIElement* >& GetChildren() const { return children_; }
        void AddChild(UIElement* element);
        void RemoveChild(UIElement* element);

        virtual glm::vec2 ScreenToElement(const glm::vec2& screenPosition);

        virtual  glm::vec2 ElementToScreen(const glm::vec2& position);

        bool IsInside(glm::vec2 position, bool isScreen);

        glm::vec2 GetCornerPosition(Corner corner);

        UIElement* GetParent() { return parent_; }
        std::string GetName() { return name_; }
        void SetName(std::string name);
        
        const std::vector<UIComponent*>& GetComponents(){return components_;}
        void AddComponent(UIComponent* comp);
        void RemoveComponent(UIComponent* target);
    protected:
        void UpdateAnchoring();


        glm::vec2 position_{0, 0};
        glm::vec2 size_{100, 100};

        bool visible_{ true };
        bool enableAnchor_{false};
        glm::vec2 anchorMin_{0.5f, 0.5f};
        glm::vec2 anchorMax_{0.5f, 0.5f};
        glm::vec2 pivot_{0.5f, 0.5f};
        glm::vec2 minOffset_{0.f, 0.f};
        glm::vec2 maxOffset_{0.f, 0.f};

        glm::vec2 scale_{1.0f, 1.0f};
        float rotation_{0.f};
        
        mutable glm::vec2 screenPosition_{};

        mutable bool positionDirty_{true};
        mutable bool opacityDirty_{true};
        mutable bool derivedColorDirty_{true};

        int priority_{ 0 };
        float opacity_{1.0f};
        mutable float derivedOpacity_{};
        mutable Color derivedColor_;
        /// Use derived opacity flag.
        bool useDerivedOpacity_{ true };
        bool colorGradient_{};

        UIElement* parent_{};
        std::vector<UIElement*> children_;
        bool sortOrderDirty_{};

        Color colors_[MAX_UIELEMENT_CORNERS];
        
        mutable glm::mat4x4 transform_;
        std::string name_;
        
        std::vector<UIComponent*> components_;
    };
}
