#include "UIComponent.h"
#include "Texture.h"
#include <glm/gtc/matrix_transform.hpp>
#include "UI.h"
#include "UIElement.h"
namespace Tiny2D
{
    UIComponent::UIComponent(Context* context):Object(context)
    {

    }

    UIComponent::~UIComponent()
    {
        
    }

    void UIComponent::OnInit(UIElement* owner)
    {
        if (owner != element_ && owner)
        {
            element_ = owner;
            OnStart();
        }
        
    }

    void UIComponent::SetEnabled(bool enable)
    {
        if(enable_ != enable)
        {
            enable_ = enable;
            if(enable_)
            {
                OnEnable();
            }
            else
            {
                OnDisable();
            }
        }
        
    }


}
