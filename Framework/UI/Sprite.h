#pragma once
#include "UIComponent.h"

namespace Tiny2D
{

class  SpriteComponent : public UIComponent
{
    OBJECT_DEFINE(SpriteComponent)
public:
    explicit SpriteComponent(Context* context);
    ~SpriteComponent() override;

    void GetBatches(std::vector<UIBatch>& batches, std::vector<float>& vertexData, const IntRect& currentScissor) override;

    void SetTexture(Texture* texture);
    void SetImageRect(const IntRect& rect);
    void SetFullImageRect();
    void SetBlendMode(BlendMode mode);

    Texture* GetTexture() const { return texture_; }
    const IntRect& GetImageRect() const { return imageRect_; }

    BlendMode GetBlendMode() const { return blendMode_; }
protected:
    Texture* texture_;
    IntRect imageRect_;
    BlendMode blendMode_;
};

}
