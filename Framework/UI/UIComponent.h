#pragma once
#include "Object.h"
#include "UIBatch.h"
namespace Tiny2D
{
    static unsigned COMP_UPDATE = 1 << 0;
    static unsigned COMP_RENDER = 1 << 1;
    static unsigned COMP_EVENT = 1 << 2;
    class UIElement;
    class  UIComponent : public Object
    {
        OBJECT_DEFINE(UIComponent)
        public: 
            explicit UIComponent(Context* context);
            ~UIComponent() override;
            /// Return UI rendering batches.
            void SetEnabled(bool enable);
            bool GetEnabled(){return enable_;}

            void OnInit(UIElement* owner);
            
            virtual void OnStart(){};
            virtual void OnDestory(){};

            virtual void OnEnable(){};
            virtual void OnDisable(){};

            virtual void GetBatches(std::vector<UIBatch>& batches, std::vector<float>& vertexData, const IntRect& currentScissor){};
            virtual void Update(float dt){};
        
            UIElement* GetOwner(){return element_;}
        protected:
            int order_{};
            unsigned flag_{};
            UIElement* element_{};
            bool enable_{true};
    };  
}
