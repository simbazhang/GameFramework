#pragma once
#include "Object.h"
#include "UIElement.h"
#include "UIBatch.h"
#include <glm/glm.hpp>
#include "UIComponent.h"

namespace Tiny2D {
    class VertexBuffer;
    class UI : public Object
    {
    public:
        explicit UI(Context* context);
        ~UI();

        void Clear();
        void Update(float timeStep);
        void SetVertexData(VertexBuffer* dest, const std::vector<float>& vertexData, unsigned numVertices);
        void RenderUpdate();
        void Render();
        void Render(VertexBuffer* buffer, const std::vector<UIBatch>& batches, unsigned batchStart, unsigned batchEnd);
        void DebugDraw(UIElement* element);

        void GetBatches(std::vector<Tiny2D::UIBatch>& batches, std::vector<float>& vertexData, Tiny2D::UIElement* element, Tiny2D::IntRect currentScissor);

        UIElement* GetRootElement() { return rootElement_; }

        void UpdateWinSize(glm::vec2 size);

        void GetElementAt(UIElement*& result, UIElement* current, const glm::vec2& position);

        bool ToucheTest(UIElement* element, const glm::vec2& position);
        
        void SetUIOffset(glm::vec2 offset);
        void SetUIScale(float scale);
        float GetUIScale() { return uiScale_; }
        const glm::vec2& GetUIOffset() { return offset_; }

        const glm::mat4x4& GetView() { return pView_; }
        void GetUIComponents(UIElement* root);

        void UpdateViewMat();
    private:
        std::vector<UIBatch> batches_;
        std::vector<UIComponent*> components_;
        std::vector<float> vertexData_;
        VertexBuffer* vertexBuffer_;
        UIElement* rootElement_;
        glm::vec2 winSize_;
        glm::mat4x4 pProj_;
        glm::mat4x4 pView_;
        glm::vec2 offset_;
        float uiScale_;
    };
}
