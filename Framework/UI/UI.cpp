#include "UI.h"
#include "VertexBuffer.h"
#include "Graphics.h"
#include <glm/gtc/matrix_transform.hpp>
#include "Shader.h"
#include <glm/gtc/type_ptr.hpp>

namespace Tiny2D {

	UI::UI(Context* context):Object(context),pProj_(1.f), uiScale_(1.f),offset_(0.f, 0.f), pView_(1.f)
	{
		vertexBuffer_ = new VertexBuffer(context_);
		rootElement_ = new UIElement(context_);
        rootElement_->SetName("UIROOT");
	}
	UI::~UI()
	{
		delete vertexBuffer_;
		delete rootElement_;
	}
	void UI::Clear()
	{
	}

	void UI::Update(float timeStep)
	{
        components_.clear();
        GetUIComponents(rootElement_);
        for (int i = 0; i < components_.size(); i++) {
            components_[i]->Update(timeStep);
        }
	}

	void UI::SetVertexData(VertexBuffer* dest, const std::vector<float>& vertexData, unsigned numVertices)
	{
		if (vertexData.empty())
			return;

		// Update quad geometry into the vertex buffer
		// Resize the vertex buffer first if too small or much too large

		if (dest->GetVertexCount() < numVertices || dest->GetVertexCount() > numVertices * 2)
			dest->SetSize(numVertices, true);

		dest->SetData(&vertexData[0]);
	}

	void UI::RenderUpdate()
	{
		batches_.clear();
		vertexData_.clear();
		const glm::vec2& rootSize = rootElement_->GetSize();
		const glm::vec2& rootPos = rootElement_->GetPosition();
		// Note: the scissors operate on unscaled coordinates. Scissor scaling is only performed during render
		IntRect currentScissor = IntRect(rootPos.x, rootPos.y, rootPos.x + rootSize.x, rootPos.y + rootSize.y);
		//if (rootElement_->IsVisible())
		//	GetBatches(batches_, vertexData_, rootElement_, currentScissor);
        for (int i = 0; i < components_.size(); i++) {
            components_[i]->GetBatches(batches_, vertexData_, currentScissor);
        }
	}

	void UI::Render()
	{
		//context_->GetGraphics()->ApplyTopRenderSurface();
		//context_->GetGraphics()->ClearColor(0.2f, 0.2f, 0.2f);
		//context_->GetGraphics()->OnFrameStart();
		//context_->GetGraphics()->ApplyViewport();
		unsigned count = vertexData_.size() / UI_VERTEX_SIZE;
		SetVertexData(vertexBuffer_, vertexData_, count);
		Render(vertexBuffer_, batches_, 0, batches_.size());
		//context_->GetGraphics()->ResetRenderSurface();
	}

	void UI::Render(VertexBuffer* buffer, const std::vector<UIBatch>& batches, unsigned batchStart, unsigned batchEnd)
	{

		if (batches.empty())
			return;

		auto graphics = context_->GetGraphics();
		graphics->SetCullMode(CULL_CCW);

		std::vector<std::string> defines;
		defines.push_back("DIFFMAP");
		auto uiProgramState = context_->GetShaderCache()->GetOrCreate("Shaders/Basic.glsl", defines);
		graphics->SetVertexBuffer(buffer);

		uiProgramState->SetUniform("cViewProj", glm::value_ptr(pProj_));
		for (unsigned i = batchStart; i < batchEnd; ++i)
		{
			const UIBatch& batch = batches[i];
			if (batch.vertexStart_ == batch.vertexEnd_)
				continue;

			int textureID = 0;
			graphics->SetTexture(0, batch.texture_);
			uiProgramState->SetUniform("sDiffMap", &textureID);
			graphics->SetShaders(uiProgramState);
		
			graphics->Draw(TRIANGLE_LIST, batch.vertexStart_ / UI_VERTEX_SIZE,
				(batch.vertexEnd_ - batch.vertexStart_) / UI_VERTEX_SIZE);
		}
	}

	void UI::DebugDraw(UIElement* element)
	{
	}

    
    void UI::GetUIComponents(UIElement *element)
    {
        element->SortChildren();
        const auto& children = element->GetChildren();
        if (children.empty())
            return;

        auto i = children.begin();
        while (i != children.end())
        {
            auto& comps = (*i)->GetComponents();
            if(comps.size() > 0)
            {
                components_.insert(components_.end(), comps.begin(), comps.end());
            }
            GetUIComponents(*i);
            ++i;
        }
    }
	void UI::UpdateViewMat()
	{
		glm::mat4x4 mainTransform(1.0);
		mainTransform = glm::translate(mainTransform, glm::vec3((float)winSize_.x * 0.5f + offset_.x, (float)winSize_.y * 0.5f + offset_.y, 0.0f));
		mainTransform = glm::scale(mainTransform, glm::vec3(uiScale_, uiScale_, 1.f));
		pView_ = mainTransform;
		if (rootElement_)
		{
			rootElement_->MarkDirty();
		}
	}

	void UI::GetBatches(std::vector<UIBatch>& batches, std::vector<float>& vertexData, UIElement* element, IntRect currentScissor)
	{
		element->SortChildren();
		const auto& children = element->GetChildren();
		if (children.empty())
			return;

		auto i = children.begin();
		while (i != children.end())
		{
			(*i)->GetBatches(batches, vertexData, currentScissor);
			if ((*i)->IsVisible())
				GetBatches(batches, vertexData, *i, currentScissor);
			++i;
		}

	}
	void UI::UpdateWinSize(glm::vec2 size)
	{
		winSize_ = size;
		pProj_ = glm::ortho(0.0f, (float)size.x, 0.0f, (float)size.y, -100.0f, 100.0f);
		rootElement_->SetSize(winSize_);
		UpdateViewMat();
	}

	void UI::GetElementAt(UIElement*& result, UIElement* current, const glm::vec2& position)
	{
		if (!current)
			return;

		current->SortChildren();
		const std::vector<UIElement*>& children = current->GetChildren();
		
        if(children.size() > 0)
        {
            for (int i = children.size()-1; i >=0; i--)
            {
                UIElement* element = children[i];
                GetElementAt(result, element, position);
            }
        }
		
        
        if (!result && current!= rootElement_ && current->IsInside(position, true))
        {
            result = current;
        }
	}

    bool UI::ToucheTest(UIElement* element, const glm::vec2& position)
    {
        if (!element)
            return false;
        if (element->IsInside(position, true))
        {
            return true;
        }
        
        return false;
    }

	void UI::SetUIOffset(glm::vec2 offset)
	{
		if (offset != offset_)
		{
			offset_ = offset;
			UpdateViewMat();
		}
	}
	void UI::SetUIScale(float scale)
	{
		if (scale != uiScale_)
		{
			uiScale_ = scale;
			UpdateViewMat();
		}
	}
}
