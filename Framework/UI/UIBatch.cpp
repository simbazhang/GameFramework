#include "UIElement.h"
#include "Texture.h"
namespace Tiny2D
{

    glm::vec3 UIBatch::posAdjust(0.0f, 0.0f, 0.0f);

    UIBatch::UIBatch()
    {
        SetDefaultColor();
    }

    UIBatch::UIBatch(UIElement* element, BlendMode blendMode, const IntRect& scissor, Texture* texture, std::vector<float>* vertexData) :     // NOLINT(modernize-pass-by-value)
        element_(element),
        blendMode_(blendMode),
        scissor_(scissor),
        texture_(texture),
        invTextureSize_(texture ? glm::vec2(1.0f / (float)texture->GetWidth(), 1.0f / (float)texture->GetHeight()) : glm::vec2(1.f, 1.f)),
        vertexData_(vertexData),
        vertexStart_(vertexData->size()),
        vertexEnd_(vertexData->size())
    {
        SetDefaultColor();
    }

    void UIBatch::SetColor(const Color& color, bool overrideAlpha)
    {
        if (!element_)
            overrideAlpha = true;

        useGradient_ = false;
        color_ =
            overrideAlpha ? color.ToUInt() : Color(color.r_, color.g_, color.b_, color.a_ * element_->GetDerivedOpacity()).ToUInt();
    }

    void UIBatch::SetDefaultColor()
    {
        if (element_)
        {
            color_ = element_->GetDerivedColor().ToUInt();
            useGradient_ = element_->HasColorGradient();
        }
        else
        {
            color_ = 0xffffffff;
            useGradient_ = false;
        }
    }

    void UIBatch::AddQuad(const glm::mat4x4& transform, float x, float y, float width, float height, float texOffsetX, float texOffsetY,
        float texWidth, float texHeight)
    {
        Color colors[4];

        colors[0] = GetColor(x, y); //topleft
        colors[1] = GetColor(x + width, y); //topR
        colors[2] = GetColor(x, y + height); //bL
        colors[3] = GetColor(x + width, y + height); //bR

        glm::vec4 quad[4];
        quad[0] = (transform * glm::vec4((float)x, (float)y, 0.0f, 1.f));
        quad[1] = (transform * glm::vec4((float)x + (float)width, (float)y, 0.0f, 1.f));
        quad[2] = (transform * glm::vec4((float)x, (float)y + (float)height, 0.0f, 1.f));
        quad[3] = (transform * glm::vec4((float)x + (float)width, (float)y + (float)height, 0.0f, 1.f));

        float leftUV = ((float)texOffsetX) * invTextureSize_.x;
        float topUV = ((float)texOffsetY) * invTextureSize_.y;
        float rightUV = ((float)(texOffsetX + (texWidth ? texWidth : width))) * invTextureSize_.x;
        float bottomUV = ((float)(texOffsetY + (texHeight ? texHeight : height))) * invTextureSize_.y;

        glm::vec2 uvs[4];
        uvs[0] = glm::vec2(leftUV, topUV);
        uvs[1] = glm::vec2(rightUV, topUV);
        uvs[2] = glm::vec2(leftUV, bottomUV);
        uvs[3] = glm::vec2(rightUV, bottomUV);

        unsigned begin = vertexData_->size();
        vertexData_->resize(begin + 6 * UI_VERTEX_SIZE);
        float* dest = &(vertexData_->at(begin));
        vertexEnd_ = vertexData_->size();

        int index[6] = { 0, 2, 1, 1, 2, 3};
        int vertexIdx = 0;

        for (size_t i = 0; i < 6; i++)
        {
            int vidx = index[i];
            dest[vertexIdx] = quad[vidx].x;    vertexIdx++;
            dest[vertexIdx] = quad[vidx].y;    vertexIdx++;
            
            dest[vertexIdx] = colors[vidx].r_;    vertexIdx++;
            dest[vertexIdx] = colors[vidx].g_;    vertexIdx++;
            dest[vertexIdx] = colors[vidx].b_;    vertexIdx++;
            dest[vertexIdx] = colors[vidx].a_;    vertexIdx++;

            dest[vertexIdx] = uvs[vidx].x;    vertexIdx++;
            dest[vertexIdx] = uvs[vidx].y;    vertexIdx++;

        }
    }

    bool UIBatch::Merge(const UIBatch& batch)
    {
        if (batch.blendMode_ != blendMode_ ||
            batch.scissor_ != scissor_ ||
            batch.texture_ != texture_ ||
            batch.vertexData_ != vertexData_ ||
            batch.vertexStart_ != vertexEnd_)
            return false;

        vertexEnd_ = batch.vertexEnd_;
        return true;
    }

    Color UIBatch::GetColor(float x, float y)
    {
        const glm::vec2& size = element_->GetSize();

        if (size.x && size.y)
        {
            float cLerpX = Clamp(x / (float)size.x, 0.0f, 1.0f);
            float cLerpY = Clamp(y / (float)size.y, 0.0f, 1.0f);

            Color topColor = element_->GetColor(C_TOPLEFT).Lerp(element_->GetColor(C_TOPRIGHT), cLerpX);
            Color bottomColor = element_->GetColor(C_BOTTOMLEFT).Lerp(element_->GetColor(C_BOTTOMRIGHT), cLerpX);
            Color color = topColor.Lerp(bottomColor, cLerpY);
            color.a_ *= element_->GetDerivedOpacity();
            return color;
        }
        else
        {
            Color color = element_->GetColor(C_TOPLEFT);
            color.a_ *= element_->GetDerivedOpacity();
            return color;
        }
    }

    void UIBatch::AddOrMerge(const UIBatch& batch, std::vector<UIBatch>& batches)
    {
        if (batch.vertexEnd_ == batch.vertexStart_)
            return;

        if (!batches.empty() && batches.back().Merge(batch))
            return;

        batches.emplace_back(batch);
    }

}
