#include "Resource.h"
#include "MathDefs.h"
#include <iostream>
#include "File.h"
namespace Tiny2D
{
	Resource::Resource(Context * context) :Object(context)
	{
	}
	
	Resource::~Resource()
	{
		std::cout << "deleat Resource " + name_ << std::endl;
	}

	bool Resource::Load(File & file)
	{
		SetName(file.GetName());
		bool success = BeginLoad(file);
		this->EndLoad();
		return success;
	}

	bool Resource::BeginLoad(File & file)
	{
		return false;
	}

	bool Resource::EndLoad()
	{
		return false;
	}

	void Resource::SetName(const std::string & name)
	{
		name_ = name;
		nameHash_ = Calculate(name.c_str());
	}

	void Resource::SetMemoryUse(unsigned size)
	{
		memoryUse_ = size;
	}

	void Resource::ResetUseTimer()
	{
		useTimer_.Reset();
	}

	unsigned Resource::GetUseTimer()
	{
		// If more references than the resource cache, return always 0 & reset the timer
		if (Refs() > 1)
		{
			useTimer_.Reset();
			return 0;
		}
		else
			return useTimer_.GetMSec(false);
	}
	unsigned Resource::Calculate(const char * str)
	{

		unsigned hash = 0;

		if (!str)
			return hash;

		while (*str)
		{
			// Perform the actual hashing as case-insensitive
			char c = *str;
			hash = SDBMHash(hash, (unsigned char)tolower(c));
			++str;
		}

		return hash;

	}
}