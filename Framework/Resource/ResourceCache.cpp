#include "ResourceCache.h"
#include "Resource.h"
#include "File.h"
#include "FileSystem.h"
#include "Context.h"

#include "Shader.h"
#include "File.h"
#include "FileSystem.h"
#include <iostream>

namespace Tiny2D
{
	ResourceCache::ResourceCache(Context * context) :Object(context)
	{

	}

	ResourceCache::~ResourceCache()
	{
		for (auto& typeitr : cacheGroup_)
		{
			std::map<std::string, Resource*>& resmap = typeitr.second;
			for (auto& res : resmap)
			{
				res.second->ReleaseRef();
			}
		}
	}
	void ResourceCache::AddCache(const std::string & type, const std::string & name, Resource * res)
	{
		
		auto group = cacheGroup_.find(type);
		std::map<std::string, Resource*>* resmap = nullptr;

		if (group == cacheGroup_.end())
		{
			cacheGroup_[type] = std::map<std::string, Resource*>();
			resmap = &cacheGroup_[type];
		}
		else
			resmap = &(*group).second;

		;
		auto resitr = resmap->find(name);
		if (resitr == resmap->end())
		{
			(*resmap)[name] = res;
			res->AddRef();
		}
	}
	void ResourceCache::RemoveCache(const std::string & type, const std::string & name, Resource * res)
	{
		auto group = cacheGroup_.find(type);
		std::map<std::string, Resource*>* resmap = nullptr;

		if (group != cacheGroup_.end())
		{
			resmap = &(*group).second;

			auto resitr = resmap->find(name);
			if (resitr != resmap->end())
			{
				(*resmap).erase(resitr);
				res->ReleaseRef();
			}
		}
	}
	Resource* ResourceCache::GetFromCache(const std::string& type, const std::string& name)
	{
		auto group = cacheGroup_.find(type);
		std::map<std::string, Resource*>* resmap = nullptr;

		if (group != cacheGroup_.end())
		{
			resmap = &(*group).second;

			auto resitr = resmap->find(name);
			if (resitr != resmap->end())
			{
				return (resitr->second);
			}
		}
		return nullptr;
	}
}
