#pragma once
#include "Object.h"
#include "Resource.h"
#include "tiny_gltf.h"
namespace Tiny2D
{
	class Context;
	class VertexBuffer;
	class ElementBuffer;
	class GameObject;
	class Mesh;
	class Material;
	class Texture2D;
	class AnimationTrack;
	class Animation;
	class  GltfModel : public Resource
	{
		OBJECT_DEFINE(GltfModel)
		public:
			GltfModel(Context* context);
			virtual ~GltfModel();

			virtual bool BeginLoad(File& file);
			virtual bool EndLoad();

			void AnalysisNode(const tinygltf::Node& node, GameObject* parent, std::vector<GameObject*>& nodes, Animation* curAni);

			unsigned char* GetData() { return data_; }

			GameObject* GenScene(int idx = 0);
			AnimationTrack* GenAnimationTrack(int input, int output, std::string path);
			void GetInverseBindMatrices(std::vector<glm::mat4>& matrices, int buffidx);
		private:
			unsigned char* data_{};
			tinygltf::Model* model_{};
			tinygltf::TinyGLTF* loader_{};
			std::map<int, VertexBuffer*> vertextBufferMap_{};
			std::map<int, ElementBuffer*> elementBufferMap_{};
			std::map<int, Material*> materialMap_{};
			std::vector<Mesh*> mesh_{};
			std::vector<Texture2D*> textures_{};
			std::vector<GameObject*> nodes_{};
			Animation* animation_{};
	};

}
