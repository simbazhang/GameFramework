#pragma once
#include "Object.h"
#include "Timer.h"

namespace Tiny2D
{
	class Context;
	class File;
	class Resource : public Object
	{
		OBJECT_DEFINE(Resource)
	public:
		/// Construct.
		explicit Resource(Context* context);
		virtual ~Resource();

		bool Load(File& file);	
		virtual bool BeginLoad(File& file);
		virtual bool EndLoad();
		virtual void Release() {};

		/// Set name.
		void SetName(const std::string& name);
		/// Set memory use in bytes, possibly approximate.
		void SetMemoryUse(unsigned size);
		/// Reset last used timer.
		void ResetUseTimer();
		
		/// Return name.
		const std::string& GetName() const { return name_; }

		/// Return name hash.
		unsigned GetNameHash() const { return nameHash_; }

		/// Return memory use in bytes, possibly approximate.
		unsigned GetMemoryUse() const { return memoryUse_; }

		/// Return time since last use in milliseconds. If referred to elsewhere than in the resource cache, returns always zero.
		unsigned GetUseTimer();

		static unsigned Calculate(const char* str);
	protected:
		std::string name_;
		unsigned nameHash_{};
		Timer useTimer_;
		unsigned memoryUse_{};
	};
}