
#include "File.h"
#include "FileSystem.h"
#include <iostream>
#include "VertexBuffer.h"
#include "ElementBuffer.h"
#include "GltfModel.h"
#include "Mesh.h"
#include "GameObject.h"
#include <map>
#include "Material.h"
#include "Batch3D.h"
#include <glm/gtc/type_ptr.hpp>
#include "Render.h"
// Define these only in *one* .cc file.
#define TINYGLTF_IMPLEMENTATION
// #define TINYGLTF_NOEXCEPTION // optional. disable exception handling.
#include "tiny_gltf.h"
#include "Shader.h"
#include "Texture2D.h"
#include "ResourceCache.h"
#include "Resource.h"
#include "Animation.h"

static std::string GetFilePathExtension(const std::string& FileName) {
	if (FileName.find_last_of(".") != std::string::npos)
		return FileName.substr(FileName.find_last_of(".") + 1);
	return "";
}

static std::string GetBaseDir(const std::string& filepath) {
	if (filepath.find_last_of("/\\") != std::string::npos)
		return filepath.substr(0, filepath.find_last_of("/\\"));
	return "";
}

unsigned GetComponentTypeSize(int type)
{
	switch (type)
	{
	case TINYGLTF_COMPONENT_TYPE_BYTE:
	case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
		return 1;
	case TINYGLTF_COMPONENT_TYPE_SHORT:
	case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
		return 2;
	case TINYGLTF_COMPONENT_TYPE_INT:
	case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
	case TINYGLTF_COMPONENT_TYPE_FLOAT:
		return 4;
	case TINYGLTF_COMPONENT_TYPE_DOUBLE:
		return 8;
	default:
		break;
	}

	return -1;
}

unsigned GetNumberComponents(int com)
{
	switch (com)
	{
	case TINYGLTF_TYPE_SCALAR:
		return 1;
	case TINYGLTF_TYPE_VEC2:
		return 2;
	case TINYGLTF_TYPE_VEC3:
		return 3;
	case TINYGLTF_TYPE_VEC4:
		return 4;
	case TINYGLTF_TYPE_MAT2:
		return 4;
	case TINYGLTF_TYPE_MAT3:
		return 9;
	case TINYGLTF_TYPE_MAT4:
		return 16;
	default:
		break;
	}
	return 0;
}



namespace Tiny2D
{
	static Material* deaultMat = nullptr;
	GltfModel::GltfModel(Context * context) :Resource(context),
		data_(nullptr),
		model_(nullptr),
		loader_(nullptr)
	{
		model_ = new tinygltf::Model();
		loader_ = new tinygltf::TinyGLTF();

		deaultMat = new Material(context_);

		Pass pass;
		pass.passIdx = (unsigned)RenderPass::BasePass;
		pass.programState = context_->GetShaderCache()->Create("Shaders/LightObject.glsl", {"SKIN"});
		deaultMat->AddPass(pass); 
		deaultMat->AddRef();
		deaultMat->SetName("DeaultPBR");
	}

	GltfModel::~GltfModel()
	{
		delete model_;
		delete loader_;

		if (animation_)
		{
			animation_->ReleaseRef();
		}
		
		for (auto& itr : vertextBufferMap_)
		{
			itr.second->ReleaseRef();
		}

		for (auto& itr : elementBufferMap_)
		{
			itr.second->ReleaseRef();
		}

		for (auto& itr : materialMap_)
		{
			itr.second->ReleaseRef();
		}

		for (auto& itr : mesh_)
		{
			itr->ReleaseRef();
		}

		for (auto& itr : textures_)
		{
			itr->ReleaseRef();
		}

		for (auto& itr : nodes_)
		{
			itr->ReleaseRef();
		}
	}

	bool GltfModel::BeginLoad(File & source)
	{
		if (data_)
		{
			delete data_;
			data_ = nullptr;
		}
	
		SetName(source.GetName());
		source.Open();
		source.Seek(0);
		
		unsigned dataSize = source.GetSize();
		unsigned char* buffer = new unsigned char[dataSize];
		source.Read(buffer, dataSize);

		std::string err;
		std::string warn;
		std::string ext = GetFilePathExtension(name_);
		std::string basedir = GetBaseDir(name_);
		loader_->SetStoreOriginalJSONForExtrasAndExtensions(true);

		bool ret = false;
		if (ext.compare("glb") == 0) {
			
			std::cout << "Reading binary glTF" << std::endl;
			// assume binary glTF.
			ret = loader_->LoadBinaryFromMemory(model_, &err, &warn, buffer, dataSize);
		
		}
		else {
			std::cout << "Reading ASCII glTF" << std::endl;
			// assume ascii glTF.
			ret =
				loader_->LoadASCIIFromString(model_, &err, &warn, reinterpret_cast<const char*>(buffer), dataSize, basedir);
		}

		delete[] buffer;

		if (!warn.empty()) {
			printf("Warn: %s\n", warn.c_str());
		}

		if (!err.empty()) {
			printf("Err: %s\n", err.c_str());
		}

		if (!ret) {
			printf("Failed to parse glTF\n");
			return false;
		}

		if (model_->animations.size() > 0)
		{
			animation_ = new Animation(context_);
			animation_->AddRef();
		}

		int bufferViewIdx = 0;
		for (const auto& vertextBuff : model_->bufferViews)
		{
			glCheckError();
			if (vertextBuff.target == TINYGLTF_TARGET_ELEMENT_ARRAY_BUFFER)
			{
				glCheckError();
				const auto& buffdata = model_->buffers[vertextBuff.buffer];
				auto buff = new ElementBuffer(context_);
				buff->SetVertexSize(4);
				buff->SetSize(vertextBuff.byteLength/4);
				buff->SetData((void*)(&buffdata.data.at(0) + vertextBuff.byteOffset));
				buff->AddRef();
				elementBufferMap_[bufferViewIdx] = buff;
				glCheckError();
			}
			else if (vertextBuff.target == TINYGLTF_TARGET_ARRAY_BUFFER)
			{
				glCheckError();
				const auto& buffdata = model_->buffers[vertextBuff.buffer];
				auto buff = new VertexBuffer(context_);
				buff->AddRef();
				if (vertextBuff.byteStride == 0)
				{
					buff->SetVertexSize(1);
					buff->SetSize(vertextBuff.byteLength);
				}
				else
				{
					assert(vertextBuff.byteLength % vertextBuff.byteStride == 0);
					buff->SetVertexSize(vertextBuff.byteStride);
					buff->SetSize(vertextBuff.byteLength/ vertextBuff.byteStride);
				}
				glCheckError();
				buff->SetData((void*)(&buffdata.data.at(0) + vertextBuff.byteOffset));
				vertextBufferMap_[bufferViewIdx] = buff;
				//buff->DebugPrint();
				glCheckError();
			}

			bufferViewIdx++;
		}
		
		textures_.resize(model_->textures.size());
		for (size_t i = 0; i < model_->textures.size(); i++)
		{
			const auto& img = model_->images[model_->textures[i].source];
			auto tex = context_->GetResourceCache()->GetResource<Texture2D>(basedir + "/" + img.uri, true);
			if (tex)
			{
				tex->AddRef();
				textures_[i] = tex;
			}
			else
			{
				textures_[i] = nullptr;
			}
		}

		for (size_t i = 0; i < model_->skins.size(); i++)
		{
			const auto& skin = model_->skins[i];
			Skin* s = new Skin();
			s->nodeIdx_.assign(skin.joints.begin(), skin.joints.end());
			GetInverseBindMatrices(s->inverseBindMatrices, skin.inverseBindMatrices);
			if (animation_)
			{
				animation_->AddSkin(s);
			}
			
		}

		for (size_t i = 0; i < model_->nodes.size(); i++)
		{
			auto& node = model_->nodes[i];
			auto obj = new GameObject(context_);
			obj->SetName(node.name);
			obj->AddRef();
			nodes_.push_back(obj);

			if (animation_)
			{
				animation_->AddGameObject(obj);
			}
			
		}

		for (size_t i = 0; i < model_->materials.size(); i++)
		{
			Material* mat = new Material(context_);
			mat->AddRef();
			mat->SetName(model_->materials[i].name);
			if (animation_)
			{
				mat->SetAnimation(animation_);
			}
			materialMap_[i] = mat;
			
			Pass pass;
			pass.passIdx = (unsigned)RenderPass::BasePass;

			const auto& mmat = model_->materials[i];
			std::vector<std::string> defines;
			
			if (animation_)
			{
				defines.push_back("SKIN");
			}

			if (mmat.normalTexture.index >= 0)
			{
				defines.push_back("NOR_TEX");
				if (textures_[mmat.normalTexture.index])
				{
					pass.textures[TU_NORMAL] = textures_[mmat.normalTexture.index];
				}
			}

			if (mmat.occlusionTexture.index >= 0)
			{
				defines.push_back("OCC_TEX");
				if (textures_[mmat.occlusionTexture.index])
				{
					pass.textures[TU_OCC] = textures_[mmat.occlusionTexture.index];
				}
			}

			auto& pbrmat = model_->materials[i].pbrMetallicRoughness;

			if (pbrmat.baseColorTexture.index >= 0)
			{
				defines.push_back("PBR_BASE_TEX");
				if (textures_[pbrmat.baseColorTexture.index])
				{
					pass.textures[TU_DIFFUSE] = textures_[pbrmat.baseColorTexture.index];
				}
			}

			if (pbrmat.metallicRoughnessTexture.index >= 0)
			{
				defines.push_back("PBR_MET_ROUG_TEX");
				if (textures_[pbrmat.metallicRoughnessTexture.index])
				{
					pass.textures[TU_MET_ROUG] = textures_[pbrmat.metallicRoughnessTexture.index];
				}
			}

			
			pass.programState = context_->GetShaderCache()->Create("Shaders/PBR.glsl", defines);

			float albede[3] = { (float)pbrmat.baseColorFactor[0], (float)pbrmat.baseColorFactor[1], (float)pbrmat.baseColorFactor[2] };
			float ao = 1.0f;
			float roughness = pbrmat.roughnessFactor;
			float metallic = pbrmat.metallicFactor;

			pass.programState->SetUniform("albedo", albede);
			pass.programState->SetUniform("ao", &ao);
			pass.programState->SetUniform("roughness", &roughness);
			pass.programState->SetUniform("metallic", &metallic);

			mat->AddPass(pass);

			Pass shadow_pass;
			shadow_pass.passIdx = (unsigned)RenderPass::ShadowPass;
			shadow_pass.programState = context_->GetShaderCache()->Create("Shaders/Shadow.glsl", defines);
			mat->AddPass(shadow_pass);
		}

		for (size_t i = 0; i < model_->meshes.size(); i++)
		{
			const auto& mesh = model_->meshes[i];
			const auto& primitives = mesh.primitives;

			auto meshComp = new Mesh(context_);
			meshComp->AddRef();
			mesh_.push_back(meshComp);	
			// TODO 目前只能处理这种的，之后再拓展
			if (primitives.size() == 1)
			{
				for (const auto& primitive : primitives)
				{
					if (primitive.material >= 0)
					{
						meshComp->SetMaterial(materialMap_[primitive.material]);
					}
					else
					{
						meshComp->SetMaterial(deaultMat);
					}
					
					meshComp->SetMode(primitive.mode);
					if (primitive.indices >= 0)
					{
						const auto& accessor = model_->accessors[primitive.indices];
						const auto& bufferView = model_->bufferViews[accessor.bufferView];
						const auto& itr = elementBufferMap_.find(accessor.bufferView);
						if (itr != elementBufferMap_.end())
						{
							meshComp->SetElementBuffer(itr->second);
							itr->second->SetDataType(accessor.componentType);
							meshComp->SetRang(0, accessor.count);
						}
						else
						{
							std::cout << "can not find elementBuffer" << std::endl;
						}
					}

					for (auto const& itr : primitive.attributes)
					{
						const std::string& name = itr.first;
						const auto& accessor = model_->accessors[itr.second];

						VertextAttribut attr;
						attr.name = name;
						attr.buffer = vertextBufferMap_[accessor.bufferView];
						attr.byteOffset = accessor.byteOffset;
						attr.count = accessor.count;
						attr.compCount = GetNumberComponents(accessor.type);
						attr.compSIze = GetComponentTypeSize(accessor.componentType);
						attr.compType = accessor.componentType;
						attr.byteStride = accessor.ByteStride(model_->bufferViews[accessor.bufferView]);
						meshComp->AddVertextAttribut(attr);
						/*if (name == "WEIGHTS_0")
						{
							attr.buffer->DebugPrint();
						}

						if (name == "JOINTS_0")
						{
							attr.buffer->DebugPrintChar();
						}*/
					}
				}
			}
			else
			{
				assert(false);
			}
		}
		
		for (size_t i = 0; i < model_->animations.size(); i++)
		{
			const auto& ani = model_->animations[i];
			AnimationState* state = new AnimationState();
			state->name_ = ani.name;
			if (animation_)
			{
				animation_->AddAnimationState(state);
			}

			for (size_t n = 0; n < ani.channels.size(); n++)
			{
	
				const auto& channel = ani.channels[n];
				const auto& sampler = ani.samplers[channel.sampler];
				AnimationTrack* track =GenAnimationTrack(sampler.input, sampler.output, channel.target_path);

				Channel* channelAni = new Channel();
				channelAni->nodeIdx_.push_back(channel.target_node);
				channelAni->animationTrack_.push_back(track);

				if (track->time_.size() > 0)
				{
					state->maxTime_ = track->time_.back();
				}
				
				state->channels_.push_back(channelAni);
			}	
		}

		return true;
	}

	bool GltfModel::EndLoad()
	{
		return false;
	}

	void GltfModel::AnalysisNode(const tinygltf::Node& node, GameObject* parent, std::vector<GameObject*>& nodes, Animation* curAni)
	{
		parent->SetName(node.name);

		if (node.translation.size() > 0)
		{
			parent->SetPosition(glm::vec3(node.translation[0], node.translation[1], node.translation[2]));
		}

		if (node.rotation.size() > 0)
		{
			parent->SetRotation(glm::vec3(node.rotation[0], node.rotation[1], node.rotation[2]));
		}

		if (node.scale.size() > 0)
		{
			parent->SetScale(glm::vec3(node.scale[0], node.scale[1], node.scale[2]));
		}
		
		if (node.mesh >= 0)
		{
			if (parent->GetName() == "")
			{
				parent->SetName(std::string("Mesh_") + std::to_string(node.mesh));
			}
			auto meshc = (Mesh*)mesh_[node.mesh]->Clone();
			auto mat = meshc->GetMaterial();
			if (mat)
			{
				mat->SetAnimation(curAni);
			}
			parent->AddComponent(meshc);
		}

		for (size_t i = 0; i < node.children.size(); i++)
		{
			int nodeIdx = node.children[i];
			const tinygltf::Node& nodeNext = model_->nodes[nodeIdx];
			GameObject* gameObject = nodes[nodeIdx];
			parent->AddChild(gameObject);
			AnalysisNode(nodeNext, gameObject, nodes, curAni);
		}
	}

	GameObject* GltfModel::GenScene(int idx)
	{
		if (model_)
		{
			if (model_->scenes.size() > idx)
			{
				const auto& secene = model_->scenes[idx];
				GameObject* rootObject = new GameObject(context_);

				std::vector<GameObject*> cur_nodes;
				Animation* curani = nullptr;
				if(animation_)
					curani = (Animation*)animation_->Clone();
				for (size_t i = 0; i < model_->nodes.size(); i++)
				{
					auto& node = model_->nodes[i];
					auto obj = new GameObject(context_);
					obj->SetName(node.name);
					obj->AddRef();
					cur_nodes.push_back(obj);

					if (curani)
					{
						curani->AddGameObject(obj);
					}

				}

				for (size_t i = 0; i < secene.nodes.size(); i++)
				{
					GameObject* child = cur_nodes[secene.nodes[i]];
					rootObject->AddChild(child);
					AnalysisNode(model_->nodes[secene.nodes[i]], child, cur_nodes, curani);
				}
				if (curani)
				{
					rootObject->AddComponent(curani);
				}

				return rootObject;
			}
		}
		return nullptr;
	}

	AnimationTrack* GltfModel::GenAnimationTrack(int input, int output, std::string path)
	{
		const auto& accessorInput = model_->accessors[input];
		const auto& bufferviewInput = model_->bufferViews[accessorInput.bufferView];
		const auto& buffInput = model_->buffers[bufferviewInput.buffer];
		
		auto compCount = GetNumberComponents(accessorInput.type);
		auto compSIze = GetComponentTypeSize(accessorInput.componentType);

		AnimationTrack* track = new AnimationTrack();
		track->path_ = path;
		track->time_.resize(accessorInput.count);
		memcpy(track->time_.data(), buffInput.data.data() + bufferviewInput.byteOffset, accessorInput.count * compCount * compSIze);

		const auto& accessorOutput = model_->accessors[output];
		const auto& bufferviewOutput = model_->bufferViews[accessorOutput.bufferView];
		const auto& buffOutput = model_->buffers[bufferviewOutput.buffer];

		auto compCount2 = GetNumberComponents(accessorOutput.type);
		auto compSIze2 = GetComponentTypeSize(accessorOutput.componentType);

		track->data_.resize(accessorOutput.count);
		for (int i = 0; i < accessorOutput.count; i++)
		{
			float temp[4];
			memcpy(temp, buffOutput.data.data() + compCount2 * compSIze2 * i + bufferviewOutput.byteOffset,  compCount2 * compSIze2);
			track->data_[i] = glm::vec4({ temp [0], temp [1], temp [2], temp [3]});
		}
		

		return track;
	}

    void GltfModel::GetInverseBindMatrices(std::vector<glm::mat4>& matrices, int buffidx)
    {
        const auto& accessorInput = model_->accessors[buffidx];
        const auto& bufferviewInput = model_->bufferViews[accessorInput.bufferView];
        const auto& buffInput = model_->buffers[bufferviewInput.buffer];
        
        auto compCount = GetNumberComponents(accessorInput.type);
        auto compSIze = GetComponentTypeSize(accessorInput.componentType);
        for (int n = 0; n < accessorInput.count; n++) {
            float mat[16];
			memcpy(mat, buffInput.data.data() + bufferviewInput.byteOffset  + 64* n,  64);   
            glm::mat4 out = glm::make_mat4(mat);
            matrices.push_back(out);
        }
        
    }

}
