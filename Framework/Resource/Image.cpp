
#include "File.h"
#include "FileSystem.h"
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#include "Image.h"
#include <glad/glad.h>

#include "rgbe.h"


namespace Tiny2D
{

	Image::Image(Context * context) :Resource(context),
		width_(0),
		height_(0),
		components_(4),
		data_(nullptr)
	{
	}

	Image::~Image()
	{
		if (data_)
		{
			stbi_image_free(data_);
		}
	}

	bool Image::BeginLoad(File & source)
	{
		if (data_)
		{
			delete data_;
			data_ = nullptr;
		}
	
		SetName(source.GetName());

		source.Open();

		source.Seek(0);

		unsigned dataSize = source.GetSize();

		unsigned char* buffer = new unsigned char[dataSize];
		source.Read(buffer, dataSize);
        
		stbi_set_flip_vertically_on_load(true);
		data_ = stbi_load_from_memory(buffer, dataSize, &width_, &height_, (int*)&components_, 0);

		if (!data_)
		{
			std::cout << "load image failed " << name_ << std::endl;
			return false;
		}
		delete[] buffer;
		return true;
	}

	bool Image::EndLoad()
	{
		return false;
	}

    unsigned Image::GetFormat()
    {
//        if(name_.size() < 3)
//            return GL_RGBA;
        
//        std::string ext = name_.substr(name_.size() - 3);
//        if(ext == "hdr")
//            return GL_RGB16F;
        if(components_ == 4)
            return GL_RGBA;
        else
            return GL_RGB;
    }

}
