#pragma once
#include "Object.h"
#include "File.h"
#include "FileSystem.h"
#include <iostream>
#include <map>
#include <string>

namespace Tiny2D
{
	class Context;
	class Resource;
	class ResourceCache : public Object
	{
	public:
		/// Construct.
		explicit ResourceCache(Context* context);
		/// Destruct.
		~ResourceCache() override;

		void AddCache(const std::string& type, const std::string& name, Resource* res);
		void RemoveCache(const std::string& type, const std::string& name, Resource* res);
		Resource* GetFromCache(const std::string& type, const std::string& name);

		template <class T> T* GetResource(const std::string& name, bool fullPath = false);
	private:
		std::map<std::string, std::map<std::string, Resource*>> cacheGroup_;
	};

	template<class T>
	inline T * ResourceCache::GetResource(const std::string & name, bool fullPath)
	{
		auto cache = GetFromCache(T::GetTypeName(), name);
		if (cache)
		{
			return (T*)cache;
		}

		auto fileSystem = context_->GetFileSystem();
		auto file = fileSystem->GetFile(name, fullPath);
		if (file)
		{
			auto res = new T(context_);
			if (res->Load(*file))
			{
				AddCache(res->GetTypeName(), name, res);
				return res;
			}
			else
			{
				delete res;
			}
		}
		return nullptr;
	};
}
