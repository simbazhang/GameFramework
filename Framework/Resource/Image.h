#pragma once
#include "Object.h"
#include "Resource.h"

namespace Tiny2D
{
	class Context;
	class  Image : public Resource
	{
		OBJECT_DEFINE(Image)
		public:
			Image(Context* context);
			virtual ~Image();

			virtual bool BeginLoad(File& file);
			virtual bool EndLoad();

			unsigned char* GetData() { return data_; }

			int GetWidth() const { return width_; }
			int GetHeight() const { return height_; }

            unsigned GetFormat();
			int width_;
			int height_;
			unsigned components_;

		private:
			unsigned char* data_;
	};

}
