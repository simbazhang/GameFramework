#pragma once

#include "Object.h"
#include <functional>

namespace Tiny2D
{
	template <class T>
	class Signal
	{
	public:
		~Signal();

		using HandlerFunctionPtr = std::function<void(T&)>;
		struct Handler
		{
			int id;
			Object* obj;
			HandlerFunctionPtr func;
		};

	public:
		int AddHandler(Object*, HandlerFunctionPtr);
		void RemoveHandler(int id);
		void Emit(T&);
	private:
		std::vector<Handler> handler_{};
		int id_{};
	};

	template<class T>
	inline Signal<T>::~Signal()
	{
		for (size_t i = 0; i < handler_.size(); i++)
		{
			if (handler_[i].obj)
			{
				handler_[i].obj->ReleaseRef();
			}
		}
	}

	template<class T>
	int Signal<T>::AddHandler(Object* obj, HandlerFunctionPtr ptr)
	{
		if (obj)
		{
			obj->AddRef();
		}

		id_ = id_ + 1;
		handler_.push_back({id_, obj , ptr });
		return id_;
	}

	template<class T>
	void Signal<T>::RemoveHandler(int id)
	{
		for (size_t i = 0; i < handler_.size(); i++)
		{
			if (handler_[i].id == id)
			{
				handler_.erase(handler_.begin() + i);
				return;
			}
		}
	}

	template<class T>
	void Signal<T>::Emit(T& data)
	{
		for (size_t i = 0; i < handler_.size(); i++)
		{
			handler_[i].func(data);
		}
	}
}