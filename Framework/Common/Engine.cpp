#include "Engine.h"
#include <stdio.h>
#include <stdlib.h>
#include "Context.h"
#include "FileSystem.h"
#include "Image.h"
#include "Shader.h"
#include "ResourceCache.h"
#include "Camera.h"
#include "Graphics.h"
#include <stb_image.h>
#include "UI.h"
#include "Service.h"
#include "Render.h"
#include "GameObject.h"

namespace Tiny2D
{
	Engine::Engine(Context* context):context_(context), maxFps_(60), minFps_(15), timeStepSmoothing_(2), timeStep_(0)
	{

	}

	Engine::~Engine()
	{
	}

	int Engine::Initialize()
	{
		return 0;
	}

	void Engine::Finalize()
	{
	}

	void Engine::Tick()
	{
        if (winSize_.x <= 0 || winSize_.y <= 0)
        {
            return;
        }
        context_->GetService()->Tick(timeStep_);
		context_->GetRender()->Update(timeStep_);
		context_->GetRender()->RenderUpdate();
        context_->GetUI()->Update(timeStep_);
		context_->GetUI()->RenderUpdate();
        context_->GetRender()->RenderShadowMap();
        context_->GetGraphics()->OnFrameStart(winSize_.x, winSize_.y);
		context_->GetGraphics()->ApplyViewport();
		context_->GetRender()->RenderBatches();
		context_->GetUI()->Render();
        context_->GetGraphics()->OnFrameEnd();
		context_->GetGraphics()->ResetRenderSurface();
        ApplyFrameLimit();
	}

    void Engine::Resize(int newWidth, int newHeight)
    {
        if (newWidth <= 0 || newHeight <= 0)
        {
            return;
        }

		glCheckError();
        winSize_.x = newWidth;
        winSize_.y = newHeight;
        centerPos_.x = newWidth * 0.5f;
        centerPos_.y = newHeight * 0.5f;
		glCheckError();
		context_->GetRender()->UpdateWinSize(winSize_);
        context_->GetUI()->UpdateWinSize(winSize_);
        context_->GetGraphics()->SetViewport(IntRect(0, 0, newWidth, newHeight));
		glCheckError();
    }

	void Engine::ApplyFrameLimit()
	{
        unsigned maxFps = maxFps_;
        long long elapsed = 0;

        if (maxFps)
        {
            long long targetMax = 1000000LL / maxFps;

            for (;;)
            {
                elapsed = frameTimer_.GetUSec(false);
                if (elapsed >= targetMax)
                    break;

                // Sleep if 1 ms or more off the frame limiting goal
                if (targetMax - elapsed >= 1000LL)
                {
                    auto sleepTime = (unsigned)((targetMax - elapsed) / 1000LL);
                    Time::Sleep(sleepTime);
                }
            }
        }

        elapsed = frameTimer_.GetUSec(true);

        // If FPS lower than minimum, clamp elapsed time
        if (minFps_)
        {
            long long targetMin = 1000000LL / minFps_;
            if (elapsed > targetMin)
                elapsed = targetMin;
        }

        // Perform timestep smoothing
        timeStep_ = 0.0f;
        lastTimeSteps_.push_back(elapsed / 1000000.0f);
        if (lastTimeSteps_.size() > timeStepSmoothing_)
        {
            // If the smoothing configuration was changed, ensure correct amount of samples
            auto over = lastTimeSteps_.size() - timeStepSmoothing_;
            lastTimeSteps_.erase(lastTimeSteps_.begin(), lastTimeSteps_.begin() + over);
            for (unsigned i = 0; i < lastTimeSteps_.size(); ++i)
                timeStep_ += lastTimeSteps_[i];
            timeStep_ /= lastTimeSteps_.size();
        }
        else
            timeStep_ = lastTimeSteps_.back();
	}
}
