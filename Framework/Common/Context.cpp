#include "Context.h"
#include "Object.h"
#include "FileSystem.h"
#include "ResourceCache.h"
#include "Graphics.h"
#include "Shader.h"
#include "Timer.h"
#include "UI.h"
#include "Service.h"
#include "Render.h"

namespace Tiny2D
{
	Context::Context():fileSystem_(nullptr)
	{
		fileSystem_ = new FileSystem(this);
		resourceCache_ = new ResourceCache(this);
		graphics_ = new Graphics(this);
		time_ = new Time(this);
		shaderCache_ = new ShaderCache(this);
		ui_ = new UI(this);
		render_ = new Render(this);
		service_ = new Service();
	}
	Context::~Context()
	{
		for (auto iter = autoReleasePool_.cbegin(); iter != autoReleasePool_.cend(); iter++)
		{
			(*iter)->ReleaseRef();
		}
		delete fileSystem_;
		delete resourceCache_;
		delete graphics_;
		delete time_;
		delete shaderCache_;
		delete ui_;
		delete render_;
		delete service_;
	}
	void Context::AutoRelease(Object * obj)
	{
		if (std::find(autoReleasePool_.begin(), autoReleasePool_.end(), obj) == autoReleasePool_.end())
		{
			obj->AddRef();
			autoReleasePool_.push_back(obj);
		}
	}
	void Context::onEndFrame()
	{
		for (auto iter = autoReleasePool_.begin(); iter != autoReleasePool_.end();)
		{	
			(*iter)->ReleaseRef();
			iter++;
		}
		autoReleasePool_.clear();
	}
}
