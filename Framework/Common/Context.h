#pragma once
#include "RefCounted.h"
#include <string>
#include <vector>
namespace Tiny2D
{
	class Object;
	class FileSystem;
	class ResourceCache;
	class Graphics;
	class ShaderCache;
	class Time;
	class UI;
	class Service;
	class Render;
	class Context : public RefCounted
	{
	public:
		Context();
		virtual ~Context();
		void AutoRelease(Object* obj);
		FileSystem* GetFileSystem() { return fileSystem_; }
		ResourceCache* GetResourceCache() { return resourceCache_; }
		Graphics* GetGraphics() { return graphics_; }
		ShaderCache* GetShaderCache() { return shaderCache_; }
		Time* GetTime() { return time_; }
		UI* GetUI() { return ui_; }
		Service* GetService() { return service_; }
		Render* GetRender() { return render_; }
		void onEndFrame();
	private:
		std::vector<Object*> autoReleasePool_{};
		FileSystem* fileSystem_;
		ResourceCache* resourceCache_;
		Graphics* graphics_;
		ShaderCache* shaderCache_;
		Time* time_;
		UI* ui_;
		Service* service_;
		Render* render_;
	};
} // namespace My