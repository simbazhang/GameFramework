#pragma once
#include "Object.h"
#include <glm/glm.hpp>
#include <functional>
#include <string>
#include "Color.h"
namespace Tiny2D
{
	const int MaxChar = 256;
	enum class  VariantType {
		INT = 1,
		FLOAT,
		IVEC2,
		UVEC2,
		VEC2,
		BOOL,
		STR,
        VEC3,
		COLOR,
	};

	struct Variant
	{
		VariantType type;
		union Value
		{
			glm::ivec2 v2i;
			glm::vec2 v2;
			float f;
			int i;
			bool b;
			glm::uvec2 v2u;
			char str[MaxChar];
            glm::vec3 v3;
			Color color{1.0f,1.0f,1.0f,1.0f};
		}v;

		template <class T> T Get() const;

		float                          GetFloat()const;
		int                             GetInt()const;
		glm::ivec2                 GetIVec2()const;
		glm::vec2                  GetVec2()const;
		glm::uvec2                GetUVec2()const;
		glm::vec3                  GetVec3()const;
		bool                          GetBool()const;
		std::string                 GetString()const;
		Color						 GetColor()const;

		Variant& operator =(int rhs)
		{
			type = VariantType::INT;
			v.i = rhs;
			return *this;
		}

		Variant& operator =(float rhs)
		{
			type = VariantType::FLOAT;
			v.f = rhs;
			return *this;
		}

		Variant& operator =(glm::ivec2 rhs)
		{
			type = VariantType::IVEC2;
			v.v2i = rhs;
			return *this;
		}

		Variant& operator =(glm::vec2 rhs)
		{
			type = VariantType::VEC2;
			v.v2 = rhs;
			return *this;
		}
        
        Variant& operator =(glm::vec3 rhs)
        {
            type = VariantType::VEC3;
            v.v3 = rhs;
            return *this;
        }

		Variant& operator =(glm::uvec2 rhs)
		{
			type = VariantType::UVEC2;
			v.v2u = rhs;
			return *this;
		}

		Variant& operator =(bool rhs)
		{
			type = VariantType::BOOL;
			v.b = rhs;
			return *this;
		}

		Variant& operator =(std::string rhs)
		{
			type = VariantType::STR;
			memset(v.str, '\0', MaxChar);
			if (rhs.length() <= MaxChar)
			{
				memcpy(v.str, rhs.data(), rhs.length());
			}
			
			return *this;
		}

		Variant& operator =(Color rhs)
		{
			type = VariantType::COLOR;
			v.color = rhs;
			return *this;
		}

	};
	
	template <> float Variant::Get<float>() const;
	template <> int Variant::Get<int>() const;
	template <> bool Variant::Get<bool>() const;
	template <> glm::vec2 Variant::Get<glm::vec2>() const;
	template <> glm::ivec2 Variant::Get<glm::ivec2>() const;
	template <> glm::uvec2 Variant::Get<glm::uvec2>() const;
	template <> glm::vec3 Variant::Get<glm::vec3>() const;
	template <> std::string Variant::Get<std::string>() const;
	template <> Color Variant::Get<Color>() const;
	
	struct AttributeBind
	{
		std::string name;
		std::string type;
		Variant value;
		std::function<void(void)> get;
		std::function<void(void)> set;
		bool inited = false;
	};
}
