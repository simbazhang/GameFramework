#pragma once
#include "RefCounted.h"
#include <string>
#include "Context.h"
#include "AttributeBind.hpp"
namespace Tiny2D
{
    #define OBJECT_DEFINE(name)\
    public:\
    static const char* GetTypeName() { return #name; };

    #define ACCESSOR_ATTRIBUTE(aname, type, getf, setf){\
        AttributeBind* bind = new AttributeBind();\
        bind->name = aname;\
        bind->value = getf();\
        bind->get = [=]() {\
            bind->value = getf();\
        };\
        bind->set = [=]() {\
            setf(bind->value.Get<type>());\
        };\
        attributeBind_.push_back(bind);};

    struct AttributeBind;
    class Object : public RefCounted
    {
        OBJECT_DEFINE(Object)
    public:
        explicit Object(Context *context);
        virtual ~Object() override;
        virtual std::vector<AttributeBind*>& GetAttributeBind() { return attributeBind_; }
        AttributeBind* GetAttributeBind(const std::string& name);
    protected:
        Context* context_;
        std::vector<AttributeBind*> attributeBind_;

	};
} 
