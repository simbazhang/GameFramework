#pragma once
#include <string>
#include "Object.h"

namespace Tiny2D
{
	enum FileMode
	{
		FILE_READ = 0,
		FILE_WRITE,
		FILE_READWRITE
	};

	class Context;
	class  File : public Object
	{
		OBJECT_DEFINE(File)
	public:
		File(Context* context);
		File(Context* context,  const std::string& fileName, FileMode mode = FILE_READ);
		virtual~File() override;

		unsigned Read(void* dest, unsigned size);
		unsigned Seek(unsigned position);
		unsigned Write(const void* data, unsigned size);

		const std::string& GetName() const { return fileName_; }

		bool Open(FileMode mode = FILE_READ);
		void Close();
		void Flush();
		unsigned Tell() { return position_; }

		bool IsEof() const { return position_ >= size_; }

		FileMode GetMode() const { return mode_; }
		bool IsOpen() const;
		unsigned GetSize() { return size_; }

		std::string ReadLine();
		std::string ReadString();
		signed char ReadByte();

		void* GetHandle() { return handle_; }
	protected:
		bool ReadInternal(void* dest, unsigned size);
		void SeekInternal(unsigned newPosition);

		std::string fileName_;
		FileMode mode_;
		void* handle_;
		unsigned size_;
		unsigned position_;
	};

}
