#include "BaseApplication.hpp"
#include <iostream>
#include "Engine.h"
using namespace Tiny2D;

bool Tiny2D::BaseApplication::m_bQuit = false;
BaseApplication* BaseApplication::instance_ = nullptr;

Tiny2D::BaseApplication::BaseApplication(GfxConfiguration& cfg)
	:m_Config(cfg)
{
	instance_ = this;
}

// Parse command line, read configuration, initialize all sub modules
int Tiny2D::BaseApplication::Initialize()
{
	int result = 0;

	std::wcout << m_Config;

	return result;
}

// Finalize all sub modules and clean up all runtime temporary files.
void Tiny2D::BaseApplication::Finalize()
{
	if (engine_)
	{
		delete engine_;
	}
}

// One cycle of the main loop
void Tiny2D::BaseApplication::Tick()
{
}

bool Tiny2D::BaseApplication::IsQuit()
{
	return m_bQuit;
}

Context* Tiny2D::BaseApplication::GetContext()
{
	if (engine_)
	{
		return engine_->GetContext();
	}

	return nullptr;
}
