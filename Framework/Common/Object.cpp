#include "Object.h"
#include "AttributeBind.hpp"

namespace Tiny2D
{
    Object::Object(Context* context) :
        context_(context)
    {
        assert(context_);
    }

    Object::~Object()
    {

    }

    AttributeBind* Object::GetAttributeBind(const std::string& name)
    {
        for (size_t i = 0; i < attributeBind_.size(); i++)
        {
            if (attributeBind_[i]->name == name)
            {
                return attributeBind_[i];
            }
        }
        return nullptr;
    }

}