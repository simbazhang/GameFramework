#include <stdio.h>
#include "BaseApplication.hpp"

using namespace Tiny2D;

namespace Tiny2D {
	extern BaseApplication*    g_pApp;
}

int main(int argc, char** argv) {
	int ret;

	if ((ret = g_pApp->Initialize()) != 0) {
		printf("App Initialize failed, will exit now.");
		return ret;
	}


	while (!g_pApp->IsQuit()) {
		g_pApp->Tick();
	}

	g_pApp->Finalize();
	return 0;
}

