#pragma once
#include "IApplication.hpp"
#include "GfxConfiguration.h"
namespace Tiny2D
{
    class Engine;
    class Context;
    class BaseApplication : public IApplication
    {
    public:
        static BaseApplication* GetInstance() { return instance_; }
        BaseApplication(GfxConfiguration& cfg);
        virtual int Initialize();
        virtual void Finalize();
        // One cycle of the main loop
        virtual void Tick();

        virtual bool IsQuit();

        Engine* GetEngine() { return engine_; }
        Context* GetContext();


    protected:
        GfxConfiguration m_Config;
        static bool m_bQuit;
        Engine* engine_{};
    private:
        static BaseApplication* instance_;
        BaseApplication(){};
};

} // namespace My