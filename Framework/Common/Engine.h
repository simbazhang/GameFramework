#pragma once
#include "IRuntimeModule.hpp"
#include "Context.h"
#include "Timer.h"
#include <glm/glm.hpp>

namespace Tiny2D
{
	class  Engine :public IRuntimeModule
	{
	public:
		/// Construct.
		explicit Engine(Context*);
		/// Destruct. Free all subsystems.
		virtual ~Engine() override;
		virtual int Initialize()override;
		virtual void Finalize()override;

		virtual void Tick()override;

        void ApplyFrameLimit();

		void Resize(int width, int height);
		glm::ivec2 GetWinSize() { return winSize_; };

		void SetContext(Context* context) { context_ = context; };
		Context* GetContext() { return context_; }
	private:
		Context* context_{};

		HiresTimer frameTimer_{};
        std::vector<float> lastTimeSteps_;
		float timeStep_{};
		unsigned timeStepSmoothing_{};
		unsigned minFps_{};
		unsigned maxFps_{};
		glm::ivec2 winSize_{};
		glm::vec2 centerPos_{};
	};

}