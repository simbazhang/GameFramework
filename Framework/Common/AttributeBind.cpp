#include "AttributeBind.hpp"
using namespace Tiny2D;
float Tiny2D::Variant::GetFloat() const
{
	if (type == VariantType::FLOAT)
	{
		return v.f;
	}

	return 0.0f;
}

int Tiny2D::Variant::GetInt() const
{
	if (type == VariantType::INT)
	{
		return v.i;
	}
	return 0;
}

glm::ivec2 Tiny2D::Variant::GetIVec2() const
{
	if (type == VariantType::IVEC2)
	{
		return v.v2i;
	}
	return glm::ivec2();
}

glm::vec2 Tiny2D::Variant::GetVec2() const
{
	if (type == VariantType::VEC2)
	{
		return v.v2;
	}
	return glm::vec2();
}

glm::uvec2 Tiny2D::Variant::GetUVec2() const
{
	if (type == VariantType::UVEC2)
	{
		return v.v2u;
	}
	return glm::uvec2();
}

glm::vec3 Tiny2D::Variant::GetVec3() const
{
	if (type == VariantType::VEC3)
	{
		return v.v3;
	}
	return glm::vec3();
}

bool Tiny2D::Variant::GetBool() const
{
	if (type == VariantType::BOOL)
	{
		return v.b;
	}
	return false;
}

std::string Tiny2D::Variant::GetString() const
{
	if (type == VariantType::STR)
	{
		return v.str;
	}
	return std::string();
}

Color Tiny2D::Variant::GetColor() const
{
	if (type == VariantType::COLOR)
	{
		return v.color;
	}
	return Color();
}

template <> bool Variant::Get<bool>() const
{
	return GetBool();
}


template <> int Variant::Get<int>() const
{
	return GetInt();
}


template <> float Variant::Get<float>() const
{
	return GetFloat();
}

template <> glm::vec2 Variant::Get<glm::vec2>() const
{
	return GetVec2();
}

template <> glm::vec3 Variant::Get<glm::vec3>() const
{
	return GetVec3();
}

template <> glm::ivec2 Variant::Get<glm::ivec2>() const
{
	return GetIVec2();
}

template <> glm::uvec2 Variant::Get<glm::uvec2>() const
{
	return GetUVec2();
}

template <> std::string Variant::Get<std::string>() const
{
	return GetString();
}

template <> Color Variant::Get<Color>() const
{
	return GetColor();
}

