# GameFramework

#### 介绍
包含一个渲染框架和编辑器，主要参考了Urho3D的实现

编辑器基于Imgui

渲染框架基于opengl

支持windows和mac，使用cmake构建，选择EditorApp启动

已经完成基础的ui渲染框架和编辑

目前正在打算支持3d相关功能
![输入图片说明](https://images.gitee.com/uploads/images/2020/1210/201736_5c392062_580435.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1223/220426_45ef63d7_580435.png "屏幕截图.png")