#pragma once
#include "BaseApplication.hpp"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
namespace Tiny2D {
	class GLFWApplication : public BaseApplication
	{
	public:
		GLFWApplication(GfxConfiguration& config)
			: BaseApplication(config) {};

		virtual int Initialize();
		virtual void Finalize();
		// One cycle of the main loop
		virtual void Tick();	

		virtual void OnMouseDown(int x, int y) {};
		virtual void OnMouseMove(int x, int y) {};
		virtual void OnMouseUp(int x, int y) {};
		virtual void onSizeChange(int width, int height){};

	public:
		static GLFWApplication* sInstance;
	protected:
		void processInput(GLFWwindow *window);
        GLFWwindow* window_{};
		int winWidth_;
		int winHeight_;
	};
}