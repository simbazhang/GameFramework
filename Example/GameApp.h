#pragma once
#include "GLFWApplication.hpp"

namespace Tiny2D {
	class GameApp : public GLFWApplication
	{
	public:
		GameApp(GfxConfiguration& config)
			: GLFWApplication(config) {};

		virtual int Initialize();
		virtual void Finalize();
		virtual void Tick();
		virtual void onSizeChange(int width, int height)override;
	};
}
