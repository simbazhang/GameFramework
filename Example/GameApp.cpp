#include "GameApp.h"
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include "Context.h"
#include "FileSystem.h"
#include "Image.h"
#include "Shader.h"
#include "Resource.h"
#include "ResourceCache.h"
#include "Camera.h"
#include "Graphics.h"
#include <stb_image.h>
#include "UI.h"
#include "Engine.h"
#include "Texture2D.h"
#include "Sprite.h"
#include "UIElement.h"
#include "Texture2D.h"
#include <iostream>
#include <stdio.h>
#include "GltfModel.h"
#include "GameObject.h"
#include "Render.h"
#include "VertexBuffer.h"
#include "ElementBuffer.h"
#include "Mesh.h"
#include "Camera.h"
#include "Light.h"
#include "Material.h"

namespace Tiny2D {
	GfxConfiguration config(8, 8, 8, 8, 32, 0, 0, 960, 540, "Editor (Windows)");
	BaseApplication* g_pApp = static_cast<BaseApplication*>(new GameApp(config));
    Camera* g_debugCamera;

	GltfModel* g_gltf;
}

using namespace Tiny2D;

int Tiny2D::GameApp::Initialize()
{
	using namespace std::placeholders;

	GLFWApplication::Initialize();
	auto context = new Context();
	engine_ = new Engine(context);
	engine_->Initialize();
	engine_->Resize(winWidth_, winHeight_);
	
    std::string curdir = GetContext()->GetFileSystem()->GetCurrentDir();
#if WIN32
	std::string resourcePath = curdir + "/../../Resources";
#else
	std::string resourcePath = curdir + "/../../../Resources";
#endif
    std::cout << resourcePath << std::endl;
	GetContext()->GetFileSystem()->addSearchePath(resourcePath);

	GameObject* cameraObj = new GameObject(GetContext());
	cameraObj->SetName("Main Camera");
	cameraObj->SetPosition(glm::vec3(0.f, 5.f, 0.f));
	cameraObj->SetRotation(glm::vec3(0.f, 0.f, 0.f));
	Camera* camera = new Camera(GetContext());
	camera->SetNearFar(glm::vec2(1.f, 100.f));
	cameraObj->AddComponent(camera);
	GetContext()->GetRender()->GetRootObject()->AddChild(cameraObj);

	auto test6 = GetContext()->GetResourceCache()->GetResource<GltfModel>("models/tree.gltf");
	if (test6)
	{
		auto testnode = test6->GenScene(0);
		GetContext()->GetRender()->GetRootObject()->AddChild(testnode);
	}
	return 0;
}

void Tiny2D::GameApp::Finalize()
{

}


void Tiny2D::GameApp::Tick()
{
    static bool show_shadow_map = false;
	auto graphics = GetContext()->GetGraphics();
	graphics->ClearColor(0.3f, 0.3f, 0.3f);
	engine_->Tick();
	GLFWApplication::Tick();
}

void Tiny2D::GameApp::onSizeChange(int width, int height)
{

}
