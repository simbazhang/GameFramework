#ifdef COMPILEPS
    uniform sampler2D sDiffMap; 
    uniform sampler2D sNormalMap;
    uniform sampler2D sShadowMap;
    uniform sampler2D sMatRougMap;
    uniform sampler2D sIrradianceMap;
    uniform sampler2D sOccMap;
    uniform sampler2D sSkybox;
    uniform sampler2D sCascadeShadowMap[NUM_CASCADES];
    //uniform sampler2D sCascadeShadowMap_0;
    //uniform sampler2D sCascadeShadowMap_1;
    //uniform sampler2D sCascadeShadowMap_2;
#endif
