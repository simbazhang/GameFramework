uniform mat4 cModel;
uniform mat4 cView;
uniform mat4 cViewInv;
uniform mat4 cViewProj;

#ifdef COMPILEVS
    layout (location = 0) in vec2 iPos;
    layout (location = 1) in vec4 iColor;
    layout (location = 2) in vec2 iTexCoord;

    out vec4 vColor;
    out vec2 vTexCoord;
#endif


#ifdef COMPILEPS
    uniform sampler2D sDiffMap;
    in vec4 vColor;
    in vec2 vTexCoord;
    out vec4 outColor;
#endif



void VS()
{
    vec4 pos = vec4(iPos.xy, 0.0, 1.0);
    gl_Position = cViewProj * pos;
    vColor = iColor;
    vTexCoord = iTexCoord;
}

void PS()
{
    outColor = texture(sDiffMap, vTexCoord) * vColor;
}
