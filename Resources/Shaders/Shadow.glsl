uniform mat4 cModel;
uniform mat4 cView;
uniform mat4 cProj;

#ifdef COMPILEVS
    in vec3 POSITION;
    #ifdef SKIN
        in vec4 JOINTS_0;
        in vec4 WEIGHTS_0;
        uniform mat4 bones[128];
    #endif
#endif


#ifdef COMPILEPS
    out vec4 outColor;
#endif



void VS()
{
    vec3 WorldPos;
#ifdef SKIN
    vec4 pos = vec4(POSITION, 1.0);
    vec4 spos = bones[int(JOINTS_0[0])] * pos * WEIGHTS_0[0] + bones[int(JOINTS_0[1])] * pos * WEIGHTS_0[1] + bones[int(JOINTS_0[2])] * pos * WEIGHTS_0[2] + bones[int(JOINTS_0[3])] * pos * WEIGHTS_0[3];
    WorldPos = vec3(spos);    
#else
    WorldPos = vec3(cModel * vec4(POSITION, 1.0));
#endif
    gl_Position =  cProj * cView * vec4(WorldPos, 1.0);
}

void PS()
{
    //outColor = vec4(1.0,1.0,1.0, 1.0);
}
