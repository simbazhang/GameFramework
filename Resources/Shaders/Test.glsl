#ifdef COMPILEVS
    layout (location = 0) in vec3 NORMAL;
    layout (location = 1) in vec3 POSITION;
    layout (location = 2) in vec3 TANGENT;
    layout (location = 2) in vec2 TEXCOORD_0;
    
    uniform mat4 cModel;
    uniform mat4 cView;
    uniform mat4 cProj;
    out vec2 TexCoord;
#endif

void VS()
{
    TexCoord = TEXCOORD_0;
    gl_Position =  vec4(POSITION, 1.0);
}

#ifdef COMPILEPS
    uniform sampler2D texture_diffuse1;
    in vec2 TexCoord;
    out vec4 outColor;
#endif

void PS()
{    
    outColor = vec4(1.0, 1.0, 0.0, 1.0);
}