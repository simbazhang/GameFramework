uniform mat4 cModel;
uniform mat4 cView;
uniform mat4 cProj;

#ifdef COMPILEVS
    layout (location = 0) in vec3 POSITION;
    layout (location = 2) in vec3 NORMAL;
#endif


#ifdef COMPILEPS
    uniform vec3 albedo;
    out vec4 outColor;
#endif



void VS()
{
    vec3 WorldPos = vec3(cModel * vec4(POSITION, 1.0));
    gl_Position =  cProj * cView * vec4(WorldPos, 1.0);
}

void PS()
{
    outColor = vec4(1.0,1.0,1.0, 1.0);
}
