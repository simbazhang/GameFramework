#ifdef COMPILEVS
    in vec3 POSITION;
    in vec2 TEXCOORD_0;
    uniform mat4 cModel;
    uniform mat4 cView;
    uniform mat4 cProj;
    uniform float cElapsedTime;
    out vec4 clipPos;
    out vec3 WorldPos;
    out vec2 vWaterUV;

#ifdef SKIN
    in vec4 JOINTS_0;
    in vec4 WEIGHTS_0;
    uniform mat4 bones[128];
#endif

#endif

void VS()
{

#ifdef SKIN
    vec4 pos = vec4(POSITION, 1.0);
    clipPos = cProj * cView *
            (bones[int(JOINTS_0[0])] * pos * WEIGHTS_0[0] +
                bones[int(JOINTS_0[1])] * pos * WEIGHTS_0[1] +
                bones[int(JOINTS_0[2])] * pos * WEIGHTS_0[2] +
                bones[int(JOINTS_0[3])] * pos * WEIGHTS_0[3]);
#endif

    clipPos = cProj * cView * cModel * vec4(POSITION, 1.0);
    WorldPos = (cModel * vec4(POSITION, 1.0)).xyz;
    vWaterUV = TEXCOORD_0 + cElapsedTime * 0.1;
    gl_Position = clipPos;
}

#ifdef COMPILEPS
    #include Samplers.glsl
    uniform float cNoiseStrength;

    out vec4 outColor;
    in vec4 clipPos;
    in vec3 WorldPos;
    in vec2 vWaterUV;
    // lights
    uniform vec3 cLightPositions;
    uniform vec3 cLightColors;
    uniform vec3 cLightDir;
    uniform vec3 cCamPos;

    vec2 GetQuadTexCoord(vec4 clipPos)
    {
        return vec2(
            clipPos.x / clipPos.w * 0.5 + 0.5,
            clipPos.y / clipPos.w * 0.5 + 0.5);
    }

    vec3 fresnelSchlick(float cosTheta, vec3 F0)
    {
        return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
    }
#endif

void PS()
{
    vec3 V = normalize(cCamPos - WorldPos);
    vec3 L = normalize(cLightPositions - WorldPos);
    vec3 H = normalize(V + L);
    
    vec3 nMap = texture(sNormalMap, vWaterUV).rgb;
   
    vec2 uv = GetQuadTexCoord(clipPos);
    uv.y = 1.0 - uv.y;
    vec2 noise = (nMap.rg - 0.5) * cNoiseStrength;
    uv += noise;
    uv.y = max(uv.y, 0.0);
    
    float cosTheta = max(dot(H, V), 0.0);
    vec3 reflectColor = texture(sShadowMap, uv).rgb;
    vec3 albedo = vec3(0.0, 0.21, 0.47);
    float fresnel = pow(cosTheta, 1.0);
    vec3 finalColor = mix(albedo, reflectColor, fresnel);
    outColor = vec4(finalColor, 1.0);
}
