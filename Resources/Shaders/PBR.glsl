const int NUM_CASCADES = 1;

#ifdef COMPILEVS
    in vec3 POSITION;
    in vec2 TEXCOORD_0;
    in vec3 NORMAL;

#ifdef SKIN
    in vec4 JOINTS_0;
    in vec4 WEIGHTS_0;
    uniform mat4 bones[128];
#endif

    out vec2 TexCoords;
    out vec3 WorldPos;
    out vec3 Normal;
    out vec4 FragPosLightSpace[NUM_CASCADES];
    out float ClipSpacePosZ;
    out float LightClipSpacePosZ;

    uniform mat4 cModel;
    uniform mat4 cView;
    uniform mat4 cProj;
    uniform mat4 cLightSpaceMatrix[NUM_CASCADES];
    uniform vec3 cLightPositions;
   
#endif

void VS()
{
    TexCoords = vec2(TEXCOORD_0.x, 1.0 - TEXCOORD_0.y);  
       
    vec3 WorldPos;
#ifdef SKIN
    vec4 pos = vec4(POSITION, 1.0);
    vec4 spos = bones[int(JOINTS_0[0])] * pos * WEIGHTS_0[0] + bones[int(JOINTS_0[1])] * pos * WEIGHTS_0[1] + bones[int(JOINTS_0[2])] * pos * WEIGHTS_0[2] + bones[int(JOINTS_0[3])] * pos * WEIGHTS_0[3];
    WorldPos = vec3(spos);
    Normal = mat3(bones[int(JOINTS_0[0])]) * NORMAL * WEIGHTS_0[0] +
             mat3(bones[int(JOINTS_0[1])]) * NORMAL * WEIGHTS_0[1] +
             mat3(bones[int(JOINTS_0[2])]) * NORMAL * WEIGHTS_0[2] +
             mat3(bones[int(JOINTS_0[3])]) * NORMAL * WEIGHTS_0[3];
#else
    Normal = mat3(cModel) * NORMAL;
    WorldPos = vec3(cModel * vec4(POSITION, 1.0));
#endif

    for (int i = 0 ; i < NUM_CASCADES; i++) {
        FragPosLightSpace[i] = cLightSpaceMatrix[i] * vec4(WorldPos, 1.0);
    }

    vec4 viewpos = cView * vec4(WorldPos, 1.0);
    vec4 lightviewpos = cView * vec4(cLightPositions, 1.0);
    gl_Position =  cProj * viewpos;
    ClipSpacePosZ = viewpos.z;
    LightClipSpacePosZ = (cProj * lightviewpos).z * 0.5 + 0.5;
    //ClipSpacePosZ = ClipSpacePosZ * 0.5 + 0.5;
}

#ifdef COMPILEPS
    out vec4 FragColor;
    in vec2 TexCoords;
    in vec3 WorldPos;
    in vec3 Normal;
    in vec4 FragPosLightSpace[NUM_CASCADES];
    in float ClipSpacePosZ;
    in float LightClipSpacePosZ;

    // material parameters
    #ifdef PBR_BASE_TEX
        vec3 albedo;
    #else
        uniform vec3 albedo;
    #endif

    #ifdef PBR_MET_ROUG_TEX
        float metallic;
        float roughness;
    #else
        uniform float metallic;
        uniform float roughness;
    #endif  

    #ifdef OCC_TEX
        float ao;
    #else
        uniform float ao;
    #endif  

    #ifdef PCF
    uniform vec2 cShadowMapInvSize;
    #else
    vec2 cShadowMapInvSize = vec2(0, 0);
    #endif   
    

    // lights
    uniform vec3 cLightPositions;
    uniform vec3 cLightColors;
    uniform vec3 cLightDir;
    uniform vec3 cCamPos;
    uniform float gCascadeEndClipSpace[NUM_CASCADES];
    
    const float PI = 3.14159265359;

    #include Samplers.glsl
    #include PCSS.glsl

    // (1/(pi/2), 1/(pi))
    const vec2 invAtan = vec2(0.1591, 0.3183);
    vec2 SampleSphericalMap(vec3 v)
    {
        vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
        uv *= invAtan;
        uv += 0.5;
        return uv;
    }

    vec3 rgbe2rgb(vec4 rgbe)
    {
        return (rgbe.rgb * pow(2.0 , rgbe.a * 255.0 - 128.0));
    }

    vec3 ldr2hdr(vec3 rgb)
    {
        return pow(rgb, vec3(2.2, 2.2, 2.2));
    }
    // ----------------------------------------------------------------------------
    // Easy trick to get tangent-normals to world-space to keep PBR code simplified.
    // Don't worry if you don't get what's going on; you generally want to do normal
    // mapping the usual way for performance anways; I do plan make a note of this
    // technique somewhere later in the normal mapping tutorial.
    vec3 getNormalFromMap()
    {
        vec3 tangentNormal = texture(sNormalMap, TexCoords).xyz * 2.0 - 1.0;

        vec3 Q1  = dFdx(WorldPos);
        vec3 Q2  = dFdy(WorldPos);
        vec2 st1 = dFdx(TexCoords);
        vec2 st2 = dFdy(TexCoords);

        vec3 N   = normalize(Normal);
        vec3 T  = normalize(Q1*st2.t - Q2*st1.t);
        vec3 B  = -normalize(cross(N, T));
        mat3 TBN = mat3(T, B, N);

        return normalize(TBN * tangentNormal);
    }
    // ----------------------------------------------------------------------------
    float DistributionGGX(vec3 N, vec3 H, float roughness)
    {
        float a = roughness*roughness;
        float a2 = a*a;
        float NdotH = max(dot(N, H), 0.0);
        float NdotH2 = NdotH*NdotH;

        float nom   = a2;
        float denom = (NdotH2 * (a2 - 1.0) + 1.0);
        denom = PI * denom * denom;

        return nom / denom;
    }
    // ----------------------------------------------------------------------------
    float GeometrySchlickGGX(float NdotV, float roughness)
    {
        float r = (roughness + 1.0);
        float k = (r*r) / 8.0;

        float nom   = NdotV;
        float denom = NdotV * (1.0 - k) + k;

        return nom / denom;
    }
    // ----------------------------------------------------------------------------
    float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
    {
        float NdotV = max(dot(N, V), 0.0);
        float NdotL = max(dot(N, L), 0.0);
        float ggx2 = GeometrySchlickGGX(NdotV, roughness);
        float ggx1 = GeometrySchlickGGX(NdotL, roughness);

        return ggx1 * ggx2;
    }
    // ----------------------------------------------------------------------------
    vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
    {
        return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
    } 
    // ----------------------------------------------------------------------------
    vec3 fresnelSchlick(float cosTheta, vec3 F0)
    {
        return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
    }

    float ShadowCalculation(vec4 fragPosLightSpace, vec3 normal, vec3 lightDir, int idx)
    {
        // 执行透视除法
        vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
        // 变换到[0,1]的范围
        projCoords = projCoords * 0.5 + 0.5;
        // return projCoords.z;
        
        if(projCoords.z > 1.0)
            return 0.0;
        float shadow = PCSS(sCascadeShadowMap[idx], projCoords);
        return shadow;
    }
#endif

void PS()
{	
    #ifdef NOR_TEX
        // 有bug好像先注释	
        // vec3 N = getNormalFromMap();
        vec3 N = normalize(Normal);
    #else
        vec3 N = normalize(Normal);
    #endif
    
    #ifdef OCC_TEX
        ao = texture(sOccMap, TexCoords).r;
    #endif
    vec3 V = normalize(cCamPos - WorldPos);

    // calculate reflectance at normal incidence; if dia-electric (like plastic) use F0 
    // of 0.04 and if it's a metal, use the albedo color as F0 (metallic workflow)

    #ifdef PBR_BASE_TEX
        albedo = pow(texture(sDiffMap, TexCoords).rgb, vec3(2.2));
    #endif

    #ifdef PBR_MET_ROUG_TEX
        vec3 matinfo = texture(sMatRougMap, TexCoords).rgb;
        metallic = matinfo.b;
        roughness = matinfo.g;
    #endif
    
    float roughnessfinal = roughness * roughness;
    roughnessfinal = clamp(roughness, 0.04, 1.0);
    
    float metalnessfinal = clamp(metallic, 0.03, 1.0);

    vec3 F0 = vec3(0.04); 
    F0 = mix(F0, albedo, metalnessfinal);

    // reflectance equation
    vec3 Lo = vec3(0.0);
    vec3 test;
    float test2;
    for(int i = 0; i < 1; ++i) 
    {
        //vec3 L = normalize(cLightPositions - WorldPos);
        vec3 L = normalize(cLightDir);
        vec3 H = normalize(V + L);
        //float distance = length(cLightPositions - WorldPos);
        //float attenuation = 1.0 / (distance * distance);
        float attenuation = 1.0;
        vec3 radiance = cLightColors * attenuation;

        // Cook-Torrance BRDF
        float NDF = DistributionGGX(N, H, roughnessfinal);
        float G   = GeometrySmith(N, V, L, roughnessfinal);
        vec3 F    = fresnelSchlickRoughness(max(dot(H, V), 0.0), F0, roughnessfinal);
           
        vec3 nominator    = NDF * G * F;
        float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; // 0.001 to prevent divide by zero.
        vec3 specular = nominator / denominator;
        test = texture(sNormalMap, TexCoords).xyz;
        // kS is equal to Fresnel
        vec3 kS = F;
        // for energy conservation, the diffuse and specular light can't
        // be above 1.0 (unless the surface emits light); to preserve this
        // relationship the diffuse component (kD) should equal 1.0 - kS.
        vec3 kD = vec3(1.0) - kS;
        // multiply kD by the inverse metalness such that only non-metals
        // have diffuse lighting, or a linear blend if partly metal (pure metals
        // have no diffuse light).
        kD *= 1.0 - metalnessfinal;

        // scale light by NdotL
        float NdotL = max(dot(N, L), 0.0);

        // add to outgoing radiance Lo
        Lo += (kD * albedo / PI + specular) * radiance * NdotL;
        test2 = NdotL;
    }   
    
    
    vec2 iblUV = SampleSphericalMap(N); 
    vec3 kS = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughnessfinal);
    vec3 kD = 1.0 - kS;
    //kD *= 1.0 - metalnessfinal;	  
    vec3 irradiance = ldr2hdr(texture(sIrradianceMap, iblUV).rgb);
    vec3 diffuse =  irradiance * albedo;
    vec3 ambient = (kD * diffuse) * ao;

    //float ShadowFactor = ShadowCalculation(FragPosLightSpace[0], N, normalize(cLightDir), 0);
    //FragColor = vec4(vec3(ShadowFactor), 1.0);
    //return;
    // 计算阴影
    float ShadowFactor = 0.0;
    vec4 CascadeIndicator = vec4(0.0, 0.0, 0.0, 0.0);
    for (int i = 0 ; i < NUM_CASCADES; i++) {
        if (-ClipSpacePosZ <= gCascadeEndClipSpace[i]) {
            ShadowFactor = ShadowCalculation(FragPosLightSpace[i], N, normalize(cLightDir), i);

            //if (i == 0) 
            //    CascadeIndicator = vec4(0.2, 0.0, 0.0, 0.0);
            //else if (i == 1)
            //    CascadeIndicator = vec4(0.0, 0.2, 0.0, 0.0);
            //else if (i == 2)
            //    CascadeIndicator = vec4(0.0, 0.0, 0.2, 0.0);

            break;
        }
   }
    test = getNormalFromMap();
    vec3 color = ambient + (ShadowFactor)*Lo;

    // HDR tonemapping
    color = color / (color + vec3(1.0));
    // gamma correct
    color = pow(color, vec3(1.0/2.2));
    FragColor = vec4(vec3(color), 1.0) + CascadeIndicator;
}
