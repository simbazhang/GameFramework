#ifdef COMPILEVS
    layout (location = 0) in vec3 POSITION;
    out vec3 TexCoords;
    uniform mat4 cView;
    uniform mat4 cProj;
#endif

void VS()
{
    TexCoords = POSITION;
    vec4 pos = cProj * cView * vec4(POSITION, 1.0);
    gl_Position = pos.xyww;
}


#ifdef COMPILEPS
    out vec4 FragColor;
    in vec3 TexCoords;
    uniform sampler2D sSkybox;

        // (1/(pi/2), 1/(pi))
    const vec2 invAtan = vec2(0.1591, 0.3183);
    vec2 SampleSphericalMap(vec3 v)
    {
        vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
        uv *= invAtan;
        uv += 0.5;
        return uv;
    }
#endif

void PS()
{   
    vec2 iblUV = SampleSphericalMap(normalize(TexCoords));
    FragColor = texture(sSkybox, iblUV);
}