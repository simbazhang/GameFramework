#include "EditorApp.h"
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include "Context.h"
#include "FileSystem.h"
#include "Image.h"
#include "Shader.h"
#include "ResourceCache.h"
#include "Camera.h"
#include "Graphics.h"
#include <stb_image.h>
#include "UI.h"
#include "Engine.h"
#include "Texture2D.h"
#include "Sprite.h"
#include "UIElement.h"

#include "imgui.h"
#include "imgui_internal.h"
#include "backends/imgui_impl_glfw.h"
#include "backends/imgui_impl_opengl3.h"

#include "ThemeContainer.h"
#include "Texture2D.h"
#include <iostream>
#include <stdio.h>
#include "GltfModel.h"
#include "GameObject.h"
#include "Render.h"
#include "VertexBuffer.h"
#include "ElementBuffer.h"
#include "Mesh.h"
#include "Camera.h"
#include "Light.h"
#include "Material.h"
#include "bind.h"
#include "docs/DocMgr.h"
#include "ProjectMgr.h"
#include "views/ProjectFilesView.h"
#include "IconsFontAwesome5.h"


namespace Tiny2D {
	GfxConfiguration config(8, 8, 8, 8, 32, 0, 0, 960, 540, "Editor (Windows)");
	BaseApplication* g_pApp = static_cast<BaseApplication*>(new EditorApp(config));
    Camera* g_debugCamera;
}

using namespace Tiny2DEditor;
#define RANDNUM(a, b) ((rand() % (b - a + 1)) + a)
#define RANDFLOAT() (rand() % (1000) / (float)(1000))
int Tiny2D::EditorApp::Initialize()
{
	using namespace std::placeholders;

	GLFWApplication::Initialize();
	auto context = new Context();
	engine_ = new Engine(context);
	engine_->Initialize();
	engine_->Resize(winWidth_, winHeight_);
	
    std::string curdir = GetContext()->GetFileSystem()->GetCurrentDir();
#if WIN32
	std::string resourcePath = curdir + "/../../Resources";
#else
	std::string resourcePath = curdir + "/../../../Resources";
#endif
    std::cout << resourcePath << std::endl;
	GetContext()->GetFileSystem()->addSearchePath(resourcePath);

	std::string temp(GetNativePath(resourcePath));
	string_replase(temp, "\\", "/");

	
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
	//io.ConfigViewportsNoAutoMerge = true;
	//io.ConfigViewportsNoTaskBarIcon = true;

	auto fileSystem = GetContext()->GetFileSystem();
	auto file = fileSystem->GetFile("imgui.ini");
	if(file)
		ImGui::LoadIniSettingsFromDisk(file->GetName().c_str());

	ImGui_ImplGlfw_InitForOpenGL(window_, true);
	ImGui_ImplOpenGL3_Init("#version 150");

	ImGuiStyle& style = ImGui::GetStyle();
	style.WindowMenuButtonPosition = ImGuiDir_Right;
	UIHelper::UI_InitStyle(GetEngine());

	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard | ImGuiConfigFlags_DockingEnable /*| ImGuiConfigFlags_ViewportsEnable TODO: allow this on windows? test on linux?*/;

	io.Fonts->AddFontDefault();
	File* ttf = GetContext()->GetFileSystem()->GetFile("Fonts/Default.ttf");
    if(ttf)
    {
        ttf->Open();
        char* data = new char[ttf->GetSize()];
        ttf->Read(data, ttf->GetSize());

        ImFontConfig font_cfg = ImFontConfig();
        ImFont* font = io.Fonts->AddFontFromMemoryTTF(data, ttf->GetSize(), 16, &font_cfg, io.Fonts->GetGlyphRangesChineseFull());
        ImGui::GetIO().FontDefault = font;
    }

	File* ttfIcon = GetContext()->GetFileSystem()->GetFile("Fonts/fa-solid-900.ttf");
	static const ImWchar icon_ranges[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };
	if (ttfIcon && ttfIcon->Open()) {
		char* data = new char[ttfIcon->GetSize()];
		ttfIcon->Read(data, ttfIcon->GetSize());

		ImFontConfig config;
		config.MergeMode = true;
		io.Fonts->AddFontFromMemoryTTF(data, ttfIcon->GetSize(), 16, &config, icon_ranges);
	}

	ImGui::GetStyle() = ThemeContainer::Instance().GetUIStyle("Corporate Gray");

	DocMgr::GetInstance()->Init(context);
	ProjectMgr::GetInstance()->LoadConfig();
	ProjectMgr::GetInstance()->projSignal.AddHandler(nullptr, std::bind(&EditorApp::OnOpenProject, this, _1));

	BuildTestScene();

	return 0;
}

void Tiny2D::EditorApp::Finalize()
{
	BaseApplication::Finalize();
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}


void Tiny2D::EditorApp::Tick()
{
    static bool show_shadow_map = false;
    
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	ImGuiContext& g = *GImGui;
	ImGuiIO& io = ImGui::GetIO();

	const ImGuiViewport* viewport = ImGui::GetMainViewport();
	ImGui::SetNextWindowPos(viewport->WorkPos);
	ImGui::SetNextWindowSize(viewport->WorkSize);
	ImGui::SetNextWindowViewport(viewport->ID);

	ImGuiWindowFlags window_flags = ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus | (ImGuiWindowFlags_MenuBar) | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;

	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	ImGui::Begin("Content", nullptr, window_flags);
	{
		ImGui::PopStyleVar(3);
		UIHelper::UI_DrawMenu();

		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu(u8"Test"))
			{
				if (ImGui::MenuItem("test scene")) {
					BuildTestScene();
				}

				if (ImGui::MenuItem("add role")) {
					AddTestRole();
				}

				if (ImGui::MenuItem("add car")) {
					AddTestCar();
				}

				if (ImGui::MenuItem("add tv")) {
					AddTestTV();
				}

				if (ImGui::MenuItem("add house")) {
					auto resCache = GetContext()->GetResourceCache();
					auto test6 = resCache->GetResource<GltfModel>("models/tree.gltf");
					if (test6)
					{
						auto testnode = test6->GenScene(0);
						testnode->SetPosition(glm::vec3(0, 2.2, -5.3));
						testnode->SetRotation(glm::vec3(0, -16, 0));
						GetContext()->GetRender()->GetRootObject()->AddChild(testnode);
					}
				}
                
                if (ImGui::MenuItem("showmap")) {
                    show_shadow_map = !show_shadow_map;
                }

				if (ImGui::MenuItem("add quad ui")) {
					auto resCache = GetContext()->GetResourceCache();
					auto tex = resCache->GetResource<Texture2D>("test.png");
					auto ui = GetContext()->GetUI();
					auto obj = new UIElement(GetContext());
					auto sprite = new SpriteComponent(GetContext());
					sprite->SetTexture(tex);
					ui->GetRootElement()->AddChild(obj);
					//obj->SetPosition(glm::vec2(RANDNUM(-100, 100), RANDNUM(-100, 100)));
					obj->SetRotation(RANDNUM(0, 360));
					obj->SetColor(Color(RANDFLOAT(), RANDFLOAT(), RANDFLOAT(), 1.f));
					obj->AddComponent(sprite);
				}

				ImGui::EndMenu();
			}

		
			ImGui::EndMenuBar();
		}

		DocMgr::GetInstance()->OnGui(g_pApp->GetContext());
		ProjectMgr::GetInstance()->OnGui();
	}
	ImGui::End();
	ImGui::Render();
	
	auto graphics = GetContext()->GetGraphics();
	graphics->ClearColor(0.3f, 0.3f, 0.3f);

	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		GLFWwindow* backup_current_context = glfwGetCurrentContext();
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		glfwMakeContextCurrent(backup_current_context);
	}

	if (DocMgr::GetInstance()->GetCurDoc())
		OnUpdate();
		//engine_->Tick();
	GLFWApplication::Tick();
    static bool pressed = false;
    if(!pressed && io.MouseDown[0])
    {
        this->OnMouseDown(io.MousePos.x, io.MousePos.y);
        pressed = true;
    }
    
    this->OnMouseMove(io.MousePos.x, io.MousePos.y);
    
    if(pressed && io.MouseDown[0] == false)
    {
        this->OnMouseUp(io.MousePos.x, io.MousePos.y);
        pressed = false;
    }
    
}

void Tiny2D::EditorApp::OnMouseDown(int x, int y)
{
	if(DocMgr::GetInstance()->GetCurDoc())
		DocMgr::GetInstance()->GetCurDoc()->OnMouseDown(x, y);
}

void Tiny2D::EditorApp::OnMouseMove(int x, int y)
{
	if (DocMgr::GetInstance()->GetCurDoc())
		DocMgr::GetInstance()->GetCurDoc()->OnMouseMove(x, y);
}

void Tiny2D::EditorApp::OnMouseUp(int x, int y)
{
	if (DocMgr::GetInstance()->GetCurDoc())
		DocMgr::GetInstance()->GetCurDoc()->OnMouseUp(x, y);
}

void Tiny2D::EditorApp::onSizeChange(int width, int height)
{

}

void Tiny2D::EditorApp::AddTestRole()
{
	auto resCache = GetContext()->GetResourceCache();
	auto test4 = resCache->GetResource<GltfModel>("Character Running.gltf");
	if (test4)
	{
		auto lightObj = test4->GenScene(0);
		lightObj->SetName("SkinTest");
		lightObj->SetPosition(glm::vec3(RANDNUM(0, 4), 5.f, -2.f));
		GetContext()->GetRender()->GetRootObject()->AddChild(lightObj);
	}
}

void Tiny2D::EditorApp::AddTestCar()
{
	auto resCache = GetContext()->GetResourceCache();
	auto test2 = resCache->GetResource<GltfModel>("models/car.gltf");
	if (test2)
	{
		auto testnode = test2->GenScene(0);
		testnode->SetPosition(glm::vec3(0, 0, -3));
		testnode->SetRotation(glm::vec3(0, 220, 180));
		GetContext()->GetRender()->GetRootObject()->AddChild(testnode);
	}
}

void Tiny2D::EditorApp::AddTestTV()
{
	auto resCache = GetContext()->GetResourceCache();
	auto test5 = resCache->GetResource<GltfModel>("models/scene.gltf");
	if (test5)
	{
		auto testnode = test5->GenScene(0);
		testnode->SetPosition(glm::vec3(0, 2.2, -5.3));
		testnode->SetRotation(glm::vec3(0, -16, 0));
		GetContext()->GetRender()->GetRootObject()->AddChild(testnode);
	}
}

void Tiny2D::EditorApp::BuildTestScene()
{
	auto resCache = GetContext()->GetResourceCache();
	auto tex = resCache->GetResource<Texture2D>("test.png");
	auto ui = GetContext()->GetUI();
	glCheckError();
	auto tex2 = resCache->GetResource<Texture2D>("ibl/Playa_Sunrise_Env.hdr");
	GetContext()->GetRender()->SetEnvIBLMap(tex2);
	//glCheckError();
	auto tex3 = resCache->GetResource<Texture2D>("ibl/Playa_Sunrise_4k.png");
	GetContext()->GetRender()->SetEnvMap(tex3);
	glCheckError();
	GameObject* cameraObj = new GameObject(GetContext());
	cameraObj->SetName("Main Camera");
	cameraObj->SetPosition(glm::vec3(0.f, 5.f, 0.f));
	cameraObj->SetRotation(glm::vec3(0.f, 0.f, 0.f));
	Camera* camera = new Camera(GetContext());
	camera->SetNearFar(glm::vec2(1.f, 100.f));
	//camera->SetUseReflection(true);
	cameraObj->AddComponent(camera);
	//Camera* camera2 = new Camera(GetContext());
	//camera2->SetNearFar(glm::vec2(0.1f, 500.f));
	//camera2->SetUseReflection(true);
	//cameraObj->AddComponent(camera2);
	//g_debugCamera = camera2;
	auto test3 = resCache->GetResource<GltfModel>("models/pointlight.gltf");
	if (test3)
	{
		auto lightObj = test3->GenScene(0);
		lightObj->SetName("Main Light");
		lightObj->SetPosition(glm::vec3(0.f, 5.f, 0.f));
		Light* light = new Light(GetContext());
        light->EnableShadowMap(true);
		lightObj->AddComponent(light);
		light->SetLightFactor(20);
		GetContext()->GetRender()->GetRootObject()->AddChild(lightObj);
	}

	auto test4 = resCache->GetResource<GltfModel>("plane.gltf");
	if (test4)
	{
		auto lightObj = test4->GenScene(0);
		lightObj->SetName("plane");
		lightObj->SetScale(glm::vec3(100, 1, 100));
		GetContext()->GetRender()->GetRootObject()->AddChild(lightObj);
	}

	//auto test5 = resCache->GetResource<GltfModel>("models/SHADOW.gltf");
	//if (test5)
	//{
	//	auto lightObj = test5->GenScene(0);
	//	lightObj->SetName("test");
	//	GetContext()->GetRender()->GetRootObject()->AddChild(lightObj);
	//}

	
	//auto test4 = resCache->GetResource<GltfModel>("Character Running.gltf");
	//if (test4)
	//{
	//	auto lightObj = test4->GenScene(0);
	//	lightObj->SetName("SkinTest");
	//	lightObj->SetPosition(glm::vec3(0.f, 5.f, -2.f));
	//	GetContext()->GetRender()->GetRootObject()->AddChild(lightObj);
	//}
	//light->SetColor({0xe4, 0xc4, 0x10});

	GetContext()->GetRender()->GetRootObject()->AddChild(cameraObj);


	/*auto test2 = resCache->GetResource<GltfModel>("models/car.gltf");
	if (test2)
	{
		auto testnode = test2->GenScene(0);
		testnode->SetPosition(glm::vec3(0, 0, -3));
		testnode->SetRotation(glm::vec3(0, 220, 180));
		g_pContext->GetRender()->GetRootObject()->AddChild(testnode);
	}*/
	/*
	auto test5 = resCache->GetResource<GltfModel>("models/scene.gltf");
	if (test5)
	{
		auto testnode = test5->GenScene(0);
		testnode->SetPosition(glm::vec3(0, 2.2, -5.3));
		testnode->SetRotation(glm::vec3(0, -16, 0));
		GetContext()->GetRender()->GetRootObject()->AddChild(testnode);
	}

	auto test6 = resCache->GetResource<GltfModel>("models/tree.gltf");
	if (test6)
	{
		auto testnode = test6->GenScene(0);
		testnode->SetPosition(glm::vec3(0, 2.2, -5.3));
		testnode->SetRotation(glm::vec3(0, -16, 0));
		GetContext()->GetRender()->GetRootObject()->AddChild(testnode);
	}*/
	/*
	float vertices[] = {
	1, 1, 0.0f, 1.0f, 1.0f,  // 右上角
	1, -1, 0.0f,  1.0f, 0.0f,// 右下角
	-1, -1, 0.0f, 0.0f, 0.0f,// 左下角
	-1, 1, 0.0f,   0.0f, 1.0f // 左上角
	};

	unsigned int indices[] = { // 注意索引从0开始! 
	0, 1, 3, // 第一个三角形
	1, 2, 3  // 第二个三角形
	};

	auto buff = new ElementBuffer(GetContext());
	buff->SetVertexSize(4);
	buff->SetSize(6);
	buff->SetData(indices);
	buff->SetDataType(GL_UNSIGNED_INT);

	auto quad = new VertexBuffer(GetContext());
	quad->SetVertexSize(20);
	quad->SetSize(4);
	quad->SetData(vertices);

	auto meshComp = new Mesh(GetContext());
	meshComp->SetElementBuffer(buff);
	meshComp->SetRang(0, 6);
	VertextAttribut attr;
	attr.name = "POSITION";
	attr.buffer = quad;
	attr.byteOffset = 0;
	attr.count = 4;
	attr.compCount = 3;
	attr.compSIze = 4;
	attr.compType = GL_FLOAT;
	attr.byteStride = 20;
	meshComp->AddVertextAttribut(attr);

	VertextAttribut attr2;
	attr2.name = "TEXCOORD_0";
	attr2.buffer = quad;
	attr2.byteOffset = 12;
	attr2.count = 4;
	attr2.compCount = 2;
	attr2.compSIze = 4;
	attr2.compType = GL_FLOAT;
	attr2.byteStride = 20;
	meshComp->AddVertextAttribut(attr2);

	Material* mat = new Material(GetContext());
	mat->SetName("water");
	Pass pass;
	pass.passIdx = (unsigned)RenderPass::RefractPass;
	pass.textures[TU_SHADOWMAP] = camera2->GetReflectSurface()->colorAtt;
	pass.textures[TU_NORMAL] = GetContext()->GetResourceCache()->GetResource<Texture2D>("WaterNoise.png");
	pass.programState = GetContext()->GetShaderCache()->Create("Shaders/StaticMesh.glsl", {});
	float cNoiseStrength = 0.1f;
	pass.programState->SetUniform("cNoiseStrength", &cNoiseStrength);
	mat->AddPass(pass);
	meshComp->SetMaterial(mat);


	GameObject* waterObj = new GameObject(GetContext());
	waterObj->SetName("Water");
	waterObj->AddComponent(meshComp);
	waterObj->SetRotation({ 90, 0, 0 });
	waterObj->SetScale({ 100,100,100 });
	GetContext()->GetRender()->GetRootObject()->AddChild(waterObj);*/

	auto graphics = GetContext()->GetGraphics();
	srand((int)(10));
	for (size_t i = 0; i <0; i++)
	{
		auto obj = new UIElement(GetContext());
		auto sprite = new SpriteComponent(GetContext());
		sprite->SetTexture(tex);
		ui->GetRootElement()->AddChild(obj);
		//obj->SetPosition(glm::vec2(RANDNUM(-100, 100), RANDNUM(-100, 100)));
		obj->SetRotation(RANDNUM(0, 360));
		obj->SetColor(Color(RANDFLOAT(), RANDFLOAT(), RANDFLOAT(), 1.f));
		obj->AddComponent(sprite);
	}
}

void Tiny2D::EditorApp::OnOpenProject(ProjectInfo& proj)
{
	DocMgr::GetInstance()->SetRootPath(proj.path);
}

void Tiny2D::EditorApp::OnUpdate()
{
}
