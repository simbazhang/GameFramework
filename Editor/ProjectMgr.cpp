#include "ProjectMgr.h"
#include "BaseApplication.hpp"
#include "Context.h"
#include "File.h"
#include "FileSystem.h"
#include "imgui.h"
#include "imgui_internal.h"
#include "ImGuiFileDialog.h"
#include "Resource.h"
#include "ResourceCache.h"
#include "Texture2D.h"

using nlohmann::json;
using namespace Tiny2D;

void to_json(json& j, const ProjectInfo& p) {
	j = json{ {"name", p.name}, {"path", p.path}, {"version", p.version} };
}

void from_json(const json& j, ProjectInfo& p) {
	j.at("name").get_to(p.name);
	j.at("path").get_to(p.path);
	j.at("version").get_to(p.version);
}

ProjectMgr* ProjectMgr::instance_ = new ProjectMgr();
ProjectMgr* ProjectMgr::GetInstance()
{
	return instance_;
}

ProjectMgr::ProjectMgr()
{

}

ProjectMgr::~ProjectMgr()
{
}

void ProjectMgr::LoadConfig()
{
	auto fileSys = Tiny2D::BaseApplication::GetInstance()->GetContext()->GetFileSystem();
	std::string config = fileSys->GetUserDocumentsDir() + "tiny2d_editor.json";
	if (fileSys->FileExists(config))
	{
		auto cf = fileSys->GetFile(config, true);
		if (cf && cf->Open())
		{
			auto json_ = nlohmann::json::parse(std::move(cf->ReadString()));

			if (json_.count("LastDir") > 0)
			{
				lastDir_ = json_["LastDir"].get<std::string>();
			}

			if (json_["projects"].is_array())
			{
				for (nlohmann::json::iterator it = json_["projects"].begin(); it != json_["projects"].end(); ++it) {
					auto path = (*it)["path"].get<std::string>();
					if (fileSys->DirExists(path))
					{
						ProjectInfo pi;
						GetProjInfo(pi, path);
						projList_.push_back(pi);
					}
				}
			}
			cf->Close();
		}

	}
}

void ProjectMgr::SaveConfig()
{
	auto context = Tiny2D::BaseApplication::GetInstance()->GetContext();
	auto fileSys = context->GetFileSystem();
	std::string config = fileSys->GetUserDocumentsDir() + "tiny2d_editor.json";

	json out;
	out["LastDir"] = lastDir_;
	for (size_t i = 0; i < projList_.size(); i++)
	{
		out["projects"].push_back(projList_[i]);
	}
	SaveJsonFile(out, config);
}

void ProjectMgr::OnGui()
{
	auto context = Tiny2D::BaseApplication::GetInstance()->GetContext();
	auto resCache = context->GetResourceCache();

	ImGui::PushStyleVar(ImGuiStyleVar_WindowMinSize, ImVec2(300, 80));
	if (show_)
		ImGui::OpenPopup("ProjectMgr");
	if (ImGui::BeginPopupModal("ProjectMgr", nullptr))
	{
		if (ImGui::ListBoxHeader("", { 0.f, 5 * 60.f }))
		{

			for (size_t i = 0; i < projList_.size(); i++)
			{
				bool select;
				const ProjectInfo& proj = projList_[i];
				auto tex = resCache->GetResource<Texture2D>("icon.png");
				//ImGui::SameLine();
				if (ImGui::Selectable(("###" + proj.name).c_str(), &select, ImGuiSelectableFlags_SpanAllColumns | ImGuiSelectableFlags_AllowItemOverlap | ImGuiSelectableFlags_AllowDoubleClick, { 0, 60 }))
				{
					if (ImGui::IsMouseDoubleClicked(0))
					{
						openProj_ = &projList_[i];
						show_ = false;
						ImGui::CloseCurrentPopup();
						projSignal.Emit(projList_[i]);
					}
				}
				ImGui::SameLine();
				ImGui::Image((ImTextureID)tex->GetTextureID(), { 55, 55 }, { 0,1 }, { 1,0 });
				ImGui::SameLine();
				ImGui::BeginGroup();
				ImGui::Text(proj.name.c_str());
				ImGui::Text(proj.path.c_str());
				ImGui::Text(proj.version.c_str());
				ImGui::EndGroup();
			}
			ImGui::ListBoxFooter();
		}
		ImGui::SameLine();

		ImGui::BeginGroup();
		{
			if (ImGui::Button("New Project"))
			{
				showCreate_ = true;
			}

			if (ImGui::Button("Import Project"))
			{
				showImport_ = true;
				igfd::ImGuiFileDialog::Instance()->OpenModal("ImportDir", "Choose Project Dir", nullptr, ".");
			}
		}
		ImGui::EndGroup();

		if (showCreate_)
		{
			ImGui::OpenPopup("Create New Project");
			if (ImGui::BeginPopupModal("Create New Project", &showCreate_))
			{
				if (lastDir_ != "")
				{
					for (size_t i = 0; i < lastDir_.size() && i < StrLength; i++)
					{
						createPath_[i] = lastDir_[i];
					}
				}

				ImGui::InputTextEx("ProjectName", "New Game Project", createName_, StrLength, ImVec2(), ImGuiInputTextFlags_EnterReturnsTrue);
				ImGui::InputTextEx("ProjectPath", "New Game Project Path", createPath_, StrLength, ImVec2(), ImGuiInputTextFlags_EnterReturnsTrue);
				ImGui::SameLine();
				if (ImGui::Button("Browse"))
				{
					igfd::ImGuiFileDialog::Instance()->OpenModal("ChooseDir", "Choose Project Dir", nullptr, ".");
				}

				// display
				if (igfd::ImGuiFileDialog::Instance()->FileDialog("ChooseDir"))
				{
					// action if OK
					if (igfd::ImGuiFileDialog::Instance()->IsOk == true)
					{
						std::string filePathName = igfd::ImGuiFileDialog::Instance()->GetFilePathName();
						std::string filePath = igfd::ImGuiFileDialog::Instance()->GetCurrentPath();

						lastDir_ = filePath;

						int i;
						for (i = 0; i < filePath.length(); i++)
						{
							createPath_[i] = filePath[i];
						}
						createPath_[i] = '\0';

					}
					// close
					igfd::ImGuiFileDialog::Instance()->CloseDialog("ChooseDir");
				}
				if (ImGui::Button("CreateProject"))
				{
					InitProject(createPath_, createName_);
					SaveConfig();
					showCreate_ = false;
				}
				ImGui::SameLine();
				if (ImGui::Button("Cancle"))
				{
					ImGui::CloseCurrentPopup();
					showCreate_ = false;
				}
				ImGui::EndPopup();
			}
		}


		if (igfd::ImGuiFileDialog::Instance()->FileDialog("ImportDir"))
		{
			if (igfd::ImGuiFileDialog::Instance()->IsOk == true)
			{
				std::string filePathName = igfd::ImGuiFileDialog::Instance()->GetFilePathName();
				std::string filePath = igfd::ImGuiFileDialog::Instance()->GetCurrentPath();

				if (filePath.size() < StrLength)
				{
					if (ImportProject(filePath))
					{
						SaveConfig();
						showImport_ = false;
					}
				}

			}
			igfd::ImGuiFileDialog::Instance()->CloseDialog("ImportDir");
		}

		ImGui::EndPopup();
	}
	ImGui::PopStyleVar();
}

bool ProjectMgr::InitProject(std::string path, std::string name)
{
	if (path != "")
	{
		auto context = Tiny2D::BaseApplication::GetInstance()->GetContext();
		auto fileSys = context->GetFileSystem();
		auto fulldir = path + Tiny2D::FileSystem::Sep + name;
		std::string config = fulldir + Tiny2D::FileSystem::Sep + "tiny2d_init.json";
		fileSys->CreateDir(fulldir);
		fileSys->CreateDir(fulldir + Tiny2D::FileSystem::Sep + "Resources");
		if (!fileSys->FileExists(config))
		{
			json init;
			init["version"] = "1.0.0.0";
			init["name"] = name;
			SaveJsonFile(init, config);
			ProjectInfo pi;
			GetProjInfo(pi, fulldir);
			projList_.push_back(pi);
			return true;
		}
	}

	return false;
}

bool ProjectMgr::ImportProject(std::string path)
{
	ProjectInfo pi;
	if (!ProjExist(path) && GetProjInfo(pi, path))
	{
		projList_.push_back(pi);
		return true;
	}
	return false;
}

bool ProjectMgr::GetProjInfo(ProjectInfo& pi, std::string path)
{
	if (path != "")
	{
		auto context = Tiny2D::BaseApplication::GetInstance()->GetContext();
		auto fileSys = context->GetFileSystem();
		std::string config = path + Tiny2D::FileSystem::Sep + "tiny2d_init.json";
		auto cf = fileSys->GetFile(config, true);
		if (cf && cf->Open())
		{
			auto json = nlohmann::json::parse(std::move(cf->ReadString()));
			if (json.find("name") != json.end())
			{
				pi.name = json["name"].get<std::string>();
			}
			if (json.find("version") != json.end())
			{
				pi.version = json["version"].get<std::string>();
			}
			pi.path = path;
			cf->Close();
			return true;
		}
	}

	return false;
}

bool ProjectMgr::SaveJsonFile(const json& j, std::string path)
{
	auto context = Tiny2D::BaseApplication::GetInstance()->GetContext();
	auto fileSys = context->GetFileSystem();
	auto cf = new Tiny2D::File(context, path);
	if (cf && cf->Open(Tiny2D::FILE_WRITE))
	{
		auto outstr = std::move(j.dump(4));
		cf->Write(outstr.c_str(), outstr.length());
		cf->Close();
		return true;
	}
	return false;
}

bool ProjectMgr::ProjExist(std::string fullpath)
{
	for (size_t i = 0; i < projList_.size(); i++)
	{
		if (projList_[i].path == fullpath)
		{
			return true;
		}
	}
	return false;
}
