#include "GameApp.h"
#include <stdio.h>
#include <tchar.h>
#include <stdio.h>
#include <stdlib.h>
#include "Context.h"
#include "FileSystem.h"
#include "Image.h"
#include "Shader.h"
#include "ResourceCache.h"
#include "Camera.h"
#include "Graphics.h"
#include <stb_image.h>
#include "UI.h"
#include "Engine.h"
#include "Texture2D.h"
#include "Sprite.h"
#include <SDL.h>
#include <SDL_video.h>
#include "UIElement.h"

namespace Tiny2D {
	GfxConfiguration config(8, 8, 8, 8, 32, 0, 0, 960, 540, _T("Game Engine From Scratch (Windows)"));
	IApplication* g_pApp = static_cast<IApplication*>(new GameApp(config));
	Context* g_pContext;
	Engine* g_engine;
}

int Tiny2D::GameApp::Initialize()
{
	SDLApplication::Initialize();
	g_pContext = new Context();
	g_engine = new Engine();
	g_engine->SetContext(g_pContext);
	g_engine->Initialize();
	g_engine->Resize(winWidth_, winHeight_);

	char path[MAX_PATH];
	path[0] = 0;
	GetCurrentDirectory(MAX_PATH, path);
	std::cout << path << std::endl;
	std::string resourcePath = std::string(path) + "/../../../Resources";
	std::string resourcePath2 = std::string(path) + "/Resources";
	g_pContext->GetFileSystem()->addSearchePath(resourcePath);
	g_pContext->GetFileSystem()->addSearchePath(resourcePath2);

	auto resCache = g_pContext->GetResourceCache();
	auto tex = resCache->GetResource<Texture2D>("test.png");
	auto ui = g_pContext->GetUI();

	srand((int)(0));
	for (size_t i = 0; i < 100; i++)
	{
		auto sprite = new Sprite(g_pContext);
		sprite->SetTexture(tex);
		ui->GetRootElement()->AddChild(sprite);
		sprite->SetPosition(glm::ivec2(rand() % 400 - 200, rand() % 400 - 200));
		sprite->SetRotation(rand() % 360);
		float scale = (rand() % (100) + 5) / (float)(30);
		
		sprite->SetScale(glm::vec2(scale, scale));
		for (size_t i = 0; i < 4; i++)
		{
			float colorr = (rand() % (20)) / (float)(20);
			float colorg = (rand() % (20)) / (float)(20);
			float colorb = (rand() % (20)) / (float)(20);
			Color c(colorr, colorg, colorb, 1.0);
			sprite->SetColor(Corner(i), c);
		}
	}


	return 0;
}

void Tiny2D::GameApp::Finalize()
{
	SDLApplication::Finalize();
}

void Tiny2D::GameApp::Tick()
{
	SDLApplication::Tick();
	g_engine->Tick();
}

void Tiny2D::GameApp::MouseMove(int x, int y)
{

}
