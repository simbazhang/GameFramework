#pragma once
#include "json.hpp"
#include <string>
#include "Signal.h"
#include "Common.h"

struct ProjectInfo
{
	std::string name{};
	std::string path{};
	std::string version{};
};

class ProjectMgr
{
public:
	static ProjectMgr* GetInstance();
	ProjectMgr();
	~ProjectMgr();

	void LoadConfig();
	void SaveConfig();
	void OnGui();
	bool InitProject(std::string path, std::string name);
	bool ImportProject(std::string path);
	bool GetProjInfo(ProjectInfo& pi, std::string path);
public:
	Tiny2D::Signal<ProjectInfo> projSignal{};
private:
	bool SaveJsonFile(const nlohmann::json& j, std::string path);
	bool ProjExist(std::string fullpath);

	char createName_[StrLength]{};
	char createPath_[StrLength]{};
	bool show_{ true };
	bool showCreate_{ false };
	bool showImport_{ false };
	std::string lastDir_{};
	std::vector<ProjectInfo> projList_{};
	ProjectInfo* openProj_{};
	static ProjectMgr* instance_;
};