#pragma once

#include <iostream>
#include <string>

#include "FileSystem.h"
#include "Context.h"
#include "ProjectFilesView.h"
#include "imgui.h"
#include "imgui_internal.h"
#include "Graphics.h"
#include "Texture2D.h"
#include "UIElement.h"
#include "Engine.h"
#include "UI.h"
#include "ImGuiFileDialog.h"
#include "../UIHelper.hpp"
#include "../IconsFontAwesome5.h"

using namespace Tiny2D;

namespace Tiny2DEditor {
	ProjectFilesView::ProjectFilesView(Context* context, EditorContext* editorContext):BaseView(context, editorContext)
	{
		//context_->GetFileSystem()->ScanDir("*.*", listOut_);
	}
	void ProjectFilesView::SetRootPath(std::string path)
	{
		if (rootPath_ != path)
		{
			rootPath_ = path;
			editorFileInfo_.clear();
			std::vector<FileInfoStruct> out;
			context_->GetFileSystem()->ListFiles(path.c_str(), out);
			for (size_t i = 0; i < out.size(); i++)
			{
				if (out[i].fileName != "..")
				{
					EditorFileInfo info;
					info.CopyFrom(out[i]);
					editorFileInfo_.emplace_back(info);
				}
			}
		}
		
	}
	void ProjectFilesView::OnGui()
	{
		ImGuiIO& io = ImGui::GetIO();
		ImVec2 curwinSize;
		auto graphics = context_->GetGraphics();

		ImGui::Begin("ProjectFilesView", nullptr);
		{
            popCreateID_ = ImGui::GetCurrentWindow()->GetID("CreateFile");
			if(ImGui::BeginPopup("CreateFile"))
			{
				if (ImGui::MenuItem("CreateScene"))
				{
					SignalFileAction action{};
					action.action = "CreateScene";
					action.info = selectEditorFileInfo_;
					SignalFile.Emit(action);
				}
				ImGui::EndPopup();
			}

			for (size_t i = 0; i < editorFileInfo_.size(); i++)
			{
				RenderItem(editorFileInfo_[i]);
			}
			ImGui::End();
		}
	}


	void ProjectFilesView::OnMouseDown(int x, int y)
	{
		
	}

	void ProjectFilesView::OnMouseMove(int x, int y)
	{
		
	}

	void ProjectFilesView::OnMouseUp(int x, int y)
	{
	}

	void ProjectFilesView::OnSave()
	{
	}

	bool ProjectFilesView::RenderItem(EditorFileInfo& info)
	{
        
		bool popCreateMenu = false;
		ImGui::AlignTextToFramePadding();
		unsigned flag = ImGuiTreeNodeFlags_AllowItemOverlap;
		if (info.type != 'd')
		{
            flag = (flag|ImGuiTreeNodeFlags_Leaf);

		}
        
        std::string text;
        
		
		if (info.type == 'd')
		{
			if (info.expansion)
			{
				text = std::string(ICON_FA_FOLDER_OPEN) + " " + info.fileName;
			}
			else
			{
				text = std::string(ICON_FA_FOLDER_MINUS) + " " + info.fileName;
			}
		}
		else if (info.type == 'f')
		{
			text = std::string(ICON_FA_FILE) + " " + info.fileName;
		}
		else
		{
            text = "? " + info.fileName;
		}
        auto id = ImGui::GetCurrentWindow()->GetID((info.filePath + info.fileName).c_str());
        bool open = ImGui::TreeNodeBehavior(id, flag, text.c_str(), nullptr);
        
        info.expansion = open;
      
        if(info.type == 'd')
        {
            ImGui::SameLine();
            ImGui::PushID(id);
			auto rect = ImGui::GetCurrentWindow()->Rect();
			ImGui::SetCursorPosX(rect.GetWidth() - 30);
            if (ImGui::Button("+"))
            {
                selectEditorFileInfo_ = &info;
                popCreateMenu = true;
            }
            ImGui::PopID();
            
            if(open)
            {
                if(info.dirty)
                {
                    info.chilren.clear();
                    std::vector<FileInfoStruct> out;
                    std::string childpath = info.filePath + FileSystem::Sep + info.fileName;
                    context_->GetFileSystem()->ListFiles(childpath.c_str(), out);
                    for (size_t i = 0; i < out.size(); i++)
                    {
                        if (out[i].fileName != "..")
                        {
                            EditorFileInfo child;
                            child.CopyFrom(out[i]);
                            info.chilren.emplace_back(child);
                        }
                    }
                    info.dirty = false;
                }
                for (int i=0; i < info.chilren.size(); ++i) {
                    RenderItem(info.chilren[i]);
                }
            }
            else
            {
                info.dirty = true;
            }
        }
        
        
		//if (ImGui::IsItemHovered()) ImGui::SetTooltip("CreateFile");
		if (open)
		{
			ImGui::TreePop();
		}

		if (popCreateMenu)
		{
			ImGui::OpenPopupEx(popCreateID_);
		}

		return open;
	}
}
