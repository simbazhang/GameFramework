#pragma once

#include <iostream>
#include <string>

#include "FileSystem.h"
#include "Context.h"
#include "SceneTreeView.h"
#include "imgui.h"
#include "imgui_internal.h"
#include "Graphics.h"
#include "Texture2D.h"
#include "UIElement.h"
#include "Engine.h"
#include "UI.h"
#include "ImGuiFileDialog.h"
#include "Render.h"
#include "GameObject.h"

using namespace Tiny2D;
static Context* sContext = nullptr;
namespace Tiny2DEditor {
	SceneTreeView::SceneTreeView(Context* context, EditorContext* editorContext):BaseView(context, editorContext)
	{
        sContext = context;
	}

	void TreeItem(UIElement* elem, EditorContext* editor);
	void TreeItem(UIElement* elem, EditorContext* editor)
	{
		if (elem)
		{
			auto children = elem->GetChildren();
			auto node_flags = ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_OpenOnArrow; // ImGuiTreeNodeFlags_Bullet
			if(elem == editor->select)
            {
                node_flags = node_flags | ImGuiTreeNodeFlags_Selected;
            }
            
            if (children.size() == 0)
			{
				node_flags = node_flags | ImGuiTreeNodeFlags_Leaf;
			}

            bool node_opened = (ImGui::TreeNodeEx((void*)(elem), node_flags, (elem->GetName().c_str())));
                
            if (ImGui::IsItemClicked() && elem != sContext->GetUI()->GetRootElement())
			{
                editor->select = elem;
			}
            
            if (ImGui::BeginDragDropSource())
            {
                ImGui::Text("This is a drag and drop source");
                ImGui::SetDragDropPayload("_TREENODE", &elem, sizeof(elem));
                ImGui::EndDragDropSource();
            }
            else if(ImGui::BeginDragDropTarget())
            {
                
                const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("_TREENODE");
                if (payload && payload->Data)
                {
                    UIElement* eledrag = *(UIElement**)(payload->Data);
                    elem->AddChild(eledrag);
                }
                ImGui::EndDragDropTarget();
            }
            
            if(node_opened)
            {
                for (size_t i = 0; i < children.size(); i++)
                {
                    TreeItem(children[i], editor);
                }
                ImGui::TreePop();
                
            }
		}
	}

    void TreeItem(GameObject* elem, EditorContext* editor);
    void TreeItem(GameObject* elem, EditorContext* editor)
    {
        if (elem)
        {
            auto children = elem->GetChildren();
            auto node_flags = ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_OpenOnArrow; // ImGuiTreeNodeFlags_Bullet
            if(editor && elem == editor->select)
            {
                node_flags = node_flags | ImGuiTreeNodeFlags_Selected;
            }
            
            if (children.size() == 0)
            {
                node_flags = node_flags | ImGuiTreeNodeFlags_Leaf;
            }

            bool node_opened = (ImGui::TreeNodeEx((void*)(elem), node_flags, (elem->GetName().c_str())));
                
            if (ImGui::IsItemClicked() && elem != sContext->GetRender()->GetRootObject())
            {
                editor->select = elem;
            }
            
            if (ImGui::BeginDragDropSource())
            {
                ImGui::Text("This is a drag and drop source");
                ImGui::SetDragDropPayload("GAMEOBJECT", &elem, sizeof(elem));
                ImGui::EndDragDropSource();
            }
            else if(ImGui::BeginDragDropTarget())
            {
                
                const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("GAMEOBJECT");
                if (payload && payload->Data)
                {
                    GameObject* eledrag = *(GameObject**)(payload->Data);
                    elem->AddChild(eledrag);
                }
                ImGui::EndDragDropTarget();
            }
            
            if(node_opened)
            {
                for (size_t i = 0; i < children.size(); i++)
                {
                    TreeItem(children[i], editor);
                }
                ImGui::TreePop();
                
            }
        }
    }

	void SceneTreeView::OnGui()
	{
		ImGuiIO& io = ImGui::GetIO();
		ImVec2 curwinSize;
		auto graphics = context_->GetGraphics();
		auto ui = context_->GetUI();
        auto render = context_->GetRender();
        ImGui::Begin("SceneTreeView");
        {
            if (render->GetRootObject())
            {
                TreeItem(render->GetRootObject(), editorContext_);
            }
            if (ui->GetRootElement())
            {
                TreeItem(ui->GetRootElement(), editorContext_);
            }
        }
        ImGui::End();
       
	}


	void SceneTreeView::OnMouseDown(int x, int y)
	{
		
	}

	void SceneTreeView::OnMouseMove(int x, int y)
	{
		
	}

	void SceneTreeView::OnMouseUp(int x, int y)
	{
	}

	void SceneTreeView::OnSave()
	{
	}
}
