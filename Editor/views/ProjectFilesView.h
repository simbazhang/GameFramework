#pragma once

#include <iostream>
#include <string>

#include "FileSystem.h"
#include "Context.h"
#include "BaseEditor.h"
#include "Signal.h"

using namespace Tiny2D;

namespace Tiny2DEditor {
	struct EditorFileInfo : public FileInfoStruct
	{
		void CopyFrom(const FileInfoStruct& info)
		{
			type = info.type;
			filePath = info.filePath;
			fileName = info.fileName;
			fileName_optimized = info.fileName_optimized;
			ext = info.ext;
			fileSize = info.fileSize;
			formatedFileSize = info.formatedFileSize;
			fileModifDate = info.fileModifDate;
		}

		bool expansion{ false };
		std::vector<EditorFileInfo> chilren{};
		bool dirty{ true };
	};

	struct SignalFileAction
	{
		std::string action;
		EditorFileInfo* info;
	};

	class ProjectFilesView : public BaseView
	{
	public:
		ProjectFilesView(Context* context, EditorContext* editorContext);

		void SetRootPath(std::string path);
		virtual void OnGui() override;
		virtual void OnMouseDown(int x, int y)override;
		virtual void OnMouseMove(int x, int y)override;
		virtual void OnMouseUp(int x, int y)override;
		virtual void OnSave() override;

		bool RenderItem(EditorFileInfo& info);

	public:
		Signal<SignalFileAction> SignalFile;
	private:
		std::string rootPath_{};
		std::vector<EditorFileInfo> editorFileInfo_{};
		EditorFileInfo* selectEditorFileInfo_{};
        unsigned popCreateID_{};
	};
}
