#pragma once

#include <iostream>
#include <string>

#include "FileSystem.h"
#include "Context.h"
#include "BaseEditor.h"

using namespace Tiny2D;

namespace Tiny2DEditor {
	class SceneTreeView : public BaseView
	{
	public:
		SceneTreeView(Context* context, EditorContext* editorContext);

		virtual void OnGui() override;
		virtual void OnMouseDown(int x, int y)override;
		virtual void OnMouseMove(int x, int y)override;
		virtual void OnMouseUp(int x, int y)override;
		virtual void OnSave() override;
	private:
	};
}