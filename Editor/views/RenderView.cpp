#pragma once

#include <iostream>
#include <string>

#include "FileSystem.h"
#include "Context.h"
#include "RenderView.h"
#include "imgui.h"
#include "imgui_internal.h"
#include "Graphics.h"
#include "Texture2D.h"
#include "UIElement.h"
#include "Engine.h"
#include "UI.h"
#include <GLFW/glfw3.h>
#include "BaseApplication.hpp"

using namespace Tiny2D;

namespace Tiny2DEditor {
	static RenderSurface* renderRS = nullptr;
	RenderView::RenderView(Context* context, EditorContext* editorContext):BaseView(context, editorContext)
	{
		
	}

	void RenderView::OnGui(std::string name, bool& open, bool& show)
	{
		
		if (!renderRS)
		{
			renderRS = context_->GetGraphics()->CreateRenderSurface(0, 0);
			context_->GetGraphics()->PushRenderSurface(renderRS);
		}
		ImGuiIO& io = ImGui::GetIO();
		ImVec2 curwinSize;
		auto graphics = context_->GetGraphics();
		auto ui = context_->GetUI();
		
		show = ImGui::Begin(name.c_str(), &open, ImGuiWindowFlags_NoMove);
		{
			ImDrawList* draw_list = ImGui::GetWindowDrawList();
			static bool opt_enable_grid = true;
			
			editorContext_->renderWin = ImGui::GetCurrentWindowRead();
			curWindow_ = ImGui::GetCurrentWindowRead();
			curwinSize = curWindow_->Size;
			curwinSize.x -= curWindow_->WindowPadding.x * 2;
			curwinSize.y -= (curWindow_->MenuBarHeight() + curWindow_->TitleBarHeight() + curWindow_->WindowPadding.y * 2);
			auto g_offsetX = curWindow_->WindowPadding.x;
			auto g_offsetY = (curWindow_->MenuBarHeight() + curWindow_->TitleBarHeight() + curWindow_->WindowPadding.y);

			ImVec2 canvasPos = ImGui::GetCursorScreenPos();

			auto quad = renderRS;
			if (quad && quad->colorAtt)
			{
				ImGui::Image((ImTextureID)(quad->colorAtt->GetTextureID()), ImVec2(quad->colorAtt->GetWidth(), quad->colorAtt->GetHeight()), ImVec2(0, 1), ImVec2(1, 0));
			}

			// ImGui::Checkbox("Enable grid", &opt_enable_grid);
			if (opt_enable_grid)
			{
				
				glm::vec2 o = ui->GetRootElement()->ElementToScreen(glm::vec2(0, 0));
				o.y = renderSize_.y - o.y  + canvasPos.y;
				o.x = o.x + canvasPos.x;

				draw_list->AddLine(ImVec2(canvasPos.x, o.y), ImVec2(canvasPos.x + curwinSize.x, o.y), IM_COL32(255, 255, 255, 100));
				draw_list->AddLine(ImVec2(o.x, canvasPos.y), ImVec2(o.x, canvasPos.y + curwinSize.y), IM_COL32(255, 255, 255, 100));
			}
            
            auto g_select = dynamic_cast<UIElement*>(editorContext_->select);
			if (g_select)
			{
				static ImVec4 colf = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
				const ImU32 col = ImColor(colf);

				glm::vec2 corners[4];
				corners[0] = g_select->GetCornerPosition(Corner::C_TOPLEFT);
				corners[1] = g_select->GetCornerPosition(Corner::C_TOPRIGHT);
				corners[2] = g_select->GetCornerPosition(Corner::C_BOTTOMRIGHT);
				corners[3] = g_select->GetCornerPosition(Corner::C_BOTTOMLEFT);

				corners[0] = g_select->ElementToScreen(corners[0]);
				corners[1] = g_select->ElementToScreen(corners[1]);
				corners[2] = g_select->ElementToScreen(corners[2]);
				corners[3] = g_select->ElementToScreen(corners[3]);

				corners[0].y = renderSize_.y - corners[0].y + curWindow_->Pos.y + g_offsetY;
				corners[1].y = renderSize_.y - corners[1].y + curWindow_->Pos.y + g_offsetY;
				corners[2].y = renderSize_.y - corners[2].y + curWindow_->Pos.y + g_offsetY;
				corners[3].y = renderSize_.y - corners[3].y + curWindow_->Pos.y + g_offsetY;

				corners[0].x += (curWindow_->Pos.x + g_offsetX);
				corners[1].x += (curWindow_->Pos.x + g_offsetX);
				corners[2].x += (curWindow_->Pos.x + g_offsetX);
				corners[3].x += (curWindow_->Pos.x + g_offsetX);

				draw_list->AddLine(ImVec2(corners[0].x, corners[0].y), ImVec2(corners[1].x, corners[1].y), col, 1);
				draw_list->AddLine(ImVec2(corners[1].x, corners[1].y), ImVec2(corners[2].x, corners[2].y), col, 1);
				draw_list->AddLine(ImVec2(corners[2].x, corners[2].y), ImVec2(corners[3].x, corners[3].y), col, 1);
				draw_list->AddLine(ImVec2(corners[3].x, corners[3].y), ImVec2(corners[0].x, corners[0].y), col, 1);
			}
			ImGui::End();
		}

		if ((renderSize_.x != curwinSize.x
			|| renderSize_.y != curwinSize.y) && io.MouseDown[0] == false && curwinSize.x > 0 && curwinSize.y > 0)
		{
			renderSize_ = curwinSize;
			graphics->ResizeRenderSurface(renderRS, renderSize_.x, renderSize_.y);
			BaseApplication::GetInstance()->GetEngine()->Resize(renderSize_.x, renderSize_.y);
		}

		ImGuiContext& g = *GImGui;
		if (curWindow_ == g.HoveredWindow)
		{
			if (io.KeysDown[GLFW_KEY_SPACE])
			{
				ImGui::SetMouseCursor(ImGuiMouseCursor_Hand);
				if (io.MouseWheel != 0)
				{
					ui->SetUIScale(ui->GetUIScale() + io.MouseWheel / 50.f);
				}
			}
		}
		
	}


	void RenderView::OnMouseDown(int x, int y)
	{
		
		ImGuiContext& g = *GImGui;
		//if (curWindow_ != g.NavWindow)
		if (curWindow_ != g.HoveredWindow)
		{
			return;
		}

		UpdateCursorPos(x, y);
		std::cout << "mouse down " << x << " " << y << std::endl;
        auto ui = context_->GetUI();
        
		if (ImGui::GetMouseCursor() == ImGuiMouseCursor_Arrow)
		{
            UIElement* select = dynamic_cast<UIElement*>(editorContext_->select);
            bool selectLast = false;
            if(select)
            {
                selectLast = ui->ToucheTest(select, glm::vec2(cursorPos_.x, cursorPos_.y));
            }
            
            if(!selectLast)
            {
                select = nullptr;
                ui->GetElementAt(select, ui->GetRootElement(), glm::vec2(cursorPos_.x, cursorPos_.y));
            }
			
			if (select)
			{
				selectUIPos_ = select->GetPosition();
				touchStartPos_ = select->GetParent()->ScreenToElement(glm::vec2(cursorPos_.x, cursorPos_.y));
			}
            
            editorContext_->select = select;
		}
	}

	void RenderView::OnMouseMove(int x, int y)
	{
		ImGuiContext& g = *GImGui;
		if (curWindow_ != g.NavWindow)
		{
			return;
		}
		ImGuiIO& io = ImGui::GetIO();
		UpdateCursorPos(x, y);
		
		auto ui = context_->GetUI();
        UIElement* select = dynamic_cast<UIElement*>(editorContext_->select);
		if (ImGui::GetMouseCursor() == ImGuiMouseCursor_Hand && io.MouseDown[0])
		{
			float x = io.MouseDelta.x;
			float y = -io.MouseDelta.y;
			ui->SetUIOffset(ui->GetUIOffset() + glm::vec2(x, y));
		}
		else if (select && io.MouseDown[0])
		{
            if(select)
            {
                glm::vec2 targetpos = select->GetParent()->ScreenToElement(glm::vec2(cursorPos_.x, cursorPos_.y));
                select->SetPosition(selectUIPos_ +  targetpos - touchStartPos_);
            }
		}
		
	}

	void RenderView::OnMouseUp(int x, int y)
	{
	}

	void RenderView::OnSave()
	{
	}
	void RenderView::UpdateCursorPos(int x, int y)
	{
		if (curWindow_)
		{
			int offset = (curWindow_->MenuBarHeight() + curWindow_->TitleBarHeight() + curWindow_->WindowPadding.y);
			auto ui = context_->GetUI();
			x = x - curWindow_->Pos.x - curWindow_->WindowPadding.x;
			y = curWindow_->Size.y - (y - curWindow_->Pos.y) - curWindow_->WindowPadding.y;

			cursorPos_.x = x;
			cursorPos_.y = y;
		}

	}
}
