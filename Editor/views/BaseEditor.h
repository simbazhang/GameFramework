#pragma once

#include <iostream>
#include <string>

#include "FileSystem.h"
#include "Context.h"
#include "UIElement.h"
#include "imgui.h"
#include "imgui_internal.h"
#include "Engine.h"

#include "../EditorContext.h"

using namespace Tiny2D;

namespace Tiny2DEditor {

	class BaseView
	{
	public:
		BaseView(Context* context, EditorContext* editorContext);
		virtual ~BaseView() {}
		virtual void OnGui() {};
		virtual void OnMouseDown(int x, int y) = 0;
		virtual void OnMouseMove(int x, int y) = 0;
		virtual void OnMouseUp(int x, int y) = 0;
		virtual void OnSave() = 0;
	protected:
		Context* context_;
		EditorContext* editorContext_;
	};
}
