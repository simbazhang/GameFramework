#pragma once

#include <iostream>
#include <string>

#include "FileSystem.h"
#include "Context.h"
#include "BaseEditor.h"

using namespace Tiny2D;

namespace Tiny2DEditor {
	class RenderView : public BaseView
	{
	public:
		RenderView(Context* context, EditorContext* editorContext);

		virtual void OnGui(std::string name, bool& open, bool& show);
		virtual void OnMouseDown(int x, int y)override;
		virtual void OnMouseMove(int x, int y)override;
		virtual void OnMouseUp(int x, int y)override;
		virtual void OnSave() override;

		void UpdateCursorPos(int x, int y);
	private:
		ImVec2 renderSize_{};
		ImVec2 cursorPos_{};
		ImGuiWindow* curWindow_{};
		glm::vec2 touchStartPos_{};
		glm::vec2 selectUIPos_{};
	};
}