#pragma once

#include <iostream>
#include <string>

#include "FileSystem.h"
#include "Context.h"
#include "BaseEditor.h"
#include "../UIHelper.hpp"

using namespace Tiny2D;

namespace Tiny2DEditor {
	class PropertyView : public BaseView
	{
	public:
		PropertyView(Context* context, EditorContext* editorContext);

		virtual void OnGui() override;
		virtual void OnMouseDown(int x, int y)override;
		virtual void OnMouseMove(int x, int y)override;
		virtual void OnMouseUp(int x, int y)override;
		virtual void OnSave() override;
	private:
		ImVec2 renderSize_{};
		ImVec2 cursorPos_{};
	};
}