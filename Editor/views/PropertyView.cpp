#pragma once

#include <iostream>
#include <string>

#include "FileSystem.h"
#include "Context.h"
#include "PropertyView.h"
#include "imgui.h"
#include "imgui_internal.h"
#include "Graphics.h"
#include "Texture2D.h"
#include "UIElement.h"
#include "Engine.h"
#include "UI.h"
#include "GameObject.h"

using namespace Tiny2D;

namespace Tiny2DEditor {

	PropertyView::PropertyView(Context* context, EditorContext* editorContext):BaseView(context, editorContext)
	{

	}
	void PropertyView::OnGui()
	{
		ImGuiIO& io = ImGui::GetIO();
		ImVec2 curwinSize;
		auto graphics = context_->GetGraphics();

		ImGui::Begin("Property");
		{
			if (editorContext_->select)
			{
				auto& attrs = editorContext_->select->GetAttributeBind();
				if (ImGui::CollapsingHeader("GameObject"))
				{
					for (auto& attr : attrs)
					{
						UIHelper::UI_DrawIAttr(*attr);
					}
				}
				
				auto gameobject = dynamic_cast<GameObject*>(editorContext_->select);
				if (gameobject)
				{
					auto& comps = gameobject->GetComponents();
					for (size_t i = 0; i < comps.size(); i++)
					{
						auto c = comps[i];
						auto& attrs = c->GetAttributeBind();
						if (ImGui::CollapsingHeader(c->GetComponentName().c_str()))
						{
							for (auto& attr : attrs)
							{
								UIHelper::UI_DrawIAttr(*attr);
							}
						}
					}
				}
			}
			else
			{
				auto& attrs = context_->GetUI()->GetRootElement()->GetAttributeBind();
				if (ImGui::CollapsingHeader("GameObject"))
				{
					for (auto& attr : attrs)
					{
						UIHelper::UI_DrawIAttr(*attr);
					}
				}
			}
		}

		ImGui::End();
	}


	void PropertyView::OnMouseDown(int x, int y)
	{		
		
	}

	void PropertyView::OnMouseMove(int x, int y)
	{
		
	}

	void PropertyView::OnMouseUp(int x, int y)
	{
	}

	void PropertyView::OnSave()
	{
	}
}
