#pragma once
#include "imgui.h"
#include "INIReader.h"
#include <map>
#include <string>
#include "Engine.h"
#include "File.h"

using namespace Tiny2D;

namespace Tiny2DEditor {
	struct CustomColors {
		ImVec4 ComputePass;
		ImVec4 ErrorMessage;
		ImVec4 WarningMessage;
		ImVec4 InfoMessage;
	};

	class ThemeContainer {
	public:
		explicit ThemeContainer();

		std::string LoadTheme(File* file);
		inline const ImGuiStyle& GetUIStyle(const std::string& name)
		{
			return m_ui[name];
		}

		static inline ThemeContainer& Instance()
		{
			static ThemeContainer ret;
			return ret;
		}

		const std::map<std::string, ImGuiStyle>& GetThemes() { return m_ui; }
		ImVec4 ParseColor(const std::string& str);
	private:
		std::map<std::string, ImGuiStyle> m_ui;
	};
}