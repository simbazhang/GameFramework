#pragma once
#include "UIElement.h"
#include "imgui.h"
#include "ThemeContainer.h"
#include "FileSystem.h"
#include "Engine.h"
#include "Context.h"
#include "File.h"
#include <glm/gtc/matrix_transform.hpp>
#include "Shader.h"
#include <glm/gtc/type_ptr.hpp>
#include "AttributeBind.hpp"
#include "Light.h"
#include "docs/DocMgr.h"

using namespace Tiny2D;
using namespace Tiny2DEditor;

#define GLM_IVEC2_TO_IMVEC2(ivec2) ImVec2(ivec2.x, ivec2.y)

class UIHelper
{
public:
	static void UI_NodeTree(UIElement* node)
	{
		if (ImGui::TreeNode("root"))
		{
			const std::vector<UIElement* >& children = node->GetChildren();
			for (const auto& child : children)
			{
				UI_NodeTree(child);
			}
			ImGui::TreePop();
		}

		ImGui::End();
	}

	static void UI_InitStyle(Engine* engine)
	{
		std::vector<FileInfoStruct > themeFiles;
		engine->GetContext()->GetFileSystem()->ListRelativeFiles("themes/", themeFiles);
		for (size_t i = 0; i < themeFiles.size(); i++)
		{
			if (themeFiles[i].type == 'f')
			{
				Tiny2D::File* file = engine->GetContext()->GetFileSystem()->GetFile(themeFiles[i].filePath + themeFiles[i].fileName);
				ThemeContainer::Instance().LoadTheme(file);
			}	
		}
	}

	static void UI_DrawMenu()
	{
		if (ImGui::BeginMenuBar())
		{
			if (ImGui::BeginMenu(u8"Theme"))
			{
				auto& themesMap = ThemeContainer::Instance().GetThemes();
				for (auto& itr : themesMap)
				{
					auto name = itr.first;
					auto style = itr.second;
					if (ImGui::MenuItem(name.c_str()))
					{
						ImGui::GetStyle() = style;
					}
				}
				ImGui::EndMenu();
			}
			ImGui::EndMenuBar();
		}
	}

	static void UI_DrawLight(Light& attr)
	{

	}

	static void UI_DrawIAttr(AttributeBind& attr)
	{
		ImGui::Columns(2);
		ImGui::Text(attr.name.c_str());
		ImGui::NextColumn();

		ImGui::PushItemWidth(-1);
		if (!attr.inited)
		{
			attr.get();
			attr.inited = true;
		}
		if (attr.value.type == VariantType::IVEC2)
		{
			if (ImGui::DragInt2(("##_" + attr.name).c_str(), glm::value_ptr(attr.value.v.v2i)))
				attr.set();
		}
		else if (attr.value.type == VariantType::UVEC2)
		{
			if (ImGui::DragFloat2(("##_" + attr.name).c_str(), glm::value_ptr(attr.value.v.v2), 1.0f, 0, 999999))
				attr.set();
		}
		else if (attr.value.type == VariantType::VEC2)
		{
			if (ImGui::DragFloat2(("##_" + attr.name).c_str(), glm::value_ptr(attr.value.v.v2), 0.2f))
				attr.set();
		}
        else if (attr.value.type == VariantType::VEC3)
        {
            if (ImGui::DragFloat3(("##_" + attr.name).c_str(), glm::value_ptr(attr.value.v.v3), 0.1f))
                attr.set();
        }
		else if (attr.value.type == VariantType::FLOAT)
		{
			if (ImGui::DragFloat(("##_" + attr.name).c_str(), &attr.value.v.f))
				attr.set();
		}
		else if (attr.value.type == VariantType::BOOL)
		{
			if (ImGui::Checkbox(("##_" + attr.name).c_str(), &attr.value.v.b))
				attr.set();
		}
		else if (attr.value.type == VariantType::STR)
		{
			if (ImGui::InputText(("##_" + attr.name).c_str(), attr.value.v.str, 24))
				attr.set();
		}
		else if (attr.value.type == VariantType::COLOR)
		{
			float data[] = { 1.0f, 1.0f, 1.0f, 1.0f };
			data[0] = attr.value.v.color.r_;
			data[1] = attr.value.v.color.g_;
			data[2] = attr.value.v.color.b_;
			data[3] = attr.value.v.color.a_;
			if (ImGui::ColorEdit4(("##_" + attr.name).c_str(), data))
				attr.value.v.color.r_ = data[0];
				attr.value.v.color.g_ = data[1];
				attr.value.v.color.b_ = data[2];
				attr.value.v.color.a_ = data[3];
				attr.set();
		}
		ImGui::PopItemWidth();
		ImGui::Columns(1);
		ImGui::NextColumn();
		ImGui::Separator();
	}
};

