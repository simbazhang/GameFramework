#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include "Engine.h"
#include "GLFWApplication.hpp"

using namespace Tiny2D;

namespace Tiny2D {
	extern GfxConfiguration config;
}

GLFWApplication* GLFWApplication::sInstance = nullptr;

// glfw: whenever the window_ size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    if(GLFWApplication::sInstance)
    {
        GLFWApplication::sInstance->onSizeChange(width, height);
    }
}

int Tiny2D::GLFWApplication::Initialize()
{
    sInstance = this;
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifndef _WIN32    
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif
	winWidth_ = config.screenWidth;
	winHeight_ = config.screenHeight;

    window_ = glfwCreateWindow(config.screenWidth, config.screenHeight, config.appName, NULL, NULL);
    if (window_ == NULL)
    {
        std::cout << "Failed to create GLFW window_" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window_);
    glfwSetFramebufferSizeCallback(window_, framebuffer_size_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGL())
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    } 
    return 0;
}

void Tiny2D::GLFWApplication::Finalize()
{
    glfwDestroyWindow(window_);
    glfwTerminate();
}

void Tiny2D::GLFWApplication::Tick()
{
	if(!glfwWindowShouldClose(window_))
    {
        // input
        // -----
        processInput(window_);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window_);
        glfwPollEvents();
    }
    else
    {
        m_bQuit = true;
    }
}

void Tiny2D::GLFWApplication::processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}


