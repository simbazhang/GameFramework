#pragma once

#include <iostream>
#include <string>
#include "FileSystem.h"
#include "Context.h"
#include "UIElement.h"
#include "imgui.h"
#include "imgui_internal.h"
#include "Engine.h"
using namespace Tiny2D;

namespace Tiny2DEditor {
	struct EditorContext
	{
		Object* select = nullptr;
		ImGuiWindow* renderWin = nullptr;
	};
}
