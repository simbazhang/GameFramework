#include "SceneDoc.h"
#include "../views/PropertyView.h"
#include "../views/RenderView.h"
#include "../views/ProjectFilesView.h"
#include "../views/SceneTreeView.h"
#include "DocMgr.h"

using namespace Tiny2DEditor;

SceneDoc::SceneDoc(Context* cnt, std::string name):BaseDoc(cnt, name)
{
	renderView_ = new RenderView(context_, &editorContext_);
	propView_ = new PropertyView(context_, &editorContext_);
	treeView_ = new SceneTreeView(context_, &editorContext_);
}

SceneDoc::~SceneDoc()
{
}

void SceneDoc::OnGui()
{
	if (!opened_)
	{
		return;
	}
	renderView_->OnGui(docName_, opened_, display_);
}

void SceneDoc::OnSelecGui()
{
	propView_->OnGui();
	treeView_->OnGui();
}

void SceneDoc::OnMouseDown(int x, int y)
{
	renderView_->OnMouseDown(x, y);
}

void SceneDoc::OnMouseMove(int x, int y)
{
	renderView_->OnMouseMove(x, y);
}

void SceneDoc::OnMouseUp(int x, int y)
{
	renderView_->OnMouseUp(x, y);
}
