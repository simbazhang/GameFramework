#pragma once
#include "BaseDoc.h"
#include "../views/BaseEditor.h"

namespace Tiny2DEditor {
	class RenderView;
	class PropertyView;
	class SceneTreeView;
	class SceneDoc : public BaseDoc
	{
	public:
		SceneDoc(Context*, std::string name);
		~SceneDoc();

		virtual void OnEnter() { display_ = true; };
		virtual void OnExit() { display_ = false; };
		virtual void OnSave() {};
		virtual void OnGui();
		virtual void OnSelecGui();

		virtual void OnMouseDown(int x, int y)override;
		virtual void OnMouseMove(int x, int y)override;
		virtual void OnMouseUp(int x, int y)override;

	protected:
		RenderView* renderView_{};
		PropertyView* propView_{};
		SceneTreeView* treeView_{};
		EditorContext editorContext_{};
	};
}