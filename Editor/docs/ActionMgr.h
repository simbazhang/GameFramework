#pragma once
#include "Object.h"
#include "Context.h"
#include "AttributeBind.hpp"
#include <deque>

struct EditorAction
{
	virtual ~EditorAction() {};
	virtual void Do() = 0;
	virtual void Undo() = 0;
	virtual void Redo() = 0;
}; 

template <class T>
struct CreateAction : EditorAction
{
	CreateAction(Tiny2D::Context* context, T* parent)
	{
		parent_ = parent;
        context_ = context;
		parent_->AddRef();
	}

	~CreateAction()
	{
		parent_->ReleaseRef();
		if (create_)
		{
			create_->ReleaseRef();
		}
	}

	void Do()
	{
		create_ = new T(context_);
		parent_->AddChild(create_);
		create_->AddRef();
	}

	void Undo()
	{
		if (create_ && parent_)
		{
			parent_->RemoveChild(create_);
		}
	}

	void Redo()
	{
		if (create_ && parent_)
		{
			parent_->AddChild(create_);
		}
	}

private:
	T* parent_{};
	T* create_{};
    Tiny2D::Context* context_{};
};

template <class ObjectT, class AttrT>
struct AttrAction
{
	AttrAction(Tiny2D::Context* context, ObjectT* target, std::string attrname, const AttrT& setattr)
	{
		target_ = target;
		target_->AddRef();
		set_ = setattr;
		bind_ = target->GetAttributeBind(attrname);
	}

	~AttrAction()
	{
		target_->ReleaseRef();
	}

	void Do()
	{
		if (bind_ && target_)
		{
			bind_->get();
			record_ = bind_->value.Get<AttrT>();
			bind_->value = set_;
			bind_->set();
		}
		
	}

	void Undo()
	{
		if (bind_ && target_)
		{
			bind_->value = record_;
			bind_->set();
		}
	}

	void Redo()
	{
		if (bind_ && target_)
		{
			bind_->value = set_;
			bind_->set();
		}
	}

private:
	ObjectT* target_{};
	AttrT       record_{};
	AttrT       set_{};
	Tiny2D::AttributeBind* bind_{};
};


class ActionMgr
{
public:
	static ActionMgr* GetInstance();
	ActionMgr() {};
	~ActionMgr() {};

	void PushAction(EditorAction*);
	void Undo();
	void Redo();
private:
	static ActionMgr* instance_;
	std::deque<EditorAction*> record_{};
	std::vector<EditorAction*> recovery_{};
};
