#include "BaseDoc.h"

#include "File.h"
#include "FileSystem.h"
#include "BaseApplication.hpp"
#include "Context.h"

using namespace Tiny2D;
using namespace nlohmann;

BaseDoc::BaseDoc(Context* cnt, std::string name):context_(cnt),docName_(name)
{

}

BaseDoc::~BaseDoc()
{
}

bool BaseDoc::OnInit(std::string path)
{
	path_ = path;

	auto file = context_->GetFileSystem()->GetFile(path);
	if (file && file->Open())
	{
		try
		{
			json_ = json::parse((FILE*)(file->GetHandle()));
			init_ = true;
		}
		catch (json::parse_error& e)
		{
			std::cout << e.what() << std::endl;
			init_ = false;
		}
	}
	return init_;
}

void BaseDoc::SetPath(std::string path)
{
	path_ = path;
}
