#pragma once
#include "BaseDoc.h"
#include "json.hpp"
#include "../EditorContext.h"
#include "../views/ProjectFilesView.h"
#include "../Common.h"

namespace Tiny2D {
	class Context;
}

namespace Tiny2DEditor {
	class ProjectFilesView;
	class RenderView;
	class PropertyView;
	class SceneTreeView;
	class DocMgr
	{
	public:
		static DocMgr* GetInstance();
		DocMgr();
		~DocMgr();

		void SwitchToDoc(BaseDoc* doc);
		void CreateDoc(Tiny2D::Context* context, DocType type, std::string name, std::string path);
		void DocDisplayChange(BaseDoc& doc);
		void RemoveDoc(int idx);
		void RemoveDoc(BaseDoc* doc);
		void SetRootPath(std::string root);

		void SaveAll() {};
		void OnGui(Tiny2D::Context* context);

		void OnCreateEvent(SignalFileAction& action);

		void Init(Tiny2D::Context* context);

		void SaveFileGui();

		BaseDoc* GetCurDoc() { return curDoc_; }
	private:
		BaseDoc* curDoc_{};
		BaseDoc* saveDoc_{};
		std::vector<BaseDoc*> docLists_{};

		Tiny2D::Context* context_{};
		ImGuiID mainDockSpaceId_{}; 
		ImGuiID dockLeftId_{};
		ImGuiID dockRightId_{};

		ImGuiID itemTreeAreaId_{};
		ImGuiID resTreeAreaId_{};
		ImGuiID propAreaId_{};
		ImGuiID centerAreaId_{};

		ProjectFilesView* filesView_{};
		char createNameBuffer[StrLength]{};

		unsigned docNum_{ 0 };
		static DocMgr* instance_;
	};
}