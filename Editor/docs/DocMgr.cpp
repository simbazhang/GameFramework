#include "DocMgr.h"
#include "Context.h"
#include "FileSystem.h"
#include "File.h"
#include <iostream>
#include "SceneDoc.h"
#include "../views/ProjectFilesView.h"
#include "../views/PropertyView.h"
#include "../views/RenderView.h"
#include "../views/SceneTreeView.h"
#include <fmt/chrono.h>
#include "imgui_internal.h"
#include <fmt/core.h>

using namespace Tiny2DEditor;

DocMgr* DocMgr::instance_ = new DocMgr();

DocMgr* DocMgr::GetInstance()
{
	return instance_;
}

DocMgr::DocMgr()
{
	
}

DocMgr::~DocMgr()
{

}

void DocMgr::SwitchToDoc(BaseDoc* doc)
{
	if (!doc || curDoc_ == doc)
	{
		return;
	}

	if (curDoc_ == nullptr)
	{
		curDoc_ = doc;
		doc->OnEnter();	
		
	}
	else
	{
		curDoc_->OnExit();
		curDoc_ = doc;
		doc->OnEnter();
	}
}

void DocMgr::CreateDoc(Tiny2D::Context* context, DocType type, std::string name, std::string path)
{
	if (type == DocType::Scene)
	{
		
		auto curDoc = new SceneDoc(context, name);	
		curDoc->SetDocName(name);
		curDoc->SetPath(path);
		curDoc->MakeDirty();
		curDoc->SetOpened(true);
		SwitchToDoc(curDoc);
		docLists_.push_back(curDoc);
		ImGui::DockBuilderDockWindow(name.c_str(), centerAreaId_);
	}
	++docNum_;
}

void DocMgr::DocDisplayChange(BaseDoc& doc)
{
	if (doc.IsDisplay())
	{
		SwitchToDoc(&doc);
	}
	if (!doc.IsOpened())
	{
		RemoveDoc(&doc);
	}

}

void DocMgr::RemoveDoc(int idx)
{
	if (idx >=0 && idx < docLists_.size())
	{
		if (curDoc_ == docLists_[idx])
		{
			curDoc_->OnExit();
			curDoc_ = nullptr;
		}
		delete  docLists_[idx];
		docLists_.erase(docLists_.begin() + idx);

		if (docLists_.size() > 0)
		{
			if (idx == docLists_.size())
			{
				curDoc_ = docLists_[idx - 1];
			}
			else
			{
				curDoc_ = docLists_[idx];
			}
		}		
	}
}

void DocMgr::RemoveDoc(BaseDoc* doc)
{
	for (size_t i = 0; i < docLists_.size(); i++)
	{
		if (docLists_[i] == doc)
		{
			RemoveDoc(i);
			return;
		}
	}
}

void DocMgr::SetRootPath(std::string root)
{
	if (filesView_)
	{
		filesView_->SetRootPath(root);
	}
}

void DocMgr::OnGui(Tiny2D::Context* context)
{
	if (ImGui::BeginMenuBar())
	{
		ImGui::EndMenuBar();
	}

	bool dockSpaceCreated = ImGui::DockBuilderGetNode(mainDockSpaceId_) != nullptr;
	if (!dockSpaceCreated) {
		mainDockSpaceId_ = ImGui::GetID("MainDockSpace");
		ImGui::DockBuilderRemoveNode(mainDockSpaceId_);
		ImGuiDockNodeFlags dockSpaceFlags = 0;
		dockSpaceFlags |= ImGuiDockNodeFlags_NoCloseButton;
		ImGui::DockSpace(mainDockSpaceId_, ImVec2(0, 0));

		ImGui::DockBuilderSplitNode(mainDockSpaceId_, ImGuiDir_Left, 0.2f, &dockLeftId_, &dockRightId_);
		ImGui::DockBuilderSplitNode(dockLeftId_, ImGuiDir_Up, 0.2f, &itemTreeAreaId_, &resTreeAreaId_);
		ImGui::DockBuilderSplitNode(dockRightId_, ImGuiDir_Left, 0.8f, &centerAreaId_, &propAreaId_);
		ImGui::DockBuilderDockWindow("ProjectFilesView", resTreeAreaId_);
		ImGui::DockBuilderDockWindow("SceneTreeView", itemTreeAreaId_);
		ImGui::DockBuilderDockWindow("Property", propAreaId_);
		ImGui::DockBuilderDockWindow("RenderView", centerAreaId_);
	}
	else
	{
		ImGui::DockSpace(mainDockSpaceId_, ImVec2(0, 0));
	}

	if (filesView_)
		filesView_->OnGui();

	for (int i = docLists_.size() - 1; i >= 0; --i)
	{
		docLists_[i]->OnGui();
		if (docLists_[i]->IsDisplay())
		{
			curDoc_ = docLists_[i];
		}
	}

	if(curDoc_)
		curDoc_->OnSelecGui();

	SaveFileGui();
}

void DocMgr::OnCreateEvent(SignalFileAction& action)
{
	std::string name = fmt::format("unsaved_{}.json", docNum_);
	CreateDoc(context_, DocType::Scene, name, action.info->filePath);
}

void DocMgr::Init(Tiny2D::Context* context)
{
	using namespace std::placeholders;
	context_ = context;
	filesView_ = new ProjectFilesView(context_, nullptr);
	filesView_->SignalFile.AddHandler(nullptr, std::bind(&DocMgr::OnCreateEvent, this, _1));
}

void DocMgr::SaveFileGui()
{
	saveDoc_ = nullptr;
	for (int i = docLists_.size() - 1; i >= 0; --i)
	{
		if (!docLists_[i]->IsOpened())
		{
			saveDoc_ = docLists_[i];
			break;
		}
	}

	if (saveDoc_)
	{
		if (saveDoc_->Dirty())
		{
			std::string fullpath = PathJoin({ saveDoc_->GetPath(), saveDoc_->GetDocName() });
			if (!context_->GetFileSystem()->FileExists(fullpath))
			{
				ImGui::OpenPopup("CreateDoc");
			}
		}
	}

	if (ImGui::BeginPopupModal("CreateDoc"))
	{
		std::string name = saveDoc_->GetDocName();
		memcpy(createNameBuffer, name.c_str(), name.length()<= StrLength ? name.length() : StrLength);
		ImGui::InputText("Input Doc Name ", createNameBuffer, StrLength);
		if (ImGui::Button("OK"))
		{
			saveDoc_->OnSave();
			RemoveDoc(saveDoc_);
			ImGui::CloseCurrentPopup();
		}
		ImGui::EndPopup();
	}
}


