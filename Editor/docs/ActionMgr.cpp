#include "ActionMgr.h"

ActionMgr* ActionMgr::instance_ = new ActionMgr();

const int MaxRecord = 50;

ActionMgr* ActionMgr::GetInstance()
{
	return instance_;
}

void ActionMgr::PushAction(EditorAction* action)
{
	if (record_.size() >= MaxRecord)
	{
		record_.pop_front();
	}
	record_.push_back(action);
}

void ActionMgr::Undo()
{
	if (record_.size() > 0)
	{
		EditorAction* act = record_.back();
		act->Undo();
		recovery_.push_back(act);
		record_.pop_back();
	}
}

void ActionMgr::Redo()
{
	if (recovery_.size() > 0)
	{
		EditorAction* act = recovery_.back();
		act->Redo();
		record_.push_back(act);
		recovery_.pop_back();
	}
}
