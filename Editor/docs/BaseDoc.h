#pragma once
#include <string>
#include "json.hpp"
#include "Signal.h"

enum class DocType
{
    Unknown,
	Scene,
	LogicNode
};

namespace Tiny2D {
	class File;
	class Context;
}

using namespace Tiny2D;

class BaseDoc
{
public:
	BaseDoc(Context*, std::string name);
	virtual ~BaseDoc();

	virtual bool OnInit(std::string path);

	virtual void OnEnter() = 0;
	virtual void OnExit() = 0;
	virtual void OnSave() = 0;
	virtual void OnGui() = 0;
	virtual void OnSelecGui() = 0;
	
	virtual void OnMouseDown(int x, int y) {};
	virtual void OnMouseMove(int x, int y) {};
	virtual void OnMouseUp(int x, int y) {};

	virtual bool Dirty() { return dirty_; };
	void MakeDirty() { dirty_ = true; }

	void SetPath(std::string path);
	std::string GetPath() { return path_; }
	void SetDocName(std::string name) { docName_ = name; }
	std::string GetDocName() { return docName_; }

	void SetOpened(bool open) { opened_ = open; }
	bool IsOpened() { return opened_; }

	void SetDisplay(bool open) { display_ = open; }
	bool IsDisplay() { return display_; }

protected:
	std::string            path_{};
	DocType              docType_{};
	bool                     dirty_{ false };
	Context*              context_;
	nlohmann::json    json_{};
	bool                     init_{ false };
	bool						opened_{ false };
	bool                     display_{ false };
	std::string				docName_{};
};