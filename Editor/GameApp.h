#pragma once
#include "SDLApplication.hpp"
#include <SDL.h>
namespace Tiny2D {
	class GameApp : public SDLApplication
	{
	public:
		GameApp(GfxConfiguration& config)
			: SDLApplication(config) {};

		virtual int Initialize();
		virtual void Finalize();
		virtual void Tick();
		virtual void MouseMove(int x, int y)override;
	};
}
