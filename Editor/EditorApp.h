#pragma once
#include "UIHelper.hpp"
#include "GLFWApplication.hpp"
#include "ProjectMgr.h"

using namespace Tiny2DEditor;

namespace Tiny2D {
	class EditorApp : public GLFWApplication
	{
	public:
		EditorApp(GfxConfiguration& config)
			: GLFWApplication(config) {};

		virtual int Initialize();
		virtual void Finalize();
		virtual void Tick();
		virtual void OnMouseDown(int x, int y) override;
		virtual void OnMouseMove(int x, int y) override;
		virtual void OnMouseUp(int x, int y) override;
		virtual void onSizeChange(int width, int height)override;

		void AddTestRole();

		void AddTestCar();

		void AddTestTV();

		void BuildTestScene();

		void OnOpenProject(ProjectInfo& proj);
		void OnUpdate();
	};
}
