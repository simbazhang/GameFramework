
#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/stl.h>
#include <pybind11/embed.h>
#include <limits>
#include "imgui.h"
#include "imgui_internal.h"
#include "bind.h"
namespace py = pybind11;

template<typename T>
void template_ImVector(py::module &module, const char* name)
{
    py::class_< ImVector<T> >(module, name)
        .def_property_readonly_static("stride", [](py::object)
        {
            return sizeof(T);
        })
        .def_property_readonly("data", [](const ImVector<T>& self)
        {
            return long((void*)self.Data);
        })
        .def("__len__", [](const ImVector<T>& self)
        {
            return self.size();
        })
        .def("__iter__", [](const ImVector<T>& self)
        {
            return py::make_iterator(self.begin(), self.end());
        })
        .def("__getitem__", [](const ImVector<T>& self, size_t i)
        {
            if ((int)i >= self.size()) throw py::index_error();
            return self[i];
        })
        ;
}

PYBIND11_EMBEDDED_MODULE(deargui, deargui)
{
	deargui.doc() = "ImguiBind";

    py::class_<ImGuiContext>(deargui, "Context");
    template_ImVector<char>(deargui, "Vector_char");
    template_ImVector<float>(deargui, "Vector_float");
    template_ImVector<unsigned char>(deargui, "Vector_unsignedchar");
    template_ImVector<unsigned short>(deargui, "Vector_unsignedshort");
    template_ImVector<ImDrawCmd>(deargui, "Vector_DrawCmd");
    template_ImVector<ImDrawVert>(deargui, "Vector_DrawVert");
    template_ImVector<ImFontGlyph>(deargui, "Vector_FontGlyph");

    py::class_<ImVec2> Vec2(deargui, "Vec2");
    Vec2.def_readwrite("x", &ImVec2::x);
    Vec2.def_readwrite("y", &ImVec2::y);
    Vec2.def(py::init<>());
    Vec2.def(py::init<float, float>()
    , py::arg("_x")
    , py::arg("_y")
    );
    py::class_<ImVec4> Vec4(deargui, "Vec4");
    Vec4.def_readwrite("x", &ImVec4::x);
    Vec4.def_readwrite("y", &ImVec4::y);
    Vec4.def_readwrite("z", &ImVec4::z);
    Vec4.def_readwrite("w", &ImVec4::w);
    Vec4.def(py::init<>());
    Vec4.def(py::init<float, float, float, float>()
    , py::arg("_x")
    , py::arg("_y")
    , py::arg("_z")
    , py::arg("_w")
    );

	deargui.def("get_io", &ImGui::GetIO
		, py::return_value_policy::reference);

	deargui.def("show_demo_window", [](bool * p_open)
	{
		ImGui::ShowDemoWindow(p_open);
		return p_open;
	}
		, py::arg("p_open") = nullptr
		, py::return_value_policy::automatic_reference);

	deargui.def("begin", [](const char * name, bool * p_open, ImGuiWindowFlags flags)
	{
		auto ret = ImGui::Begin(name, p_open, flags);
		return std::make_tuple(ret, p_open);
	}
		, py::arg("name")
		, py::arg("p_open") = nullptr
		, py::arg("flags") = 0
		, py::return_value_policy::automatic_reference);
	deargui.def("end", &ImGui::End
		, py::return_value_policy::automatic_reference);
	deargui.def("begin_child", py::overload_cast<const char *, const ImVec2 &, bool, ImGuiWindowFlags>(&ImGui::BeginChild)
		, py::arg("str_id")
		, py::arg("size") = ImVec2(0, 0)
		, py::arg("border") = false
		, py::arg("flags") = 0
		, py::return_value_policy::automatic_reference);
	deargui.def("begin_child", py::overload_cast<ImGuiID, const ImVec2 &, bool, ImGuiWindowFlags>(&ImGui::BeginChild)
		, py::arg("id")
		, py::arg("size") = ImVec2(0, 0)
		, py::arg("border") = false
		, py::arg("flags") = 0
		, py::return_value_policy::automatic_reference);
	deargui.def("end_child", &ImGui::EndChild
		, py::return_value_policy::automatic_reference);
}

extern void PyInit_Deargui()
{
	py::module_::import("deargui");
}
