#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/stl.h>
#include <pybind11/embed.h>
#include "Context.h"
#include "Object.h"
#include "FileSystem.h"
#include "ResourceCache.h"
#include "Graphics.h"
#include "Shader.h"
#include "Timer.h"
#include "UI.h"
#include "Service.h"
#include "Render.h"
#include "bind.h"
#include "Texture2D.h"
#include "RefCounted.h"
#include "GltfModel.h"
#include "BaseApplication.hpp"
#include "Engine.h"

namespace py = pybind11;
using namespace Tiny2D;

class GFApp {};

PYBIND11_EMBEDDED_MODULE(gf, m)
{
	m.doc() = "game frame bind";

	py::class_<GFApp>(m, "GFApp")
		.def_property_readonly_static("context",
			[](py::object /* self */) { return BaseApplication::GetInstance()->GetContext(); });

	py::class_<RefCounted>(m, "RefCounted")
		.def(py::init<>())
		.def("AddRef", &RefCounted::AddRef);

	py::class_<Context, RefCounted>(m, "Context")
		.def(py::init<>());

	py::class_<Object, RefCounted>(m, "Object")
		.def(py::init<Context*>());

	py::class_<ResourceCache, Object>(m, "ResourceCache")
		.def(py::init<Context*>())
		.def("GetResourceTexture2D", &ResourceCache::GetResource<Texture2D>)
		.def("GetResourceGltfModel", &ResourceCache::GetResource<GltfModel>);
	
}

extern void PyInit_GameFrame()
{
	py::module_::import("gf");
}